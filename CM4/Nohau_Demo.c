
/*
*******************************************************************************
 * File:	Nohau_Demo.c
 *-----------------------------------------------------------------------------
 * Copyright (C) 2010 Nohau Solutions
 * All Rights Reserved
 *
 * Purpose
 *-----------------------------------------------------------------------------
 * Make it possible to show how to use the Lauterbach debugger and which kind 
 * of functionallitys there exsist in the system.
 *
 * Version History
 *-----------------------------------------------------------------------------
 * 11-MAR-2010	Joakim LArsson		Corrected some malfunction regarimg 
 *					callstack
 *
 * 11-MAR-2010	Kristoffer Martinsson	Added "HexSwapFractMem" swapping picture
 *					data between endienes
 *
 * 19-FEB-2010	Joakim LArsson		Addded complex structures and some union
 *
 * 08-FEB-2010	Joakim LArsson		Re - port of code to be generic
 *
 * 10-AUG-2009	Joakim Larsson		Added profiling code for trace
 *
 * 11-JUL-2008	Joakim Larsson		Created
 *
 *
*******************************************************************************
*/

#include "Nohau_Header.h"
//#include <stm32f4xx.h>  
#include "stdio.h"

//#include <RTL.h> 
//#include "cmsis_os.h"                   // ARM::CMSIS:RTOS:Keil RTX
#include "cmsis_os2.h" 
#include "Board_Buttons.h"              // ::Board Support:Buttons

//#define WITH_RTOS_OSECK 1
//#define WITH_CONSOLE 1
//#define DOUBLE_BUG 1
#define COMPILE_ALL 1
//#define COMPILE_FOR_TARGET_C6488 1
//#define BIG_ENDIAN 1
//#define SET_C64XX_TRCACECLOCK_20MHZ 1


#ifndef NULL
#define NULL ((void *) 0)
#endif

int *PointerToRefMem = 0, *PointerToOrgMem = 0;

extern void ITM_printf(const char *format,...);
extern void itm_enable(void);
extern osMessageQueueId_t Temp_MsgQueue;
extern osMessageQueueId_t ADC_MsgQueue;

//extern osThreadId_t tid_Thread_LED;

extern volatile uint32_t delay_val;

extern uint32_t  M4_EventTimer;
/*
*******************************************************************************
 * Description: This struct/union defines a bit-provider.
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/
typedef struct {
        unsigned bit0  : 1;
        unsigned bit1  : 1;
        unsigned bit2  : 1;
        unsigned bit3  : 1;
        unsigned bit4  : 1;
        unsigned bit5  : 1;
        unsigned bit6  : 1;
        unsigned bit7  : 1;
        unsigned bit8  : 1;
        unsigned bit9  : 1;
        unsigned bit10 : 1;
        unsigned bit11 : 1;
        unsigned bit12 : 1;
        unsigned bit13 : 1;
        unsigned bit14 : 1;
        unsigned bit15 : 1;
}  Integer;

typedef union {
        int BitNumber;
        Integer BitTs;
}  KindOfInteger;

struct StructCoreEtern {

    char * WhoAmI;
    int ID;

	union {

        int Nemesis[MAX][MAX];
        char Hathor;

    }   PLANC;
};

static struct NohauStructMain {  

    struct StructCoreEtern IdentitySCE;

    union {
        int GateKeeper[MAX][MAX];
        char Hathor;
    } 	EOR;

    int Field_1:2;
    unsigned Field_2:3;

}StatterNSM[CarlosMarciaRex];


//struct __FILE { int handle; /* Add whatever you need here */ };
/*
FILE __stdout;
FILE __stdin;

int fputc(int ch, FILE *f) {
  if (DEMCR & TRCENA) {
    while (ITM_Port32(0) == 0);
    ITM_Port8(0) = ch;
  }
  return(ch);
}
 */

	
int covTest(int a, int b, int c) {
	int outcome =0;
	if(a||b) {
		outcome = 1;
	}
	else {
		if(c) {
			outcome = 2;
		}
	}
	return outcome;
}


extern void ITM_write_channel_1(int );


static int i = 0;
void nohau_demo()
{
    static uint32_t delay_val=1;
    int Temp, NumBItter, NumbPower, BS[2], TrigoLooper, JuliaFlag, Bug, *_;
		volatile static int SW = 1;
    Temp = 0, NumBItter = 4, NumbPower = 9, TrigoLooper = 1500;
    JuliaFlag = 0; BS[0] = BS[1] = 0; Bug = 0;
    IdentifyMeNowClean = IdentifyMeClean, Temp++;
    IdentifyMeNow[0] = IdentifyMe1; IdentifyMeNow[1] = IdentifyMe2;
    IdentifyMeNow[2] = IdentifyMe3; IdentifyMeNow[3] = IdentifyMe4;
  
    char i_str[5] = {0,0,0,0,0};

    #ifdef SET_C64XX_TRCACECLOCK_20MHZ
        _ = (int *) 0x029A0164; *_ = 0x0000; *_ = 0x8000;
    #endif
	
	//SystemInit();

    InitMatrix();
    InitArr(SingArr);
    StructHandlerAnotherMem();
    BlankFractMem();

    #ifdef COMPILE_ALL
        LoadFractMem();

        #ifdef BIG_ENDIAN
            HexSwapFractMem();
        #endif
    #endif

    BlancStructHandler();

    #ifdef WITH_CONSOLE
        IOTestConsole(argc, argv,envp);
    #endif
static int test_variable;
    for(;;)
    {
		//printf("test");
//		while (Buttons_GetState() & 1U);        /* Wait while holding USER button */
    //osSignalSet(tid_Thread_LED, 1U);        /* Signal LED thread              */


        switch (SW)
        {

            /***** Case: 0 Generic debugging *****/ 
            case 0: 
              SetupSinewave();
              StructInit();
              StructHandler();
              StructHandlerAnotherMem();
              LinkedListInitFunction();
              SwapStructNameFault();
              LinkedListBlancFunction();


            break;


           /***** Case: 1 Handling arrays *****/
            case 1:

                SpeedTrouble();

                HexSwap(SingArr);
                HexSwap(SingArr);
                IntToFloat(SingArr, FloatArr);

                InitArr(SingArr);
                InitMatrix();

            break; 


            /***** Case: 2 Handling Memory *****/
            case 2:

                WriteMemDest();
                WriteMemWrite();
                BlankFractMem();

                #ifdef COMPILE_ALL
                    LoadFractMem();

                    #ifdef BIG_ENDIAN
                        HexSwapFractMem();
                    #endif

                #endif

            break;


            /***** Case: 3 Nestling Functions *****/
            case 3:

                InitArr(SingArr);

                BigSmall(BS, SingArr);
                Sorter(SingArr,0,MAXARR_PROF-2);
                Morgoth();
                Linker();
				
                Temp = RecurcePower(NumBItter,NumbPower);
                Temp = GetPrimerBack();
				
                Echoes00();

            break; 


            /***** Case: 4 View Fractal and multi curves *****/
            case 4:

                DataMotionWaveFormer();
               
            
            break;


            /***** Case: 5 Add load *****/
            case 5:

                InitArr(SingArr);
                InitMatrix();
                BlankFractMem();

                Common();
                Linker();
                Morgoth();

                SpeedTrouble();
                Temp = GetPrimerBack();
                Temp = RecurcePower(NumBItter,NumbPower);

                IntToFloat(SingArr, FloatArr);
                HexSwap(SingArr);
                IntToFloat(SingArr, FloatArr);
                HexSwap(SingArr);

                BigSmall(BS, SingArr);
                Sorter(SingArr,0,MAXARR_PROF-2);

                WriteMemDest();
                WriteMemWrite();

                Trigo(TrigoLooper);

                DataMotionWaveFormer();

            break;   


            case 6:
                if (i == 0)
                {
                   itm_enable();
                }
                if (osMessageQueueGetCount( Temp_MsgQueue ) > 0) {
                  osMessageQueueGet (Temp_MsgQueue, &i, NULL, NULL);
                  sprintf(i_str, "%d",i);
                  i_str[3] = i_str[2];
                  i_str[2] = '.';
                  ITM_printf("Temperature is %s C\n",i_str);
                  //ITM_printf("Temperature is %d C\n",i);
                }
                SpeedTrouble();
               
            break;


        }/*  End_Of_Switch_Case  */
        if (osMessageQueueGetCount( ADC_MsgQueue ) > 0) {
          osMessageQueueGet (ADC_MsgQueue, &delay_val, NULL, NULL);
        }
        osDelay(delay_val); 
    }/*  End_of_Loop  */
}/*  End_Of_Main  */



/*
*******************************************************************************
 * Description: This function initialize a x-referenced struct. Works also as 
 * grey code
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void StructInit(void)
{
    int i,j,k;

    int IdentifyMe[]={74,111,97,107,105,109,46,76,97,114,115,115,111,110,64,78,111,104,97,117};
    int Sophisticated[]={76,101,101,118,105,46,76,101,104,116,105,110,101,110,64,78,111,104,97,117};

    struct StructMain *EverGreen;
    struct StructCore *NoKern;

    EverGreen = (struct StructMain *) calloc(MaxMorphosis, sizeof(struct StructMain));
    NoKern = (struct StructCore *) calloc(MaxMorphosis, sizeof(struct StructCore));

    for(i = 0; i< 2; i++)
    {

        Statter[i].Field_1 = 1;
        Statter[i].Field_2 = 2;
        Statter[i].Venus = rand()%(1024);
        Statter[i].Identity1.MosseBo = rand()%(1024);
        Statter[i].EOR.Ra = rand()%(1024);
        Statter[i].EOR.Osiris = ((float)(rand()%(1024)))/2.17;
        Statter[i].EOR.Hathor = ((char) rand()%(257));
        Statter[i].Identity1.RosenFors = (char *) calloc(20, sizeof(char));
        Statter[i].Identity2.RosenFors = (char *) calloc(20, sizeof(char));

        for(j = 0; j < MaxMorphosis*2; j++)
        {
            Statter[i].Identity1.RosenFors[j]=(char) IdentifyMe[j];
            Statter[i].Identity2.RosenFors[j]=(char) Sophisticated[j];
            Statter[i].Field_1 = EverGreen[i].Field_1 + 1;
            Statter[i].Field_2 = EverGreen[i].Field_2 - 1;
        }
    }

    for(i = 0; i< MaxMorphosis; i++)
    {
        EverGreen[i].Field_1 = 1;
        EverGreen[i].Field_2 = 2;
        EverGreen[i].Venus = rand()%(1024);
        EverGreen[i].Identity1.MosseBo = rand()%(1024);
        EverGreen[i].EOR.Ra = rand()%(1024);
        EverGreen[i].EOR.Osiris = ((float)(rand()%(1024)))/2.17;
        EverGreen[i].EOR.Hathor = ((char) rand()%(257));
        EverGreen[i].Identity1.RosenFors = (char *) calloc(20, sizeof(char));
        EverGreen[i].Identity2.RosenFors = (char *) calloc(20, sizeof(char));
    }

    for(i = 0; i < MaxMorphosis; i++)
    {
        for(j = 0; j < MaxMorphosis*2; j++)
        {
            EverGreen[i].Identity1.RosenFors[j]=(char) IdentifyMe[j];
            EverGreen[i].Identity2.RosenFors[j]=(char) Sophisticated[j];
            EverGreen[i].Field_1 = EverGreen[i].Field_1 + 1;
            EverGreen[i].Field_2 = EverGreen[i].Field_2 - 1;
        }
        for(k = MaxMorphosis*2-1; k > -1; k--)
        {
            EverGreen[i].Field_1 = EverGreen[i].Field_1 - 1;
            EverGreen[i].Field_2 = EverGreen[i].Field_2 + 1;
        }
    }
    for (i = 0; i < 2; i++) {
      free(Statter[i].Identity1.RosenFors);
      free(Statter[i].Identity2.RosenFors);
    }
    for(i = 0; i< MaxMorphosis; i++) {
      free(EverGreen[i].Identity1.RosenFors);
      free(EverGreen[i].Identity2.RosenFors);
    }
    free(EverGreen);
    free(NoKern);
}
#ifdef WITH_CONSOLE
/*
*******************************************************************************
 * Description:	For demonstrating and testing IO handling via console with 
 *              Lauterbach. 
 *
 * Created by: Joakim Larsson
*******************************************************************************
*/

void IOTestConsole(int argc, char **argv,char **envp){

    int t;

    printf("There is %d parameters to main\n", argc);

    for ( t = 0; t < argc; t++ )
        printf("\nParameter %d = %s ",t,argv[t]);

    printf("\nThe environment is\n");

    for (t = 0; envp[t]; t++)
        printf("%s\n",envp[t]);

    fflush( stdout );	
}
#endif
/*
*******************************************************************************
 * Description:	For demonstrating and testing how to view structures with  
 *              Lauterbach. 
 *
 * Created by: Joakim Larsson
*******************************************************************************
*/

void StructHandler(void){

    int i, ii,iii;
    char *pointer;

    PointerToOrgMem = (int *) StatterNSM;
	
    for(i = 0; i < CarlosMarciaRex; i++){
	
        StatterNSM[i].IdentitySCE.WhoAmI = IdentifyMeNow[i];

        StatterNSM[i].IdentitySCE.ID = GetPrimerBack();

        StatterNSM[i].IdentitySCE.PLANC.Hathor = (char ) GetPrimerBack();

        StatterNSM[i].EOR.Hathor = (char ) GetPrimerBack();

        StatterNSM[i].Field_1 = GetPrimerBack();

        StatterNSM[i].Field_2 = (short) GetPrimerBack();
		
        for(ii = 0; ii < MAX; ii++)
            for(iii = 0; iii < MAX; iii++){

                StatterNSM[i].IdentitySCE.PLANC.Nemesis[ii][iii] = NohauMatrix[ii][iii]; 

                StatterNSM[i].EOR.GateKeeper[ii][iii] = NohauMatrix[iii][ii];
            }			
    }
       
    pointer = IdentifyMeNow[0];

    for(i = 0; i < (CarlosMarciaRex - 1); i++) 
        IdentifyMeNow[i] =  IdentifyMeNow[i+1];
            
    IdentifyMeNow[i] = pointer;


    #ifdef WITH_CONSOLE           
        for(i = 0; i < 4; i++) printf("\n%s",IdentifyMeNow[i]);
        printf("\n\n");
    #endif
    	
}
/*
*******************************************************************************
 * Description:	For demonstrating and testing how to memorys arbitrary memory  
 *              as a specific strutc with Lauterbach. 
 *
 * Created by: Joakim Larsson
*******************************************************************************
*/

void StructHandlerAnotherMem(void){

    int i, ii,iii;
	
    PointerToRefMem = (int *) NSM;

    for(i = 0; i < CarlosMarciaRex; i++){
	
        NSM[i].ISCE.Who = IdentifyMeNowClean;

        NSM[i].ISCE.ID = i%2;

        NSM[i].ISCE.P.H = i%2;

        NSM[i].E.H = (char ) i%2;

        NSM[i].F_1 = i%2;

        NSM[i].F_2 = (short) i%2;;
		
        for(ii = 0; ii < MAX; ii++)
            for(iii = 0; iii < MAX; iii++){

                NSM[i].ISCE.P.N[ii][iii] = NohauMatrix[iii][ii]; 

                NSM[i].E.GK[ii][iii] = NohauMatrix[ii][iii];
            }			
    }		
}

/*
*******************************************************************************
 * Description:	Initializes a memory which can be intepreted as a struct 
 *
 * Created by: Joakim Larsson
*******************************************************************************
*/

void BlancStructHandler(void){
    int i, ii,iii;
	
    for(i = 0; i < CarlosMarciaRex; i++){
	
        StatterNSM[i].IdentitySCE.WhoAmI = (char )0;

        StatterNSM[i].IdentitySCE.ID = 0;

        StatterNSM[i].IdentitySCE.PLANC.Hathor = 0;

        StatterNSM[i].EOR.Hathor = 0;

        StatterNSM[i].Field_1 = 0;

        StatterNSM[i].Field_2 = 0;
		
        for(ii = 0; ii < MAX; ii++)
            for(iii = 0; iii < MAX; iii++){

                StatterNSM[i].IdentitySCE.PLANC.Nemesis[ii][iii] = 0; 

                StatterNSM[i].EOR.GateKeeper[ii][iii] = 0;
            }			
    }
}


/*
*******************************************************************************
 * Description:	For demonstrating a single linked list with Lauterbach 
 *
 * Created by: Joakim Larsson
*******************************************************************************
*/

void LinkedListInitFunction(void){

    container * current, * head;
    int i;

    head = NULL;
 
    /*  Initialize the List  */
    for(i = 1; i <= MAX; i++) {
   
        current = (container *)malloc(sizeof(container));
        current->BoxOfInteger = i;
        current->BoxOfChar = (char) (i+96);
        current->next  = head;
        head = current;
    }

    current = head;

    while(current) {

        #ifdef WITH_CONSOLE           
            printf("%d\n", current->choice);
        #endif

        current = current->next ;
    }

	 for(i = 1; i <= MAX; i++) {
    	current = head;
    	head = head->next;
    	free(current);    	
    }

    
}

/*
*******************************************************************************
 * Description:	This functions conatains a bug. The intention is tha it should
 * swap the names in a predefiens structmember called 'WhoAmI'. That is not 
 * done in a correct way. 
 *
 * Created by: Joakim Larsson - and yes - I KNOW this code contain a bug.
*******************************************************************************
*/

void SwapStructNameFault(void){

    int i;
    char *pointer;

    #ifdef DOUBLE_BUG
    int ii;    
    for(ii = 0; ii < (CarlosMarciaRex - 1); ii++)    
    {
        pointer = IdentifyMeNow[0];

        for(i = 0; i < (CarlosMarciaRex - 1); i++) 
            IdentifyMeNow[i] =  IdentifyMeNow[i+1];
            
        IdentifyMeNow[i+1] = pointer;


        #ifdef WITH_CONSOLE           
            for(i = 0; i < 4; i++) printf("\n%s",IdentifyMeNow[i]);
            printf("\n\n");
        #endif
    }
    #else
    pointer = IdentifyMeNow[0];

    for(i = 0; i < (CarlosMarciaRex - 1); i++) 
        IdentifyMeNow[i] =  IdentifyMeNow[i+1];
            
    IdentifyMeNow[i+1] = pointer;


    #ifdef WITH_CONSOLE           
        for(i = 0; i < 4; i++) printf("\n%s",IdentifyMeNow[i]);
        printf("\n\n");
    #endif
	#endif

}


/*
*******************************************************************************
 * Description:	For demonstrating a single linked list with Lauterbach 
 *
 * Created by: Joakim Larsson
*******************************************************************************
*/

void LinkedListBlancFunction(void){

    container * current, * head;
    int i;

    head = NULL;
 
    /*  Initialize the List to base values  */
    for(i = 1; i <= MAX; i++) {
   
        current = (container *)malloc(sizeof(container));
        current->BoxOfInteger = 0;
        current->BoxOfChar = (char) (BASE);
        current->next  = head;
        head = current;
    }

    current = head;

    while(current) {

        #ifdef WITH_CONSOLE           
            printf("%d\n", current->choice);
        #endif

        current = current->next ;

    }

	for(i = 1; i <= MAX; i++) {
    	current = head;
    	head = head->next;
    	free(current);    	
    }
}

/*
*******************************************************************************
 * Description:	For demonstrating Trigometric functions in benchmarking with 
                Lauterbach. 
 *
 * Created by: Joakim Larsson
*******************************************************************************
*/

static void Trigo (int looper)
{
    int ii;
    double x,y,xx,yy;

    x = 0.6; y = 0.5; xx  = 0.49997525489; yy = 2.49997525489;

    for (ii = 0; ii < MAXARR_PROF; ii++){
        TanArr[ii] = SinArr[ii];
        SinArr[ii] = CosArr[ii];
        CosArr[ii] = TanArr[ii];

    }

    for (ii = 1; ii <= looper; ii++) {

        x = xx * NohauATAN(yy*NohauSIN(x)*NohauCOS(x)/(NohauCOS(x+y)+NohauCOS(x-y)-1.0));

        y = xx * NohauATAN(yy*NohauSIN(y)*NohauCOS(y)/(NohauCOS(x+y)+NohauCOS(x-y)-1.0));
    }

    for (ii = 0; ii < MAXARR_PROF; ii++){

        SinArr[ii] = sin(((float ) ii)/10);
        CosArr[ii] = cos(((float ) ii)/10);
        TanArr[ii] = tan(((float ) ii)/10);

    }

}


/*
*******************************************************************************
 * Description:	For demonstrating how-to use different graph-tools in 
 *              Lauterbach. This function generates three graphs based on 
 *              on tranformation of a SineWave (aproximation of filter). Can 
 *              used to draw three graphs at the same time.
 *
 * Created by: Joakim Larsson
*******************************************************************************
*/

void DataMotionWaveFormer(void)
{
    int i,p = 0;

    for(i = 0; i < 412; i++)
    {
        FrecArr1[i] = FrecArr2[i] = FrecArr3[i] = i;
    }

    for(i = 0; i < 412; i++)
    {
        FrecArr1[i] =  WaveFormer(p)*6900;
        FrecArr2[i] =  WaveFormer((int)FrecArr1[i])*100 + 100;
        FrecArr3[i] =  WaveFormer((int)FrecArr2[i])*100 + 100;
        p = p + 10;
    }
}

/*
*******************************************************************************
* Description:	
*
*  GetPrimerBack is a seudo prime generator.
*
* Created by: Joakim Larsson 
*******************************************************************************
*/

int GetPrimerBack()
{
    register int i = 0, ii = 0, TempPrimer = 0;

    for ( i = 0 ; i <= DIMM ; i++) DimKeeper[ i++ ] = 1 ;

    for ( i = 0 ; i <= DIMM ; i++ ){

        if ( DimKeeper[ i ] ) {

            TempPrimer = i + i + DRAC;  ii = i + TempPrimer;
            while ( ii <= DIMM ) { DimKeeper[ ii ] = 0; ii += TempPrimer; }
            Checker++;

        }
    }

    return Checker;
}

/*
*******************************************************************************
 * Description:	For generating Waveformer data to an array. This function is 
 *              called from DataMoionWaveFormer.
 *
 * Created by: Joakim Larsson
*******************************************************************************
*/

float WaveFormer(int x)
{

    const double A = -0.015959964859;

    const double B = 217.68468676;

    const double C = 0.000028716332164;

    const double D = -0.0030591066066;

    const double E= -7.3316892871734489e-005;

    double y;
    int neg;

    neg = FALSE;

    if (x>2048){
        neg = TRUE;
        x -= 2048;
    }

    if (x > 1024)
        x = 2048 - x;

    if (neg)
        y =- ( ( A + x ) / ( B + C * x * x ) + D * x - E );

    else
        y = ( A + x ) / ( B + C * x * x ) + D * x - E;

    return (float) y;
}

/*
*******************************************************************************
* Description:	
*
*  Wotan() Returns a random floating point value between 0.0 and 1.0.
*  If sejd is negative a new series starts (and sejd is made positive 
*  so that subsequent calls using an unchanged sejd will continue in 
*  the same sequence)
*
* Created by: Joakim Larsson 
*******************************************************************************
*/

float Wotan(int *idum)
{
    int j,k;
    static int idum2 = 123456789;
    static int iy = 0;
    static int iv[NTAB];
    float temp;

    /* initialize */
    if (*idum <= 0) {
      
        /* prevent idum == 0 */
        if (-(*idum) < 1)                           
            *idum = 1;
        
        /* make idum positive */
        else
            *idum = -(*idum);                         
        idum2 = (*idum);

        /* load the shuffle table */
        for (j = NTAB + 7; j >= 0; j--) {           
            k = (*idum) / IQ1;
            *idum = IA1 * (*idum - k*IQ1) - k*IR1;
  
            if (*idum < 0)   *idum += IM1;
            if (j < NTAB)    iv[j] = *idum;
        }
        iy = iv[0];
    }
    k = (*idum) / IQ1;
    *idum = IA1 * (*idum - k*IQ1) - k*IR1;

    if (*idum < 0)    *idum += IM1;
    k = idum2/IQ2;
    idum2 = IA2 * (idum2 - k*IQ2) - k*IR2;

    if (idum2 < 0)    idum2 += IM2;

    j = iy / NDIV;
    iy = iv[j] - idum2;
    iv[j] = *idum;

    if (iy < 1)     iy += IMM1;
    if ((temp = AM * iy) > RNMX)    return RNMX;
    else                            return temp;

}

/*
*******************************************************************************
* Description:	
*
* Kain returns '1' or '0' depending of if 'Jared' is Primenumber or not
*
* Created by: Joakim Larsson 
*******************************************************************************
*/

int Kain(int Jered){

        /*Simple test - if primenumber or not*/
        int i, Henok = 1;

        for(i = 2; i <= Jered / 2; i = i + 1)
                if((Jered%i)==0)	
                        Henok = 0;

                if(Henok == 1)
                        return 1;

                else
                        return 0;
}

/*
*******************************************************************************
 * Description:	This function sorts the numbers in  NohauMatrix. Its used 
 *              for showing index pointers, spotlights, and CTS.
 *
 *
 * Created by: Joakim Larsson
*******************************************************************************
*/

void SpeedTrouble(void){

    int Temp = 0, i = 0, j = 0, end = 0;

    while(end == 0){ end = 1;
        for(i = 0; i < MAX-1; i ++)
            for(j = 0; j < MAX; j ++){ 

                Temp = NohauMatrix[i][j];

                if(NohauMatrix[i][j] > NohauMatrix[i+1][j]){

                    NohauMatrix[i][j] = NohauMatrix[i+1][j]; 

                    NohauMatrix[i+1][j] = Temp; end = 0;
                }
            }
    }
}

/*
*******************************************************************************
 * Description:	This function calls adds �binaries� into the array: Primer. 
 *              Theses values is calculated and based from a pseudo random 
 *              generator Kain and Wotan.
 *
 * Created by: Joakim Larsson
*******************************************************************************
*/

void Morgoth(void){

    int Inv,i,dest,ticker = 0;

    for(i = StartPrimer; i < StopPrimer; i++){ 
        Inv = -i;
        dest = (int) ((Wotan(&Inv)*fact)/((float)fact/100));

        if(dest > 9) 
            dest = (int) ((Wotan(&Inv)*fact)/((float)fact/10));

        Primer[ticker] = Kain(dest);
        ticker++;
    }

    StartPrimer = StartPrimer + MaxPrimer;
    StopPrimer = StopPrimer + MaxPrimer;

    if(StopPrimer > 600){
        StopPrimer = MaxPrimer;
        StartPrimer = 1;

        for(i = 0; i < MaxPrimer; i++)	
            Primer[i] = 0;
    }
}


#ifdef WITH_RTOS
/*
*******************************************************************************
 * Description:	This function initialize �NohauMatrix� by random - numbers 
 * generated by RTOS. 
 *              
 * Created by: Joakim Larsson
*******************************************************************************
*/

void InitMatrix(void){
    int i, j;

    for(i = 0; i < MAX; i ++){
	
        for(j = 0; j < MAX; j ++){

            NohauMatrix[i][j] = rand()%100;
        }
    }
}
#else

/*
*******************************************************************************
 * Description:	This function initialize �NohauMatrix� by pseudo random - 
 * numbers. 
 *              
 * Created by: Joakim Larsson
*******************************************************************************
*/

void InitMatrix(void){

    int i, j, Inv;

    for(i = 0; i < MAX; i ++){
        for(j = 0; j < MAX; j ++)
        {
	        if(j%2 == 0)
		    Inv = -i;
                else 
                    Inv = -j;

                NohauMatrix[i][j] = (int) ((Wotan(&Inv)*fact)/((float)fact/100));
        }
    }

    for(i = 0; i < MAX; i++){
        NohauMatrix[MAX-1][i] = 0;
        NohauMatrix[0][i] = i+1;
        NohauMatrix[1][i] = i;
        NohauMatrix[2][i] = MAX-1-i;
    }
}
#endif

#ifdef WITH_RTOS_OSECK
/*
*******************************************************************************
 * Description:	This function initialize �FractMem�. It�s used for testing 
 *              different debug modes and doesn�t actually affect the program 
 *              if running in native mode.
 *
 * Created by: Joakim Larsson
*******************************************************************************
*/

void BlankFractMem(void){
    int i,ii;
    ii = ( ( ( sizeof( FractArr ) ) / 4 ) - 1 );
    for( i = 0; i < ii; i++ )
        FractArr[i] = 0x00000000;
}

#else

/*
*******************************************************************************
 * Description:	This function initialize �FractArr�. It�s used for testing 
 *              different debug modes and doesn�t actually affect the program 
 *              if running in native mode.
 *
 * Created by: Joakim Larsson
*******************************************************************************
*/

void BlankFractMem(void){
    int i,ii = 100;

    for( i = 0; i < ii; i++ ) FractArr[i] = 0x00000000; 
}
#endif

/*
*******************************************************************************
 * Description:	This function writes some values into targets memory. User
 *              can via Lauterbach change these values and/or addresses.
 *              In default mode (unchanged) the function shows how to view 
 *              the memory location as a JPG via Lauterbach. In native mode 
 *              this function 'restores' values in a memory location which 
 *              stores a JTAG - picture.
 * 
 *
 * Created by: Joakim Larsson
*******************************************************************************
*/

void WriteMemWrite(void){

        int *BlockAddr1, *BlockAddr2,i;

        BlockAddr1 = (int *) FractArr;
        BlockAddr2 = (int *) FractArr;
 
        for(i = 0; i < 12; i++){
            BlockAddr1++; BlockAddr2++;
        }

		for(i = 0; i < 52; i++)
                    BlockAddr2++;

        *BlockAddr1=0x06060800; BlockAddr1++;
        *BlockAddr2=0x04000302; BlockAddr2++;
        *BlockAddr1=0x08050607; BlockAddr1++;
        *BlockAddr2=0x21120511; BlockAddr2++;
        *BlockAddr1=0x09070707; BlockAddr1++;
        *BlockAddr2=0x13064131; BlockAddr2++;
        *BlockAddr1=0x0C0A0809; BlockAddr1++;
        *BlockAddr2=0x22076151; BlockAddr2++;	
}

/*
*******************************************************************************
 * Description:	This function writes some values into targets memory. User
 *              can via Lauterbach change these values and/or addresses.
 *              In default mode (unchanged) the function shows how to view 
 *              the memory location as a JPG via Lauterbach. In native mode 
 *              this function 'destroys' values in a memory location which 
 *              stores a JTAG - picture.
 *              
  * Created by: Joakim Larsson
*******************************************************************************
*/

void WriteMemDest(void){

        int *BlockAddr1, *BlockAddr2,i;

        BlockAddr1 = (int *) FractArr;
        BlockAddr2 = (int *) FractArr;
 
        for(i = 0; i < 12; i++){
                BlockAddr1++; BlockAddr2++;
        }

		for(i = 0; i < 52; i++)
                BlockAddr2++;

        *BlockAddr1=0xD6060800; BlockAddr1++;
        *BlockAddr2=0x04001302; BlockAddr2++;
        *BlockAddr1=0xE8F50607; BlockAddr1++;
        *BlockAddr2=0x2F120511; BlockAddr2++;
        *BlockAddr1=0xBEFDCB;   BlockAddr1++;
        *BlockAddr2=0x13B64131; BlockAddr2++;
        *BlockAddr1=0xFF0A0809; BlockAddr1++;
        *BlockAddr2=0x22F76151; BlockAddr2++;	
}

/*
*******************************************************************************
 * Description: This function prints corresponding dec-to-bin value.
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void BitPrint(int Number){

        KindOfInteger  OwnDefNumber;
        OwnDefNumber.BitNumber = Number;

        ITM_printf("%u%u%u%u%u%u%u%u%u%u%u%u%u%u%u%u\n\n", 		\
        OwnDefNumber.BitTs.bit15,OwnDefNumber.BitTs.bit14,	\
        OwnDefNumber.BitTs.bit13,OwnDefNumber.BitTs.bit12,	\
        OwnDefNumber.BitTs.bit11,OwnDefNumber.BitTs.bit10,	\
        OwnDefNumber.BitTs.bit9,OwnDefNumber.BitTs.bit8,	\
        OwnDefNumber.BitTs.bit7,OwnDefNumber.BitTs.bit6,	\
        OwnDefNumber.BitTs.bit5,OwnDefNumber.BitTs.bit4,	\
        OwnDefNumber.BitTs.bit3,OwnDefNumber.BitTs.bit2,	\
        OwnDefNumber.BitTs.bit1,OwnDefNumber.BitTs.bit0		\
        );
}

/*
*******************************************************************************
 * Description: This function swaps the data between the endieness in hex.

 * Created by: Joakim Larsson 
*******************************************************************************
*/

void HexSwap(int arr[MAXARR_PROF])
{
        int i;
        for(i = 0; i < MAXARR_PROF; i++)
        {
                arr[i] =                             \
                ((arr[i]&0xFF000000)>>24)   |        \
                ((arr[i]&0x00FF0000)>>8)    |        \
                ((arr[i]&0x0000FF00)<<8)    |        \
                ((arr[i]&0x000000FF)<<24);
        }
}

/*
*******************************************************************************
 * Description: This function calls a number of functions. Usage: To demostrate
 *              stack caller, reccursive calls and linkage analyze
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void Linker(void){
    ResPow = 0.0; NumberOfStackCalls = 0; NumBItter = 3; NumbPower = 5;

    XEchoes();
    Common();
    EchoesOfOsirian();
    XEchoes();

   // SetupSinewave();
    
    ResPow = RecurcePower(NumBItter,NumbPower);
    lecter++;
}
/*
*******************************************************************************
 * Description: This function is an extension to an example which shows handling 
 *              of virtual functions.
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

int Virtual(int i){
    
    char caster = 'A';
    for (i=100;i<110;){
        caster = (char) i++;
    }
	caster++;
    return i;
}

/*
*******************************************************************************
 * Description: This function icreates and calls a virtual function.
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/
void Common(void){

    int j = 0;
    int i = 0;

    int (*VirtualFunc)(int);

    VirtualFunc = Virtual;

    i = VirtualFunc(i);
    
    for (;;) {
        if (i==110) { break;} i++; }

    for(j = 0; j < 400; j++){}
}
/*
*******************************************************************************
 * Description: This function calculate power �pow� of  �RecCount �. Shows
 *		how the stack-viwer works in recursive mode
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

double RecurcePower(double RecCount, unsigned pow)
{
    NumberOfStackCalls++;

    if(pow == 0)  
        return(1.0);

    else
        return(RecurcePower(RecCount, pow - 1) * RecCount);
}
/*
*******************************************************************************
 * Description: This function belongs to the group of stack-frame functions.
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void Echoes00(void){

    int i;

    i = MaxPrimer-1;

    do {
        Primer[i] = 0;
        i--;
    } while(i > 0);

    Primer[0] = 1;
    Echoes01();
}

/*
*******************************************************************************
 * Description: This function belongs to the group of stack-frame functions.
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void Echoes01(void){

    char LocalEcho01 = 'a';
    GlobalEcho++;

    Primer[0]  = 0;  Primer[1]  = 1;  Primer[2]  = 0;  Primer[3]  = 0;
    Primer[4]  = 0;  Primer[5]  = 0;  Primer[6]  = 0;  Primer[7]  = 0;
    Primer[8]  = 0;  Primer[9]  = 0;  Primer[10] = 0;  Primer[11] = 0;
                                                       Primer[12] = 0;
    LocalEcho01 = 'A';
    LocalEcho01++;
    Echoes02();
}
/*
*******************************************************************************
 * Description: This function belongs to the group of stack-frame functions.
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void Echoes02(void){

    char LocalEcho02 = 'b';
    GlobalEcho++;

    Primer[0]  = 0;  Primer[1]  = 0;  Primer[2]  = 1;  Primer[3]  = 0;
    Primer[4]  = 0;  Primer[5]  = 0;  Primer[6]  = 0;  Primer[7]  = 0;
    Primer[8]  = 0;  Primer[9]  = 0;  Primer[10] = 0;  Primer[11] = 0;
                                                       Primer[12] = 0;
    LocalEcho02 = 'B';
    LocalEcho02++;
    Echoes03();
}
/*
*******************************************************************************
 * Description: This function belongs to the group of stack-frame functions.
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void Echoes03(void){

    char LocalEcho03 = 'c';
    GlobalEcho++;

    Primer[0]  = 0;  Primer[1]  = 0;  Primer[2]  = 0;  Primer[3]  = 1;
    Primer[4]  = 0;  Primer[5]  = 0;  Primer[6]  = 0;  Primer[7]  = 0;
    Primer[8]  = 0;  Primer[9]  = 0;  Primer[10] = 0;  Primer[11] = 0;
                                                       Primer[12] = 0;
    LocalEcho03 = 'C';
    LocalEcho03++;
    Echoes04();
}
/*
*******************************************************************************
 * Description: This function belongs to the group of stack-frame functions.
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void Echoes04(void){

    char LocalEcho04 = 'd';
    GlobalEcho++;

    Primer[0]  = 0;  Primer[1]  = 0;  Primer[2]  = 0;  Primer[3]  = 0;
    Primer[4]  = 1;  Primer[5]  = 0;  Primer[6]  = 0;  Primer[7]  = 0;
    Primer[8]  = 0;  Primer[9]  = 0;  Primer[10] = 0;  Primer[11] = 0;
                                                       Primer[12] = 0;
    LocalEcho04 = 'D';
    LocalEcho04++;
    Echoes05();
}
/*
*******************************************************************************
 * Description: This function belongs to the group of stack-frame functions.
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void Echoes05(void){

    char LocalEcho05 = 'e';
    GlobalEcho++;

    Primer[0]  = 0;  Primer[1]  = 0;  Primer[2]  = 0;  Primer[3]  = 0;
    Primer[4]  = 0;  Primer[5]  = 1;  Primer[6]  = 0;  Primer[7]  = 0;
    Primer[8]  = 0;  Primer[9]  = 0;  Primer[10] = 0;  Primer[11] = 0;
                                                       Primer[12] = 0;
    LocalEcho05 = 'E';
    LocalEcho05++;
    Echoes06();
}
/*
*******************************************************************************
 * Description: This function belongs to the group of stack-frame functions.
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void Echoes06(void){

    char LocalEcho06 = 'f';
    GlobalEcho++;

    Primer[0]  = 0;  Primer[1]  = 0;  Primer[2]  = 0;  Primer[3]  = 0;
    Primer[4]  = 0;  Primer[5]  = 0;  Primer[6]  = 1;  Primer[7]  = 0;
    Primer[8]  = 0;  Primer[9]  = 0;  Primer[10] = 0;  Primer[11] = 0;
                                                       Primer[12] = 0;
    LocalEcho06 = 'F';
    LocalEcho06++;
    Echoes07();
}
/*
*******************************************************************************
 * Description: This function belongs to the group of stack-frame functions.
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void Echoes07(void){

    char LocalEcho07 = 'g';
    GlobalEcho++;

    Primer[0]  = 0;  Primer[1]  = 0;  Primer[2]  = 0;  Primer[3]  = 0;
    Primer[4]  = 0;  Primer[5]  = 0;  Primer[6]  = 0;  Primer[7]  = 1;
    Primer[8]  = 0;  Primer[9]  = 0;  Primer[10] = 0;  Primer[11] = 0;
                                                       Primer[12] = 0;
    LocalEcho07 = 'G';
    LocalEcho07++;
    Echoes08();
}
/*
*******************************************************************************
 * Description: This function belongs to the group of stack-frame functions.
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void Echoes08(void){

    char LocalEcho08 = 'h';
    GlobalEcho++;

    Primer[0]  = 0;  Primer[1]  = 0;  Primer[2]  = 0;  Primer[3]  = 0;
    Primer[4]  = 0;  Primer[5]  = 0;  Primer[6]  = 0;  Primer[7]  = 0;
    Primer[8]  = 1;  Primer[9]  = 0;  Primer[10] = 0;  Primer[11] = 0;
                                                       Primer[12] = 0;
    LocalEcho08 = 'H';
    LocalEcho08++;
    Echoes09();
}
/*
*******************************************************************************
 * Description: This function belongs to the group of stack-frame functions.
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void Echoes09(void){

    char LocalEcho09 = 'i';
    GlobalEcho++;

    Primer[0]  = 0;  Primer[1]  = 0;  Primer[2]  = 0;  Primer[3]  = 0;
    Primer[4]  = 0;  Primer[5]  = 0;  Primer[6]  = 0;  Primer[7]  = 0;
    Primer[8]  = 0;  Primer[9]  = 1;  Primer[10] = 0;  Primer[11] = 0;
                                                       Primer[12] = 0;
    LocalEcho09 = 'I';
    LocalEcho09++;
    Echoes10();
}
/*
*******************************************************************************
 * Description: This function belongs to the group of stack-frame functions.
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void Echoes10(void){

    char LocalEcho10 = 'j';
    GlobalEcho++;

    Primer[0]  = 0;  Primer[1]  = 0;  Primer[2]  = 0;  Primer[3]  = 0;
    Primer[4]  = 0;  Primer[5]  = 0;  Primer[6]  = 0;  Primer[7]  = 0;
    Primer[8]  = 0;  Primer[9]  = 0;  Primer[10] = 1;  Primer[11] = 0;
                                                       Primer[12] = 0;
    LocalEcho10 = 'J';
    LocalEcho10++;
    Echoes11();
}
/*
*******************************************************************************
 * Description: This function belongs to the group of stack-frame functions.
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void Echoes11(void){

    char LocalEcho11 = 'k';
    GlobalEcho++;

    Primer[0]  = 0;  Primer[1]  = 0;  Primer[2]  = 0;  Primer[3]  = 0;
    Primer[4]  = 0;  Primer[5]  = 0;  Primer[6]  = 0;  Primer[7]  = 0;
    Primer[8]  = 0;  Primer[9]  = 0;  Primer[10] = 0;  Primer[11] = 1;
                                                       Primer[12] = 0;
    LocalEcho11 = 'K';
    LocalEcho11++;
    Echoes12();
}
/*
*******************************************************************************
 * Description: This function belongs to the group of stack-frame functions.
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void Echoes12(void){

    char LocalEcho12 = 'l';
    GlobalEcho++;

    Primer[0]  = 0;  Primer[1]  = 0;  Primer[2]  = 0;  Primer[3]  = 0;
    Primer[4]  = 0;  Primer[5]  = 0;  Primer[6]  = 0;  Primer[7]  = 0;
    Primer[8]  = 0;  Primer[9]  = 0;  Primer[10] = 0;  Primer[11] = 0;
                                                       Primer[12] = 1;
    LocalEcho12 = 'L';
    LocalEcho12++;

}

/*
*******************************************************************************
 * Description: This function belongs to the group of stack-frame functions. 
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

static void XEchoes(void){

    char LocalEchoX = 'x';
    GlobalEcho++;

    Primer[0]  = 1;  Primer[1]  = 1;  Primer[2]  = 1;  Primer[3]  = 1;
    Primer[4]  = 1;  Primer[5]  = 1;  Primer[6]  = 1;  Primer[7]  = 1;
    Primer[8]  = 1;  Primer[9]  = 1;  Primer[10] = 1;  Primer[11] = 1;
                                                       Primer[12] = 1;

    LocalEchoX = 'X';
	LocalEchoX++;
    GlobalEcho = 0;
    Common();
}

/*
*******************************************************************************
 * Description: This function Initialize the calls of all stack-frame functions
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void EchoesOfOsirian(void){

    Echoes00();  

    if(lecter==lecter%5){
        XEchoes();  lecter = 0;
        Common();
    }
}

/*
*******************************************************************************
 * Description:	This function generates a psedu wave
 *
 * Created by: Joakim Larsson
*******************************************************************************
*/

void SetupSinewave(void)
{
    unsigned char x,y,i,upper,down;

    for(x = 0, i = 10, upper = 255, down = 1; x < 255; x++)
    {
        if(upper) y = 200 - (i*i);

        else  y = i * i;

        Sinewave[x] = y;

        if (down){
            if (!i) { i++; down = 0; }

            else i--;
        }

        else {
            if (i == 10) { i--; down = 1; upper = ~upper;}

            else i++;
        }
    }
}

/*
*******************************************************************************
 * Description:	This function sort out maximum and minimum value from a array 
 *              with int. numbers.
 *
 * Created by: Joakim Larsson
*******************************************************************************
*/

void BigSmall(int end[2], int arr[MAXARR_PROF])
{
  int i=0;
  end[0]=end[1]=arr[0];

  for(i = 0; i < MAXARR_PROF; i++){
    end[1] = (end[1] < arr[i]) ? arr[i] : end[1];
    end[0] = (end[0] > arr[i]) ? arr[i] : end[0];
  }
}

/*
*******************************************************************************
 * Description: This function examine and precalculates the partitions of arrys 
 *              for the Sorter() function according to the method: 
 *              "Divide & Conquer"
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

int Partition(int *numbers, int low, int high)
{
  int x = numbers[low];
  int i = low - 1;
  int j = high + 1;

  while (i < j){
    do{ j--; }

    while (numbers[j] > x);
      do{ i++; }

    while(numbers[i] < x);
      if (i < j){
        int tmp = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = tmp;
      } else break;
  }
  return j;
}

/*
*******************************************************************************
 * Description: This function sort the data in a int. array in ascending order.
 *
 * Created by: Joakim Larsson
**************************************************************************
*/

void Sorter(int *numbers, int low, int high)
{
  if (low < high){
    int middle = Partition(numbers,low,high);
    Sorter(numbers,low,middle);
    Sorter(numbers,middle + 1,high);
  }
}

/*
*******************************************************************************
 * Description: This function cast integers to float.
 *
 * Created by: Joakim Larsson
*******************************************************************************
*/

void IntToFloat(int _int_[MAXARR_PROF], float _float_[MAXARR_PROF])
{
  int i;
  float f=0;

  for(i = 0; i < MAXARR_PROF; i++){
    f = ((float) _int_[i] );
    _float_[i]=f/10000;
  }
}

/*
*******************************************************************************
 *  Description:	This function allots precompiled data into memory. This is for
 *                             demonstrating how lauterbach can format an view memory in 
 *                             different ways. In default mode (unchanged) the function shows 
 *                             how to view  the memory location as a JPG via Lauterbach.
 * 
 * Created by: Joakim Larsson
*******************************************************************************
*/

#ifdef COMPILE_ALL
void LoadFractMem(void){

    FractArr[0] 	=	 0xE0FFD8FF;
    FractArr[1] 	=	 0x464A1000;
    FractArr[2] 	=	 0x01004649;
    FractArr[3] 	=	 0x60000101;
    FractArr[4] 	=	 0x00006000;
    FractArr[5] 	=	 0x1600E1FF;
    FractArr[6] 	=	 0x66697845;
    FractArr[7] 	=	 0x49490000;
    FractArr[8] 	=	 0x0008002A;
    FractArr[9] 	=	 0x00000000;
    FractArr[10] 	=	 0x00000000;
    FractArr[11] 	=	 0x4300DBFF;
    FractArr[12] 	=	 0x06060800;
    FractArr[13] 	=	 0x08050607;
    FractArr[14] 	=	 0x09070707;
    FractArr[15] 	=	 0x0C0A0809;
    FractArr[16] 	=	 0x0B0C0D14;
    FractArr[17] 	=	 0x12190C0B;
    FractArr[18] 	=	 0x1D140F13;
    FractArr[19] 	=	 0x1D1E1F1A;
    FractArr[20] 	=	 0x201C1C1A;
    FractArr[21] 	=	 0x20272E24;
    FractArr[22] 	=	 0x1C232C22;
    FractArr[23] 	=	 0x2937281C;
    FractArr[24] 	=	 0x3431302C;
    FractArr[25] 	=	 0x271F3434;
    FractArr[26] 	=	 0x32383D39;
    FractArr[27] 	=	 0x34332E3C;
    FractArr[28] 	=	 0x00DBFF32;
    FractArr[29] 	=	 0x09090143;
    FractArr[30] 	=	 0x0C0B0C09;
    FractArr[31] 	=	 0x180D0D18;
    FractArr[32] 	=	 0x211C2132;
    FractArr[33] 	=	 0x32323232;
    FractArr[34] 	=	 0x32323232;
    FractArr[35] 	=	 0x32323232;
    FractArr[36] 	=	 0x32323232;
    FractArr[37] 	=	 0x32323232;
    FractArr[38] 	=	 0x32323232;
    FractArr[39] 	=	 0x32323232;
    FractArr[40] 	=	 0x32323232;
    FractArr[41] 	=	 0x32323232;
    FractArr[42] 	=	 0x32323232;
    FractArr[43] 	=	 0x32323232;
    FractArr[44] 	=	 0x32323232;
    FractArr[45] 	=	 0xC0FF3232;
    FractArr[46] 	=	 0x00081100;
    FractArr[47] 	=	 0x03FA00FA;
    FractArr[48] 	=	 0x02002201;
    FractArr[49] 	=	 0x11030111;
    FractArr[50] 	=	 0x00C4FF01;
    FractArr[51] 	=	 0x0100001F;
    FractArr[52] 	=	 0x01010105;
    FractArr[53] 	=	 0x00010101;
    FractArr[54] 	=	 0x00000000;
    FractArr[55] 	=	 0x01000000;
    FractArr[56] 	=	 0x05040302;
    FractArr[57] 	=	 0x09080706;
    FractArr[58] 	=	 0xC4FF0B0A;
    FractArr[59] 	=	 0x0010B500;
    FractArr[60] 	=	 0x03030102;
    FractArr[61] 	=	 0x05030402;
    FractArr[62] 	=	 0x00040405;
    FractArr[63] 	=	 0x017D0100;
    FractArr[64] 	=	 0x04000302;
    FractArr[65] 	=	 0x21120511;
    FractArr[66] 	=	 0x13064131;
    FractArr[67] 	=	 0x22076151;
    FractArr[68] 	=	 0x81321471;
    FractArr[69] 	=	 0x2308A191;
    FractArr[70] 	=	 0x15C1B142;
    FractArr[71] 	=	 0x24F0D152;
    FractArr[72] 	=	 0x82726233;
    FractArr[73] 	=	 0x17160A09;
    FractArr[74] 	=	 0x251A1918;
    FractArr[75] 	=	 0x29282726;
    FractArr[76] 	=	 0x3635342A;
    FractArr[77] 	=	 0x3A393837;
    FractArr[78] 	=	 0x46454443;
    FractArr[79] 	=	 0x4A494847;
    FractArr[80] 	=	 0x56555453;
    FractArr[81] 	=	 0x5A595857;
    FractArr[82] 	=	 0x66656463;
    FractArr[83] 	=	 0x6A696867;
    FractArr[84] 	=	 0x76757473;
    FractArr[85] 	=	 0x7A797877;
    FractArr[86] 	=	 0x86858483;
    FractArr[87] 	=	 0x8A898887;
    FractArr[88] 	=	 0x95949392;
    FractArr[89] 	=	 0x99989796;
    FractArr[90] 	=	 0xA4A3A29A;
    FractArr[91] 	=	 0xA8A7A6A5;
    FractArr[92] 	=	 0xB3B2AAA9;
    FractArr[93] 	=	 0xB7B6B5B4;
    FractArr[94] 	=	 0xC2BAB9B8;
    FractArr[95] 	=	 0xC6C5C4C3;
    FractArr[96] 	=	 0xCAC9C8C7;
    FractArr[97] 	=	 0xD5D4D3D2;
    FractArr[98] 	=	 0xD9D8D7D6;
    FractArr[99] 	=	 0xE3E2E1DA;
   /* FractArr[100] 	=	 0xE7E6E5E4;
    FractArr[101] 	=	 0xF1EAE9E8;
    FractArr[102] 	=	 0xF5F4F3F2;
    FractArr[103] 	=	 0xF9F8F7F6;
    FractArr[104] 	=	 0x00C4FFFA;
    FractArr[105] 	=	 0x0300011F;
    FractArr[106] 	=	 0x01010101;
    FractArr[107] 	=	 0x01010101;
    FractArr[108] 	=	 0x00000001;
    FractArr[109] 	=	 0x01000000;
    FractArr[110] 	=	 0x05040302;
    FractArr[111] 	=	 0x09080706;
    FractArr[112] 	=	 0xC4FF0B0A;
    FractArr[113] 	=	 0x0011B500;
    FractArr[114] 	=	 0x04020102;
    FractArr[115] 	=	 0x07040304;
    FractArr[116] 	=	 0x00040405;
    FractArr[117] 	=	 0x00770201;
    FractArr[118] 	=	 0x11030201;
    FractArr[119] 	=	 0x31210504;
    FractArr[120] 	=	 0x51411206;
    FractArr[121] 	=	 0x13716107;
    FractArr[122] 	=	 0x08813222;
    FractArr[123] 	=	 0xA1914214;
    FractArr[124] 	=	 0x2309C1B1;
    FractArr[125] 	=	 0x15F05233;
    FractArr[126] 	=	 0x0AD17262;
    FractArr[127] 	=	 0xE1342416;
    FractArr[128] 	=	 0x1817F125;
    FractArr[129] 	=	 0x27261A19;
    FractArr[130] 	=	 0x352A2928;
    FractArr[131] 	=	 0x39383736;
    FractArr[132] 	=	 0x4544433A;
    FractArr[133] 	=	 0x49484746;
    FractArr[134] 	=	 0x5554534A;
    FractArr[135] 	=	 0x59585756;
    FractArr[136] 	=	 0x6564635A;
    FractArr[137] 	=	 0x69686766;
    FractArr[138] 	=	 0x7574736A;
    FractArr[139] 	=	 0x79787776;
    FractArr[140] 	=	 0x8483827A;
    FractArr[141] 	=	 0x88878685;
    FractArr[142] 	=	 0x93928A89;
    FractArr[143] 	=	 0x97969594;
    FractArr[144] 	=	 0xA29A9998;
    FractArr[145] 	=	 0xA6A5A4A3;
    FractArr[146] 	=	 0xAAA9A8A7;
    FractArr[147] 	=	 0xB5B4B3B2;
    FractArr[148] 	=	 0xB9B8B7B6;
    FractArr[149] 	=	 0xC4C3C2BA;
    FractArr[150] 	=	 0xC8C7C6C5;
    FractArr[151] 	=	 0xD3D2CAC9;
    FractArr[152] 	=	 0xD7D6D5D4;
    FractArr[153] 	=	 0xE2DAD9D8;
    FractArr[154] 	=	 0xE6E5E4E3;
    FractArr[155] 	=	 0xEAE9E8E7;
    FractArr[156] 	=	 0xF5F4F3F2;
    FractArr[157] 	=	 0xF9F8F7F6;
    FractArr[158] 	=	 0x00DAFFFA;
    FractArr[159] 	=	 0x0001030C;
    FractArr[160] 	=	 0x11031102;
    FractArr[161] 	=	 0xF3003F00;
    FractArr[162] 	=	 0x289AD18C;
    FractArr[163] 	=	 0x3C8FD6AF;
    FractArr[164] 	=	 0xA0284A5A;
    FractArr[165] 	=	 0xBFF4B602;
    FractArr[166] 	=	 0xD6BAEB08;
    FractArr[167] 	=	 0x69AA7193;
    FractArr[168] 	=	 0x67710DD6;
    FractArr[169] 	=	 0x7564596E;
    FractArr[170] 	=	 0x54050191;
    FractArr[171] 	=	 0xDC490131;
    FractArr[172] 	=	 0x35A02378;
    FractArr[173] 	=	 0xF0CF5E89;
    FractArr[174] 	=	 0xC38B5ACF;
    FractArr[175] 	=	 0x1D750CDF;
    FractArr[176] 	=	 0x0CDD754E;
    FractArr[177] 	=	 0x9DACAA5A;
    FractArr[178] 	=	 0x51F24678;
    FractArr[179] 	=	 0x18701D8F;
    FractArr[180] 	=	 0x70C5779C;
    FractArr[181] 	=	 0xB89A3866;
    FractArr[182] 	=	 0x95742A7A;
    FractArr[183] 	=	 0xF556DAE4;
    FractArr[184] 	=	 0xEA4D112E;
    FractArr[185] 	=	 0x1F3E5C79;
    FractArr[186] 	=	 0x0B342ED5;
    FractArr[187] 	=	 0x5D2B769D;
    FractArr[188] 	=	 0x82B46DDA;
    FractArr[189] 	=	 0x14F36629;
    FractArr[190] 	=	 0x8DB7636D;
    FractArr[191] 	=	 0xFA7EDCB9;
    FractArr[192] 	=	 0x32EB1DF4;
    FractArr[193] 	=	 0x7AC4C3BD;
    FractArr[194] 	=	 0xE17BF80A;
    FractArr[195] 	=	 0x61AD8B8F;
    FractArr[196] 	=	 0xEA92B640;
    FractArr[197] 	=	 0x8C6D7129;
    FractArr[198] 	=	 0x01CDC660;
    FractArr[199] 	=	 0xCA811DE3;
    FractArr[200] 	=	 0x6AC000FF;
    FractArr[201] 	=	 0x353CD83B;
    FractArr[202] 	=	 0x68113FE1;
    FractArr[203] 	=	 0x0E6F121E;
    FractArr[204] 	=	 0xC48B5E59;
    FractArr[205] 	=	 0xF1DC3D41;
    FractArr[206] 	=	 0xB14BC82B;
    FractArr[207] 	=	 0x7E1E4340;
    FractArr[208] 	=	 0x0EFA24F0;
    FractArr[209] 	=	 0x384BC795;
    FractArr[210] 	=	 0x3C6A8BE7;
    FractArr[211] 	=	 0xB25BBBDA;
    FractArr[212] 	=	 0xAEDFBB49;
    FractArr[213] 	=	 0x3C763AA5;
    FractArr[214] 	=	 0xFEF04D9B;
    FractArr[215] 	=	 0x5F59ACA9;
    FractArr[216] 	=	 0xF95A58DE;
    FractArr[217] 	=	 0xF9F169D0;
    FractArr[218] 	=	 0x2AE62FB7;
    FractArr[219] 	=	 0x39866BF9;
    FractArr[220] 	=	 0x159E20C1;
    FractArr[221] 	=	 0x66A567BA;
    FractArr[222] 	=	 0x77F8B857;
    FractArr[223] 	=	 0x35BCD349;
    FractArr[224] 	=	 0xC016F17B;
    FractArr[225] 	=	 0xB669F3C4;
    FractArr[226] 	=	 0x433BD1F6;
    FractArr[227] 	=	 0xDA68EEBC;
    FractArr[228] 	=	 0x7AA61C29;
    FractArr[229] 	=	 0x396724F4;
    FractArr[230] 	=	 0x69F47AAE;
    FractArr[231] 	=	 0x49F8255E;
    FractArr[232] 	=	 0x41A36BA9;
    FractArr[233] 	=	 0xDBEBB4D3;
    FractArr[234] 	=	 0x368AB51B;
    FractArr[235] 	=	 0x46668BB3;
    FractArr[236] 	=	 0xEB38C7E8;
    FractArr[237] 	=	 0x738EC9F2;
    FractArr[238] 	=	 0xAE1DCFC8;
    FractArr[239] 	=	 0x55E5689E;
    FractArr[240] 	=	 0xB8C15BAE;
    FractArr[241] 	=	 0xC400FFA4;
    FractArr[242] 	=	 0xCFD75793;
    FractArr[243] 	=	 0x0F3D9DA0;
    FractArr[244] 	=	 0xA0AFA42F;
    FractArr[245] 	=	 0xE90F6FB4;
    FractArr[246] 	=	 0xA763CF71;
    FractArr[247] 	=	 0x8717BAEA;
    FractArr[248] 	=	 0x18CC456C;
    FractArr[249] 	=	 0xEDD3C586;
    FractArr[250] 	=	 0xE4428E17;
    FractArr[251] 	=	 0xD38194B1;
    FractArr[252] 	=	 0xA7073807;
    FractArr[253] 	=	 0x742EE45A;
    FractArr[254] 	=	 0xE1CF2E2D;
    FractArr[255] 	=	 0x3BDA8CB7;
    FractArr[256] 	=	 0x9635731B;
    FractArr[257] 	=	 0xF3F6F6B4;
    FractArr[258] 	=	 0x48A561BC;
    FractArr[259] 	=	 0x009448D6;
    FractArr[260] 	=	 0x9E609CE7;
    FractArr[261] 	=	 0x14ADCFFD;
    FractArr[262] 	=	 0x9F738AF3;
    FractArr[263] 	=	 0x2EDD2B2F;
    FractArr[264] 	=	 0xABBF5D9D;
    FractArr[265] 	=	 0x361DD45F;
    FractArr[266] 	=	 0x0DAD2C8F;
    FractArr[267] 	=	 0xF5D44423;
    FractArr[268] 	=	 0x69B2CFEB;
    FractArr[269] 	=	 0xCDDC7276;
    FractArr[270] 	=	 0x8583908C;
    FractArr[271] 	=	 0x009EA41E;
    FractArr[272] 	=	 0x75EC9AFA;
    FractArr[273] 	=	 0x2FC63A7D;
    FractArr[274] 	=	 0xA10E3A82;
    FractArr[275] 	=	 0x5EBA951D;
    FractArr[276] 	=	 0x25C97ECB;
    FractArr[277] 	=	 0x47A2C4C2;
    FractArr[278] 	=	 0x5BF0D35D;
    FractArr[279] 	=	 0xF9812319;
    FractArr[280] 	=	 0xE1974A0F;
    FractArr[281] 	=	 0x5BBAB23E;
    FractArr[282] 	=	 0x6DCB506B;
    FractArr[283] 	=	 0x3017F9A8;
    FractArr[284] 	=	 0x2BDB7BA0;
    FractArr[285] 	=	 0xC0B42973;
    FractArr[286] 	=	 0x1C60337C;
    FractArr[287] 	=	 0x3C383875;
    FractArr[288] 	=	 0xB7664CAF;
    FractArr[289] 	=	 0xD5978DAB;
    FractArr[290] 	=	 0x1BA555EA;
    FractArr[291] 	=	 0xFCACB6B8;
    FractArr[292] 	=	 0x25A4BF9D;
    FractArr[293] 	=	 0x3367521D;
    FractArr[294] 	=	 0xC41FF8AC;
    FractArr[295] 	=	 0xFBB31F9E;
    FractArr[296] 	=	 0xC9A5A766;
    FractArr[297] 	=	 0xA6E0BE0D;
    FractArr[298] 	=	 0x0A249145;
    FractArr[299] 	=	 0x27ED4C4F;
    FractArr[300] 	=	 0xF5738D1F;
    FractArr[301] 	=	 0xD4819AED;
    FractArr[302] 	=	 0xDA3AC1D3;
    FractArr[303] 	=	 0xB5273EE8;
    FractArr[304] 	=	 0xE59F2EF1;
    FractArr[305] 	=	 0xF08EB831;
    FractArr[306] 	=	 0x89B7B897;
    FractArr[307] 	=	 0x0F87E494;
    FractArr[308] 	=	 0x0CB060D4;
    FractArr[309] 	=	 0x38E3843F;
    FractArr[310] 	=	 0xFE93CBC1;
    FractArr[311] 	=	 0x0200FF11;
    FractArr[312] 	=	 0x53C343F8;
    FractArr[313] 	=	 0x6A5AA0CF;
    FractArr[314] 	=	 0x0BB21A97;
    FractArr[315] 	=	 0xA26E898B;
    FractArr[316] 	=	 0xD5A60A59;
    FractArr[317] 	=	 0x849E0B66;
    FractArr[318] 	=	 0xE0760007;
    FractArr[319] 	=	 0x66A8B9E6;
    FractArr[320] 	=	 0x286D9473;
    FractArr[321] 	=	 0x24BB49F3;
    FractArr[322] 	=	 0x555FBAAE;
    FractArr[323] 	=	 0xD0EB572D;
    FractArr[324] 	=	 0xCE7904A7;
    FractArr[325] 	=	 0x53FDE187;
    FractArr[326] 	=	 0x67AF97C4;
    FractArr[327] 	=	 0xA2FDDAA4;
    FractArr[328] 	=	 0xB23223E1;
    FractArr[329] 	=	 0x50988A79;
    FractArr[330] 	=	 0x402C2740;
    FractArr[331] 	=	 0x2BB3C2EA;
    FractArr[332] 	=	 0x0F0FF4DF;
    FractArr[333] 	=	 0xE3737859;
    FractArr[334] 	=	 0x9EBEED1D;
    FractArr[335] 	=	 0xC6BD5D9E;
    FractArr[336] 	=	 0x40046E8E;
    FractArr[337] 	=	 0xAACC84F0;
    FractArr[338] 	=	 0xE7CAF640;
    FractArr[339] 	=	 0x570BAEF1;
    FractArr[340] 	=	 0xF862ACD3;
    FractArr[341] 	=	 0x11EAA023;
    FractArr[342] 	=	 0xECA55BD9;
    FractArr[343] 	=	 0x5C92ECB7;
    FractArr[344] 	=	 0x75244A2C;
    FractArr[345] 	=	 0xB1053FDD;
    FractArr[346] 	=	 0x901F3892;
    FractArr[347] 	=	 0x9AA3AAF4;
    FractArr[348] 	=	 0x4B1295AA;
    FractArr[349] 	=	 0xEFB67CDD;
    FractArr[350] 	=	 0x7E3D29EF;
    FractArr[351] 	=	 0x47763AE1;
    FractArr[352] 	=	 0x3D6D6D05;
    FractArr[353] 	=	 0xDA5BC2E5;
    FractArr[354] 	=	 0xB9F324C1;
    FractArr[355] 	=	 0x161247C2;
    FractArr[356] 	=	 0x72C03E66;
    FractArr[357] 	=	 0xC2DF526B;
    FractArr[358] 	=	 0xD5D0D5DA;
    FractArr[359] 	=	 0xBF5B368C;
    FractArr[360] 	=	 0xBB7D37B2;
    FractArr[361] 	=	 0x9C3CA8F7;
    FractArr[362] 	=	 0xF353CF6E;
    FractArr[363] 	=	 0x9EE9C67D;
    FractArr[364] 	=	 0x253AEC95;
    FractArr[365] 	=	 0xAB998EB5;
    FractArr[366] 	=	 0x5D7B3CFC;
    FractArr[367] 	=	 0x6F39C632;
    FractArr[368] 	=	 0xF384C9F4;
    FractArr[369] 	=	 0xB01C0A2C;
    FractArr[370] 	=	 0x91DF3782;
    FractArr[371] 	=	 0x8C209B8F;
    FractArr[372] 	=	 0xEBEDF09C;
    FractArr[373] 	=	 0x752C8859;
    FractArr[374] 	=	 0xCD24FED9;
    FractArr[375] 	=	 0x8BE9A375;
    FractArr[376] 	=	 0xB10C9D2E;
    FractArr[377] 	=	 0xA8DAF140;
    FractArr[378] 	=	 0x0F6ECA6D;
    FractArr[379] 	=	 0xEFCF4998;
    FractArr[380] 	=	 0x312C0309;
    FractArr[381] 	=	 0xCA4A0F9C;
    FractArr[382] 	=	 0x6EDBBC59;
    FractArr[383] 	=	 0xF3B2B211;
    FractArr[384] 	=	 0x5DBE7CFB;
    FractArr[385] 	=	 0x18CFEC87;
    FractArr[386] 	=	 0x4F66BDA2;
    FractArr[387] 	=	 0x0BFC47F8;
    FractArr[388] 	=	 0x4F0D0FE1;
    FractArr[389] 	=	 0xAA69813E;
    FractArr[390] 	=	 0x2EC86A5C;
    FractArr[391] 	=	 0x89BA252E;
    FractArr[392] 	=	 0x559B2A64;
    FractArr[393] 	=	 0x107A2E98;
    FractArr[394] 	=	 0x83DB011C;
    FractArr[395] 	=	 0xBCD3DD9A;
    FractArr[396] 	=	 0xFFE9A11B;
    FractArr[397] 	=	 0xEC6E1700;
    FractArr[398] 	=	 0x356F9F46;
    FractArr[399] 	=	 0xBB49DA8C;
    FractArr[400] 	=	 0x12E3795B;
    FractArr[401] 	=	 0x55656E24;
    FractArr[402] 	=	 0x8E03433B;
    FractArr[403] 	=	 0x3A92E987;
    FractArr[404] 	=	 0xE1CDB356;
    FractArr[405] 	=	 0xEDE0E414;
    FractArr[406] 	=	 0xF29A9EAD;
    FractArr[407] 	=	 0x7B413FBB;
    FractArr[408] 	=	 0x154D7836;
    FractArr[409] 	=	 0x9A445AEB;
    FractArr[410] 	=	 0x1B3C8B07;
    FractArr[411] 	=	 0xC38FB5E2;
    FractArr[412] 	=	 0x4B431656;
    FractArr[413] 	=	 0x4D5A32B6;
    FractArr[414] 	=	 0x6C6C6212;
    FractArr[415] 	=	 0xA9DFBB72;
    FractArr[416] 	=	 0xCFC9C76C;
    FractArr[417] 	=	 0x77D7C739;
    FractArr[418] 	=	 0x8B1E1AC3;
    FractArr[419] 	=	 0x2AEDE079;
    FractArr[420] 	=	 0xF04A0F5D;
    FractArr[421] 	=	 0x2274A5FE;
    FractArr[422] 	=	 0xEA1D7F53;
    FractArr[423] 	=	 0x50B99086;
    FractArr[424] 	=	 0x8AE14359;
    FractArr[425] 	=	 0x11D4931C;
    FractArr[426] 	=	 0x45EB748C;
    FractArr[427] 	=	 0x4F52DD5C;
    FractArr[428] 	=	 0xED340D9E;
    FractArr[429] 	=	 0xD7BAAAD3;
    FractArr[430] 	=	 0xB3EF5460;
    FractArr[431] 	=	 0xD78A223C;
    FractArr[432] 	=	 0xB4B155F1;
    FractArr[433] 	=	 0x27F435F1;
    FractArr[434] 	=	 0x2ACAFE4A;
    FractArr[435] 	=	 0x342F9BE0;
    FractArr[436] 	=	 0x4165E54B;
    FractArr[437] 	=	 0x07C161C0;
    FractArr[438] 	=	 0xB1C61E39;
    FractArr[439] 	=	 0x35A7D4EB;
    FractArr[440] 	=	 0xBF2E2938;
    FractArr[441] 	=	 0xDE0C4DD7;
    FractArr[442] 	=	 0xA2289A81;
    FractArr[443] 	=	 0x288A02AC;
    FractArr[444] 	=	 0x4A8A02A0;
    FractArr[445] 	=	 0x51124033;
    FractArr[446] 	=	 0x28C01445;
    FractArr[447] 	=	 0x003493A2;
    FractArr[448] 	=	 0x78D9D2B5;
    FractArr[449] 	=	 0x017EECB7;
    FractArr[450] 	=	 0xF62F7CD4;
    FractArr[451] 	=	 0x6C00FF1D;
    FractArr[452] 	=	 0xA7FD13B8;
    FractArr[453] 	=	 0x18CCC6CD;
    FractArr[454] 	=	 0x9FE336CE;
    FractArr[455] 	=	 0x3A727DF5;
    FractArr[456] 	=	 0x453357FB;
    FractArr[457] 	=	 0x2A945267;
    FractArr[458] 	=	 0x66AFA6A4;
    FractArr[459] 	=	 0x346CCD9F;
    FractArr[460] 	=	 0xA7EDD8DA;
    FractArr[461] 	=	 0xD73D8BF8;
    FractArr[462] 	=	 0x45F8BFC3;
    FractArr[463] 	=	 0xC6BC2C2E;
    FractArr[464] 	=	 0xDFAD9108;
    FractArr[465] 	=	 0x38228C9D;
    FractArr[466] 	=	 0xF6361B65;
    FractArr[467] 	=	 0x69AD170A;
    FractArr[468] 	=	 0x1E58FC5A;
    FractArr[469] 	=	 0x9FDA8455;
    FractArr[470] 	=	 0xF5EFAD87;
    FractArr[471] 	=	 0x0529044B;
    FractArr[472] 	=	 0x1CA19CE9;
    FractArr[473] 	=	 0x9E762563;
    FractArr[474] 	=	 0x62FA797D;
    FractArr[475] 	=	 0xB992DEBC;
    FractArr[476] 	=	 0x64E19665;
    FractArr[477] 	=	 0xB7DD709A;
    FractArr[478] 	=	 0xD7EFDDBB;
    FractArr[479] 	=	 0xE78A6DAF;
    FractArr[480] 	=	 0xFCD8D991;
    FractArr[481] 	=	 0x3F86BA42;
    FractArr[482] 	=	 0xA0ED3514;
    FractArr[483] 	=	 0xA2D7B8BA;
    FractArr[484] 	=	 0x9345DEF2;
    FractArr[485] 	=	 0xAEFC10CB;
    FractArr[486] 	=	 0x200703A3;
    FractArr[487] 	=	 0x7D641C07;
    FractArr[488] 	=	 0xCB4AB5DE;
    FractArr[489] 	=	 0xF063BFC5;
    FractArr[490] 	=	 0x6FE1A306;
    FractArr[491] 	=	 0x6DFBEFB0;
    FractArr[492] 	=	 0x3EED9FC0;
    FractArr[493] 	=	 0xC660366E;
    FractArr[494] 	=	 0xFF1CB771;
    FractArr[495] 	=	 0x91EBAB00;
    FractArr[496] 	=	 0x9AB9DAD7;
    FractArr[497] 	=	 0x50A94F2B;
    FractArr[498] 	=	 0xF5A91EE9;
    FractArr[499] 	=	 0xD67D3BDE;
    FractArr[500] 	=	 0x3D3D3317;
    FractArr[501] 	=	 0xD7C62F7E;
    FractArr[502] 	=	 0xFE9C9AD6;
    FractArr[503] 	=	 0x5693B719;
    FractArr[504] 	=	 0xAE211F82;
    FractArr[505] 	=	 0x901BC9C5;
    FractArr[506] 	=	 0xB70BB09C;
    FractArr[507] 	=	 0x938CF38C;
    FractArr[508] 	=	 0x9A539F8C;
    FractArr[509] 	=	 0x6D12BF16;
    FractArr[510] 	=	 0xD2725D63;
    FractArr[511] 	=	 0xDDB1C3EB;
    FractArr[512] 	=	 0xCBB59A69;
    FractArr[513] 	=	 0xDDAB3D5D;
    FractArr[514] 	=	 0x1BDB5110;
    FractArr[515] 	=	 0xF2BC7DBE;
    FractArr[516] 	=	 0x47008CA0;
    FractArr[517] 	=	 0x68FE79AD;
    FractArr[518] 	=	 0x845756AC;
    FractArr[519] 	=	 0x7DF1475B;
    FractArr[520] 	=	 0xD3B5D51D;
    FractArr[521] 	=	 0x92F6C35E;
    FractArr[522] 	=	 0x78145F3B;
    FractArr[523] 	=	 0xF8102FFA;
    FractArr[524] 	=	 0x8342CF5A;
    FractArr[525] 	=	 0x1BC28E45;
    FractArr[526] 	=	 0x62A8C459;
    FractArr[527] 	=	 0x0185729B;
    FractArr[528] 	=	 0xA3DD2ED4;
    FractArr[529] 	=	 0x6FCF75B3;
    FractArr[530] 	=	 0x6FF0CE7A;
    FractArr[531] 	=	 0x1B3CAF8C;
    FractArr[532] 	=	 0x6F714BA9;
    FractArr[533] 	=	 0x3A415C0A;
    FractArr[534] 	=	 0x38079A6C;
    FractArr[535] 	=	 0x0F42070E;
    FractArr[536] 	=	 0x7A7D3262;
    FractArr[537] 	=	 0xADE8E69A;
    FractArr[538] 	=	 0xA9A08263;
    FractArr[539] 	=	 0xEBBE3C3A;
    FractArr[540] 	=	 0x672E6ADD;
    FractArr[541] 	=	 0xAB8B9E7B;
    FractArr[542] 	=	 0xAD5B54FC;
    FractArr[543] 	=	 0xD14BF30E;
    FractArr[544] 	=	 0xA4673BFC;
    FractArr[545] 	=	 0xB865E8A5;
    FractArr[546] 	=	 0x7031D878;
    FractArr[547] 	=	 0x55E186C0;
    FractArr[548] 	=	 0xCDB92379;
    FractArr[549] 	=	 0x243ED257;
    FractArr[550] 	=	 0x6387B6A5;
    FractArr[551] 	=	 0x0C3A6BA6;
    FractArr[552] 	=	 0xEEE9B21A;
    FractArr[553] 	=	 0x9846D21A;
    FractArr[554] 	=	 0xDDC763C6;
    FractArr[555] 	=	 0x8E73CACF;
    FractArr[556] 	=	 0x70C5B89D;
    FractArr[557] 	=	 0xD97F5654;
    FractArr[558] 	=	 0x450E5598;
    FractArr[559] 	=	 0xDD7D2F1D;
    FractArr[560] 	=	 0xFE7BEBEF;
    FractArr[561] 	=	 0xEE95E723;
    FractArr[562] 	=	 0xC577DA77;
    FractArr[563] 	=	 0x1B6FCB1B;
    FractArr[564] 	=	 0xF292F85D;
    FractArr[565] 	=	 0x6B5A2EC1;
    FractArr[566] 	=	 0xBA1D6B53;
    FractArr[567] 	=	 0x9388E54B;
    FractArr[568] 	=	 0xC1C1B072;
    FractArr[569] 	=	 0x523FDDCF;
    FractArr[570] 	=	 0x5F954E4F;
    FractArr[571] 	=	 0xADF5F148;
    FractArr[572] 	=	 0xF0A284AF;
    FractArr[573] 	=	 0xF8E1AFEE;
    FractArr[574] 	=	 0x25584B35;
    FractArr[575] 	=	 0x735AC232;
    FractArr[576] 	=	 0xB7B85319;
    FractArr[577] 	=	 0x37BB0738;
    FractArr[578] 	=	 0xF5383842;
    FractArr[579] 	=	 0x374513CF;
    FractArr[580] 	=	 0xB79AE197;
    FractArr[581] 	=	 0x5B9B6D2F;
    FractArr[582] 	=	 0x973EDB5E;
    FractArr[583] 	=	 0xE891E762;
    FractArr[584] 	=	 0x1500FFB7;
    FractArr[585] 	=	 0xBCD6BC64;
    FractArr[586] 	=	 0xA3C7A93F;
    FractArr[587] 	=	 0x02690B47;
    FractArr[588] 	=	 0xF92CE551;
    FractArr[589] 	=	 0x14884459;
    FractArr[590] 	=	 0xA0FCF281;
    FractArr[591] 	=	 0xAFD77100;
    FractArr[592] 	=	 0x227E40B5;
    FractArr[593] 	=	 0x497CC4D9;
    FractArr[594] 	=	 0x7D0F3FF6;
    FractArr[595] 	=	 0xDC76FB9F;
    FractArr[596] 	=	 0x69FA37C7;
    FractArr[597] 	=	 0x331FB27D;
    FractArr[598] 	=	 0x39AF9C74;
    FractArr[599] 	=	 0xC0E83EF3;
    FractArr[600] 	=	 0xE0F87D1B;
    FractArr[601] 	=	 0x6E59A928;
    FractArr[602] 	=	 0x74942415;
    FractArr[603] 	=	 0xF3FB6E5A;
    FractArr[604] 	=	 0xF9A8EF77;
    FractArr[605] 	=	 0x47DA77E4;
    FractArr[606] 	=	 0xD0B694C4;
    FractArr[607] 	=	 0x67CD74EC;
    FractArr[608] 	=	 0x5D568341;
    FractArr[609] 	=	 0x485AC33D;
    FractArr[610] 	=	 0x78CC18D3;
    FractArr[611] 	=	 0x4EF9B9FB;
    FractArr[612] 	=	 0x18B7D371;
    FractArr[613] 	=	 0xA3F84EA3;
    FractArr[614] 	=	 0x6BE36D79;
    FractArr[615] 	=	 0x585E12BF;
    FractArr[616] 	=	 0x6A4DCB25;
    FractArr[617] 	=	 0x49B7636D;
    FractArr[618] 	=	 0x6E12B17C;
    FractArr[619] 	=	 0x39381856;
    FractArr[620] 	=	 0x49EAA7FB;
    FractArr[621] 	=	 0x1AB8D2E9;
    FractArr[622] 	=	 0x0AB7BC29;
    FractArr[623] 	=	 0x8A0FAFDC;
    FractArr[624] 	=	 0xDEF5D5F7;
    FractArr[625] 	=	 0x2B7FE9DA;
    FractArr[626] 	=	 0x4D479E0B;
    FractArr[627] 	=	 0x7F17AFE1;
    FractArr[628] 	=	 0xEBA23BC2;
    FractArr[629] 	=	 0x7ED877FA;
    FractArr[630] 	=	 0x6FADFDD1;
    FractArr[631] 	=	 0x6E9E79E4;
    FractArr[632] 	=	 0x75E52BCF;
    FractArr[633] 	=	 0xDF9D36CE;
    FractArr[634] 	=	 0x2B1DA77F;
    FractArr[635] 	=	 0xEEF1434F;
    FractArr[636] 	=	 0xE95AA597;
    FractArr[637] 	=	 0xF06800FF;
    FractArr[638] 	=	 0xD6F78C8D;
    FractArr[639] 	=	 0x21AF1824;
    FractArr[640] 	=	 0x2076C893;
    FractArr[641] 	=	 0xA7DA1760;
    FractArr[642] 	=	 0xE7A9C771;
    FractArr[643] 	=	 0xA8E15AAF;
    FractArr[644] 	=	 0xA182A9AB;
    FractArr[645] 	=	 0x7CC9C951;
    FractArr[646] 	=	 0x4BD7EA5B;
    FractArr[647] 	=	 0x49817D74;
    FractArr[648] 	=	 0x3AC44FA3;
    FractArr[649] 	=	 0xD723FEDC;
    FractArr[650] 	=	 0x946BF5AE;
    FractArr[651] 	=	 0x81E19644;
    FractArr[652] 	=	 0x00149D28;
    FractArr[653] 	=	 0x0AC80F14;
    FractArr[654] 	=	 0x35A3A4CC;
    FractArr[655] 	=	 0x114608D3;
    FractArr[656] 	=	 0x5AB28A50;
    FractArr[657] 	=	 0xA4C5DD12;
    FractArr[658] 	=	 0x0CA08AA2;
    FractArr[659] 	=	 0x004549D1;
    FractArr[660] 	=	 0x5014252D;
    FractArr[661] 	=	 0x8AA29904;
    FractArr[662] 	=	 0x142D604A;
    FractArr[663] 	=	 0x45015094;
    FractArr[664] 	=	 0x0A80A219;
    FractArr[665] 	=	 0x00144533;
    FractArr[666] 	=	 0x80144551;
    FractArr[667] 	=	 0x008AA228;
    FractArr[668] 	=	 0x80A2A428;
    FractArr[669] 	=	 0x288A9216;
    FractArr[670] 	=	 0x288AA200;
    FractArr[671] 	=	 0x8C92A200;
    FractArr[672] 	=	 0x51D202D0;
    FractArr[673] 	=	 0xA2190045;
    FractArr[674] 	=	 0x5A008A92;
    FractArr[675] 	=	 0x02A0284A;
    FractArr[676] 	=	 0x02A0288A;
    FractArr[677] 	=	 0x01A4288A;
    FractArr[678] 	=	 0x4D81074E;
    FractArr[679] 	=	 0x2580E6A6;
    FractArr[680] 	=	 0x802A8AA2;
    FractArr[681] 	=	 0xA4E5F0D3;
    FractArr[682] 	=	 0xD227FE17;
    FractArr[683] 	=	 0x7D53EE6C;
    FractArr[684] 	=	 0x31ECC5BD;
    FractArr[685] 	=	 0x7223994A;
    FractArr[686] 	=	 0x474680B3;
    FractArr[687] 	=	 0xB4DA8323;
    FractArr[688] 	=	 0x566920BE;
    FractArr[689] 	=	 0xF58D275A;
    FractArr[690] 	=	 0x874E3B1D;
    FractArr[691] 	=	 0xCA87B4C9;
    FractArr[692] 	=	 0x6CF61ED9;
    FractArr[693] 	=	 0xCB633566;
    FractArr[694] 	=	 0x54937A12;
    FractArr[695] 	=	 0x00FF23FC;
    FractArr[696] 	=	 0xFF85A623;
    FractArr[697] 	=	 0x7F1B6100;
    FractArr[698] 	=	 0xEAB518FD;
    FractArr[699] 	=	 0x57F8353E;
    FractArr[700] 	=	 0xC59378AE;
    FractArr[701] 	=	 0x9DB5DAD7;
    FractArr[702] 	=	 0xF7969CD6;
    FractArr[703] 	=	 0x9AD55E1E;
    FractArr[704] 	=	 0xC6360C47;
    FractArr[705] 	=	 0x110272AA;
    FractArr[706] 	=	 0xF27A4FD5;
    FractArr[707] 	=	 0x61B818B1;
    FractArr[708] 	=	 0xB3F691F1;
    FractArr[709] 	=	 0xA55F5CB4;
    FractArr[710] 	=	 0x4813BFEE;
    FractArr[711] 	=	 0x79E8B8C5;
    FractArr[712] 	=	 0xEBE081B6;
    FractArr[713] 	=	 0xE3C27EFD;
    FractArr[714] 	=	 0x0BAB4B50;
    FractArr[715] 	=	 0x461C181B;
    FractArr[716] 	=	 0xF9D337F7;
    FractArr[717] 	=	 0xC33FEF51;
    FractArr[718] 	=	 0x1F791E9C;
    FractArr[719] 	=	 0xFE09AD98;
    FractArr[720] 	=	 0xFE96EB1A;
    FractArr[721] 	=	 0x67B4D322;
    FractArr[722] 	=	 0xA2BEC896;
    FractArr[723] 	=	 0x2BD7F68E;
    FractArr[724] 	=	 0x55B83029;
    FractArr[725] 	=	 0x9EDB792C;
    FractArr[726] 	=	 0x57DCED98;
    FractArr[727] 	=	 0x052DE05F;
    FractArr[728] 	=	 0x116F746C;
    FractArr[729] 	=	 0x3A2D4B0B;
    FractArr[730] 	=	 0x5AC500FF;
    FractArr[731] 	=	 0x12C1E375;
    FractArr[732] 	=	 0x0BA8C0CE;
    FractArr[733] 	=	 0x8CDD9581;
    FractArr[734] 	=	 0x1ECC7C02;
    FractArr[735] 	=	 0x3B128C33;
    FractArr[736] 	=	 0x8FC47A75;
    FractArr[737] 	=	 0x077C8A17;
    FractArr[738] 	=	 0x03717B7D;
    FractArr[739] 	=	 0xDEE66624;
    FractArr[740] 	=	 0x1108E5E2;
    FractArr[741] 	=	 0x5008EFF9;
    FractArr[742] 	=	 0x5859A607;
    FractArr[743] 	=	 0xE08AED01;
    FractArr[744] 	=	 0x8DD69AAF;
    FractArr[745] 	=	 0x569B4279;
    FractArr[746] 	=	 0xB7A6ADB4;
    FractArr[747] 	=	 0xDFFBBA51;
    FractArr[748] 	=	 0xA914D1DF;
    FractArr[749] 	=	 0x339E5DAB;
    FractArr[750] 	=	 0x531F78A3;
    FractArr[751] 	=	 0xA84FFCD6;
    FractArr[752] 	=	 0xDAD31668;
    FractArr[753] 	=	 0x998FDD25;
    FractArr[754] 	=	 0xC18EBCE6;
    FractArr[755] 	=	 0x7008C70E;
    FractArr[756] 	=	 0xF1D49342;
    FractArr[757] 	=	 0xEBAD3890;
    FractArr[758] 	=	 0x30F6878F;
    FractArr[759] 	=	 0xD7B631FC;
    FractArr[760] 	=	 0x17B354D7;
    FractArr[761] 	=	 0xB4F8FED3;
    FractArr[762] 	=	 0x608F18B7;
    FractArr[763] 	=	 0x7C4C3E57;
    FractArr[764] 	=	 0x009CD27C;
    FractArr[765] 	=	 0x0CA4EE41;
    FractArr[766] 	=	 0xF0BFE760;
    FractArr[767] 	=	 0x0DB5848F;
    FractArr[768] 	=	 0x8857E21F;
    FractArr[769] 	=	 0x81C77B35;
    FractArr[770] 	=	 0x8DF3BB61;
    FractArr[771] 	=	 0x9A81ACB2;
    FractArr[772] 	=	 0xF7557945;
    FractArr[773] 	=	 0x8003A863;
    FractArr[774] 	=	 0x27FEDC73;
    FractArr[775] 	=	 0xEED3B991;
    FractArr[776] 	=	 0x5A807F75;
    FractArr[777] 	=	 0x26C6D852;
    FractArr[778] 	=	 0x6EA91B6B;
    FractArr[779] 	=	 0x1E75402E;
    FractArr[780] 	=	 0x24666A5C;
    FractArr[781] 	=	 0x87F69F9C;
    FractArr[782] 	=	 0x4B699E03;
    FractArr[783] 	=	 0xB15A9D31;
    FractArr[784] 	=	 0xBC519A50;
    FractArr[785] 	=	 0x9B54FD2E;
    FractArr[786] 	=	 0x440E52FC;
    FractArr[787] 	=	 0x83B9AB96;
    FractArr[788] 	=	 0xC4B3F069;
    FractArr[789] 	=	 0x377C7A37;
    FractArr[790] 	=	 0xB862AC21;
    FractArr[791] 	=	 0x3634139E;
    FractArr[792] 	=	 0xE1B64F33;
    FractArr[793] 	=	 0x8C0B77D4;
    FractArr[794] 	=	 0xC6481D77;
    FractArr[795] 	=	 0x74557146;
    FractArr[796] 	=	 0xB69A872F;
    FractArr[797] 	=	 0xEA5DA4BF;
    FractArr[798] 	=	 0xDA585C51;
    FractArr[799] 	=	 0x413B6941;
    FractArr[800] 	=	 0xA391BC38;
    FractArr[801] 	=	 0x2C85CA68;
    FractArr[802] 	=	 0x0040CA4F;
    FractArr[803] 	=	 0x8D3D3937;
    FractArr[804] 	=	 0x77A30E7B;
    FractArr[805] 	=	 0x9AD76AA9;
    FractArr[806] 	=	 0x7BE1BD6E;
    FractArr[807] 	=	 0x6CCF0D5F;
    FractArr[808] 	=	 0x863A496D;
    FractArr[809] 	=	 0xB97D87A0;
    FractArr[810] 	=	 0xA937F919;
    FractArr[811] 	=	 0x8C1806DC;
    FractArr[812] 	=	 0x3D1B8263;
    FractArr[813] 	=	 0xF28A8C6B;
    FractArr[814] 	=	 0x780FFE69;
    FractArr[815] 	=	 0x88A5EDCE;
    FractArr[816] 	=	 0xECB036CB;
    FractArr[817] 	=	 0x8D486CEF;
    FractArr[818] 	=	 0xB63B3CB7;
    FractArr[819] 	=	 0x27A79DE7;
    FractArr[820] 	=	 0x2ACD63AF;
    FractArr[821] 	=	 0xA42A9659;
    FractArr[822] 	=	 0x15B74977;
    FractArr[823] 	=	 0x66EDAEE6;
    FractArr[824] 	=	 0x0837A6BF;
    FractArr[825] 	=	 0xF097BEA2;
    FractArr[826] 	=	 0xC0EBCAEA;
    FractArr[827] 	=	 0xE932D9DA;
    FractArr[828] 	=	 0xB7D6EAF3;
    FractArr[829] 	=	 0x296AB16A;
    FractArr[830] 	=	 0x5867DF76;
    FractArr[831] 	=	 0x77ECC2B6;
    FractArr[832] 	=	 0x6705A470;
    FractArr[833] 	=	 0x5CE98FE4;
    FractArr[834] 	=	 0x5DE0B78E;
    FractArr[835] 	=	 0x4F9B4367;
    FractArr[836] 	=	 0x91F71656;
    FractArr[837] 	=	 0xD656056A;
    FractArr[838] 	=	 0xF14DCEE2;
    FractArr[839] 	=	 0x43D1CDC8;
    FractArr[840] 	=	 0x04393910;
    FractArr[841] 	=	 0x9CE03976;
    FractArr[842] 	=	 0x063E771C;
    FractArr[843] 	=	 0x9F352ED3;
    FractArr[844] 	=	 0x9FF63A83;
    FractArr[845] 	=	 0x3CC33A6B;
    FractArr[846] 	=	 0x9951C5F7;
    FractArr[847] 	=	 0xC27682B6;
    FractArr[848] 	=	 0xD6FE9376;
    FractArr[849] 	=	 0xEF34FE36;
    FractArr[850] 	=	 0xA4694111;
    FractArr[851] 	=	 0xBEC137F8;
    FractArr[852] 	=	 0x90AED715;
    FractArr[853] 	=	 0xA6E80BDD;
    FractArr[854] 	=	 0xCCC98CBB;
    FractArr[855] 	=	 0xC07D1650;
    FractArr[856] 	=	 0x3E007890;
    FractArr[857] 	=	 0x27FC73D0;
    FractArr[858] 	=	 0x58BA381D;
    FractArr[859] 	=	 0x12F1D4CA;
    FractArr[860] 	=	 0xEFCDCFA7;
    FractArr[861] 	=	 0x6E6B6B35;
    FractArr[862] 	=	 0x5F79DD5B;
    FractArr[863] 	=	 0x57539CA0;
    FractArr[864] 	=	 0x51F8A639;
    FractArr[865] 	=	 0x48B995AD;
    FractArr[866] 	=	 0x9B7BF42F;
    FractArr[867] 	=	 0x2DDF64CB;
    FractArr[868] 	=	 0xA6453794;
    FractArr[869] 	=	 0x70BB624F;
    FractArr[870] 	=	 0x5FD6D40F;
    FractArr[871] 	=	 0xAA09FC87;
    FractArr[872] 	=	 0x9B4C83F8;
    FractArr[873] 	=	 0xECAC4953;
    FractArr[874] 	=	 0xD38EF874;
    FractArr[875] 	=	 0x8A297D75;
    FractArr[876] 	=	 0xE0E07B32;
    FractArr[877] 	=	 0x5EE9F5F4;
    FractArr[878] 	=	 0x5CDBA4F9;
    FractArr[879] 	=	 0x96239E69;
    FractArr[880] 	=	 0x4DD7DED2;
    FractArr[881] 	=	 0xCE3ED0B2;
    FractArr[882] 	=	 0x0A60CDA2;
    FractArr[883] 	=	 0x2C4F791E;
    FractArr[884] 	=	 0x6E003D4E;
    FractArr[885] 	=	 0xF51CE4DC;
    FractArr[886] 	=	 0xE9D99CC7;
    FractArr[887] 	=	 0xF8265E57;
    FractArr[888] 	=	 0x34871F45;
    FractArr[889] 	=	 0x1A6B14D9;
    FractArr[890] 	=	 0xAB9BEB5D;
    FractArr[891] 	=	 0xDF8C7036;
    FractArr[892] 	=	 0xC9299F3C;
    FractArr[893] 	=	 0xCF81F803;
    FractArr[894] 	=	 0x713BA64C;
    FractArr[895] 	=	 0xBCDA1C95;
    FractArr[896] 	=	 0x92A4F9E2;
    FractArr[897] 	=	 0x9DEC75BC;
    FractArr[898] 	=	 0x894EDFEE;
    FractArr[899] 	=	 0x37565F79;
    FractArr[900] 	=	 0x8387224D;
    FractArr[901] 	=	 0xBE441F3E;
    FractArr[902] 	=	 0xFCD0BA3C;
    FractArr[903] 	=	 0x0897664F;
    FractArr[904] 	=	 0x3FE60696;
    FractArr[905] 	=	 0xDF0A662E;
    FractArr[906] 	=	 0x94C1003A;
    FractArr[907] 	=	 0x392C4782;
    FractArr[908] 	=	 0x86CF3BAC;
    FractArr[909] 	=	 0x89A6E55A;
    FractArr[910] 	=	 0xFAF9A975;
    FractArr[911] 	=	 0x77CDE274;
    FractArr[912] 	=	 0x73C15B5D;
    FractArr[913] 	=	 0x323058BE;
    FractArr[914] 	=	 0x721B80C1;
    FractArr[915] 	=	 0xE9672407;
    FractArr[916] 	=	 0x03BCEE9A;
    FractArr[917] 	=	 0xFCA2CDA4;
    FractArr[918] 	=	 0xB9B1BA50;
    FractArr[919] 	=	 0x3AA7C6D5;
    FractArr[920] 	=	 0x3C00FF68;
    FractArr[921] 	=	 0x47798B9B;
    FractArr[922] 	=	 0x7CF78FCC;
    FractArr[923] 	=	 0x11F9D393;
    FractArr[924] 	=	 0x00FFB9D2;
    FractArr[925] 	=	 0x78F2EE86;
    FractArr[926] 	=	 0x9204E23B;
    FractArr[927] 	=	 0x9FB63B33;
    FractArr[928] 	=	 0x24396696;
    FractArr[929] 	=	 0x9AE4E798;
    FractArr[930] 	=	 0xC6EBE2D1;
    FractArr[931] 	=	 0xDD19AB53;
    FractArr[932] 	=	 0xB3A34F25;
    FractArr[933] 	=	 0x7A97B6BB;
    FractArr[934] 	=	 0x950BC37F;
    FractArr[935] 	=	 0xF3DCAD68;
    FractArr[936] 	=	 0x7E5AD12C;
    FractArr[937] 	=	 0xB4EDD41F;
    FractArr[938] 	=	 0xFEDA727D;
    FractArr[939] 	=	 0x52874DF3;
    FractArr[940] 	=	 0xEB768BB7;
    FractArr[941] 	=	 0x4CB6B159;
    FractArr[942] 	=	 0x043903A9;
    FractArr[943] 	=	 0x3B3D4870;
    FractArr[944] 	=	 0x6B159F55;
    FractArr[945] 	=	 0xD5203E96;
    FractArr[946] 	=	 0xF4D3BB22;
    FractArr[947] 	=	 0x24227D4B;
    FractArr[948] 	=	 0xB7604684;
    FractArr[949] 	=	 0x3398B5DB;
    FractArr[950] 	=	 0xF32ADC1D;
    FractArr[951] 	=	 0xD54E0782;
    FractArr[952] 	=	 0xDACF39EF;
    FractArr[953] 	=	 0xB6FB72A8;
    FractArr[954] 	=	 0xB6EEEBDE;
    FractArr[955] 	=	 0x3D5656E6;
    FractArr[956] 	=	 0x677AC00F;
    FractArr[957] 	=	 0xF915DFC3;
    FractArr[958] 	=	 0x0DF65F1A;
    FractArr[959] 	=	 0xB2A1D4D9;
    FractArr[960] 	=	 0x96666E59;
    FractArr[961] 	=	 0x1B765467;
    FractArr[962] 	=	 0x93EDCA55;
    FractArr[963] 	=	 0x034736BB;
    FractArr[964] 	=	 0x2FE64A8F;
    FractArr[965] 	=	 0xE3373C6C;
    FractArr[966] 	=	 0xD24A730D;
    FractArr[967] 	=	 0x4DA7137C;
    FractArr[968] 	=	 0xE74BDCA6;
    FractArr[969] 	=	 0x237DCD79;
    FractArr[970] 	=	 0x65286C6D;
    FractArr[971] 	=	 0x809173E7;
    FractArr[972] 	=	 0xFCB8DBAF;
    FractArr[973] 	=	 0x7F07FE34;
    FractArr[974] 	=	 0xFF79EBC8;
    FractArr[975] 	=	 0xFFE76000;
    FractArr[976] 	=	 0xD691D100;
    FractArr[977] 	=	 0xF98FC237;
    FractArr[978] 	=	 0xFD477A29;
    FractArr[979] 	=	 0xD100FFB6;
    FractArr[980] 	=	 0xA32C5E2F;
    FractArr[981] 	=	 0xCEAB353A;
    FractArr[982] 	=	 0x8DE49333;
    FractArr[983] 	=	 0x76B56DD2;
    FractArr[984] 	=	 0x6BE4D3A5;
    FractArr[985] 	=	 0x89DB4ABA;
    FractArr[986] 	=	 0x57D7F07D;
    FractArr[987] 	=	 0x4F83BFB2;
    FractArr[988] 	=	 0xEA2AED4B;
    FractArr[989] 	=	 0x07816BFE;
    FractArr[990] 	=	 0x4BEE6DD9;
    FractArr[991] 	=	 0xCD2A1949;
    FractArr[992] 	=	 0x6A03C1B9;
    FractArr[993] 	=	 0xE97352ED;
    FractArr[994] 	=	 0xE1FB96DA;
    FractArr[995] 	=	 0xA467ADA6;
    FractArr[996] 	=	 0x6A30EADE;
    FractArr[997] 	=	 0x76FC4D1A;
    FractArr[998] 	=	 0xEC989B20;
    FractArr[999] 	=	 0x168F8CAE;
    FractArr[1000] 	=	 0x401BEE3A;
    FractArr[1001] 	=	 0x9C917304;
    FractArr[1002] 	=	 0xA0AD56F1;
    FractArr[1003] 	=	 0x36BED4B0;
    FractArr[1004] 	=	 0x32ADC1DC;
    FractArr[1005] 	=	 0xA5FA68C9;
    FractArr[1006] 	=	 0xF099B0C2;
    FractArr[1007] 	=	 0x14FB82A4;
    FractArr[1008] 	=	 0x18854C9F;
    FractArr[1009] 	=	 0x27D62BFC;
    FractArr[1010] 	=	 0xB13FD5B4;
    FractArr[1011] 	=	 0xBDA5573C;
    FractArr[1012] 	=	 0xBBBD85A5;
    FractArr[1013] 	=	 0x95164B5A;
    FractArr[1014] 	=	 0x33146869;
    FractArr[1015] 	=	 0xDCB0E5C7;
    FractArr[1016] 	=	 0xA32CF540;
    FractArr[1017] 	=	 0xDC231818;
    FractArr[1018] 	=	 0xC79C1813;
    FractArr[1019] 	=	 0x39C14111;
    FractArr[1020] 	=	 0xE94DDA26;
    FractArr[1021] 	=	 0xB59DDD6D;
    FractArr[1022] 	=	 0x6E2DF8E9;
    FractArr[1023] 	=	 0x3C778211;
    FractArr[1024] 	=	 0xC000FFB6;
    FractArr[1025] 	=	 0xE1C55FD2;
    FractArr[1026] 	=	 0xC23A3D0B;
    FractArr[1027] 	=	 0x56EBC6C6;
    FractArr[1028] 	=	 0xF33C79B4;
    FractArr[1029] 	=	 0x62822C7B;
    FractArr[1030] 	=	 0x759623B1;
    FractArr[1031] 	=	 0x82B3FB29;
    FractArr[1032] 	=	 0xB68C5C78;
    FractArr[1033] 	=	 0xFE870A3B;
    FractArr[1034] 	=	 0x4A881F15;
    FractArr[1035] 	=	 0x3A77ACDD;
    FractArr[1036] 	=	 0xA056975C;
    FractArr[1037] 	=	 0x9C4BB497;
    FractArr[1038] 	=	 0xD34E46CA;
    FractArr[1039] 	=	 0xC606608C;
    FractArr[1040] 	=	 0xC7F74846;
    FractArr[1041] 	=	 0x9F56E835;
    FractArr[1042] 	=	 0x55F816F2;
    FractArr[1043] 	=	 0xE96000FF;
    FractArr[1044] 	=	 0x95F400FF;
    FractArr[1045] 	=	 0xFBE11F2B;
    FractArr[1046] 	=	 0x3C1B9FBC;
    FractArr[1047] 	=	 0x88995D54;
    FractArr[1048] 	=	 0xE7244317;
    FractArr[1049] 	=	 0x0F807081;
    FractArr[1050] 	=	 0x152B00C0;
    FractArr[1051] 	=	 0x9CC6C48F;
    FractArr[1052] 	=	 0x16C35F9C;
    FractArr[1053] 	=	 0xFCFED6F5;
    FractArr[1054] 	=	 0x43F6ED96;
    FractArr[1055] 	=	 0x00FF8DE4;
    FractArr[1056] 	=	 0x3C22C7AE;
    FractArr[1057] 	=	 0x073C2F28;
    FractArr[1058] 	=	 0x7DABEBE1;
    FractArr[1059] 	=	 0xBD1BD23A;
    FractArr[1060] 	=	 0x8B16514B;
    FractArr[1061] 	=	 0x2C256F7C;
    FractArr[1062] 	=	 0x01459EE5;
    FractArr[1063] 	=	 0x47B5DBA2;
    FractArr[1064] 	=	 0x9F0439CA;
    FractArr[1065] 	=	 0xABC97194;
    FractArr[1066] 	=	 0x88F7E003;
    FractArr[1067] 	=	 0x9325DD3E;
    FractArr[1068] 	=	 0x700B695F;
    FractArr[1069] 	=	 0x6E447AA3;
    FractArr[1070] 	=	 0x0353E94E;
    FractArr[1071] 	=	 0x38B7AB2C;
    FractArr[1072] 	=	 0x0E24E304;
    FractArr[1073] 	=	 0xE909A647;
    FractArr[1074] 	=	 0x26F9BF74;
    FractArr[1075] 	=	 0xECBF0F9F;
    FractArr[1076] 	=	 0x00FF8B62;
    FractArr[1077] 	=	 0xCE4A4D47;
    FractArr[1078] 	=	 0xAA4AFBEF;
    FractArr[1079] 	=	 0x300831B3;
    FractArr[1080] 	=	 0x3EBA9FA0;
    FractArr[1081] 	=	 0xE2074EC8;
    FractArr[1082] 	=	 0x476BFC49;
    FractArr[1083] 	=	 0x5429C48D;
    FractArr[1084] 	=	 0x7DB99550;
    FractArr[1085] 	=	 0x855FD7A3;
    FractArr[1086] 	=	 0x7F7AB5E8;
    FractArr[1087] 	=	 0xD22A075A;
    FractArr[1088] 	=	 0x689F47FE;
    FractArr[1089] 	=	 0x75D6049E;
    FractArr[1090] 	=	 0x5114F5B9;
    FractArr[1091] 	=	 0x9C16676F;
    FractArr[1092] 	=	 0x2FAE6E59;
    FractArr[1093] 	=	 0x6C14D924;
    FractArr[1094] 	=	 0x07B054BD;
    FractArr[1095] 	=	 0x604F8291;
    FractArr[1096] 	=	 0x99912707;
    FractArr[1097] 	=	 0xFFF0457C;
    FractArr[1098] 	=	 0xC6F05600;
    FractArr[1099] 	=	 0x7BAD0689;
    FractArr[1100] 	=	 0xBC256371;
    FractArr[1101] 	=	 0x5B5008D3;
    FractArr[1102] 	=	 0x43925CCA;
    FractArr[1103] 	=	 0x0C74DF10;
    FractArr[1104] 	=	 0xDCEBB910;
    FractArr[1105] 	=	 0x2759A157;
    FractArr[1106] 	=	 0xF18BEEF6;
    FractArr[1107] 	=	 0x1756C303;
    FractArr[1108] 	=	 0x9ABC9A0A;
    FractArr[1109] 	=	 0x71C4C5B5;
    FractArr[1110] 	=	 0x26D20A6E;
    FractArr[1111] 	=	 0x8E64E0F5;
    FractArr[1112] 	=	 0x9CA7C20E;
    FractArr[1113] 	=	 0x6B3DC30D;
    FractArr[1114] 	=	 0x7CFAC63F;
    FractArr[1115] 	=	 0x6DC157BA;
    FractArr[1116] 	=	 0x947BC206;
    FractArr[1117] 	=	 0xFD0B9AB8;
    FractArr[1118] 	=	 0xF0BEF18E;
    FractArr[1119] 	=	 0x99B289AD;
    FractArr[1120] 	=	 0x979FF0CF;
    FractArr[1121] 	=	 0x475A03FE;
    FractArr[1122] 	=	 0xF12AAD31;
    FractArr[1123] 	=	 0x495E6983;
    FractArr[1124] 	=	 0xC6D9DA5A;
    FractArr[1125] 	=	 0xB9A8BFF7;
    FractArr[1126] 	=	 0xA7228F15;
    FractArr[1127] 	=	 0x81649E46;
    FractArr[1128] 	=	 0xC02D0537;
    FractArr[1129] 	=	 0x429B6624;
    FractArr[1130] 	=	 0x460723BB;
    FractArr[1131] 	=	 0x23C829C3;
    FractArr[1132] 	=	 0xBDD27BB5;
    FractArr[1133] 	=	 0x28E62E9F;
    FractArr[1134] 	=	 0xF4F0B2D7;
    FractArr[1135] 	=	 0xB6805AF7;
    FractArr[1136] 	=	 0x0054A269;
    FractArr[1137] 	=	 0xE4C2E512;
    FractArr[1138] 	=	 0xFA3DC39E;
    FractArr[1139] 	=	 0x78D25B93;
    FractArr[1140] 	=	 0xE4CAFC4A;
    FractArr[1141] 	=	 0xDC305958;
    FractArr[1142] 	=	 0x9F136C7E;
    FractArr[1143] 	=	 0x1B3E2B7E;
    FractArr[1144] 	=	 0x74ABFB8B;
    FractArr[1145] 	=	 0xB2E0E99E;
    FractArr[1146] 	=	 0x95705785;
    FractArr[1147] 	=	 0xCA8EF950;
    FractArr[1148] 	=	 0x3839C60E;
    FractArr[1149] 	=	 0xD91E1C00;
    FractArr[1150] 	=	 0xCAEA3DDA;
    FractArr[1151] 	=	 0xDD86F8DD;
    FractArr[1152] 	=	 0x1DBE3644;
    FractArr[1153] 	=	 0x8CB7A5D4;
    FractArr[1154] 	=	 0xB14F8A6C;
    FractArr[1155] 	=	 0x8083DE16;
    FractArr[1156] 	=	 0x63E43A72;
    FractArr[1157] 	=	 0x34E7FC9A;
    FractArr[1158] 	=	 0xD5FEB1CC;
    FractArr[1159] 	=	 0x75E93827;
    FractArr[1160] 	=	 0xB7B6DB6B;
    FractArr[1161] 	=	 0x03FCBFBA;
    FractArr[1162] 	=	 0xA651C3D7;
    FractArr[1163] 	=	 0x2767BAD3;
    FractArr[1164] 	=	 0x43454691;
    FractArr[1165] 	=	 0xFAD58DBE;
    FractArr[1166] 	=	 0x9B20CF3D;
    FractArr[1167] 	=	 0x629AD120;
    FractArr[1168] 	=	 0x5180E9D3;
    FractArr[1169] 	=	 0x51001445;
    FractArr[1170] 	=	 0x2D004549;
    FractArr[1171] 	=	 0x80A21925;
    FractArr[1172] 	=	 0x2545D10C;
    FractArr[1173] 	=	 0x94142D00;
    FractArr[1174] 	=	 0x14450150;
    FractArr[1175] 	=	 0x14059066;
    FractArr[1176] 	=	 0x8A024C51;
    FractArr[1177] 	=	 0x4501A428;
    FractArr[1178] 	=	 0x45015014;
    FractArr[1179] 	=	 0x45015014;
    FractArr[1180] 	=	 0xD3011425;
    FractArr[1181] 	=	 0xDAC473F8;
    FractArr[1182] 	=	 0x5DA79366;
    FractArr[1183] 	=	 0x199E6A58;
    FractArr[1184] 	=	 0x9DA2D5B2;
    FractArr[1185] 	=	 0xF91C09B7;
    FractArr[1186] 	=	 0x10002773;
    FractArr[1187] 	=	 0x1C480A24;
    FractArr[1188] 	=	 0x3EC7980E;
    FractArr[1189] 	=	 0xFC8937B5;
    FractArr[1190] 	=	 0x89EE9A6A;
    FractArr[1191] 	=	 0x6958A261;
    FractArr[1192] 	=	 0x5D7A6931;
    FractArr[1193] 	=	 0x12486F93;
    FractArr[1194] 	=	 0xCF2D2153;
    FractArr[1195] 	=	 0xDE1F8825;
    FractArr[1196] 	=	 0x2449726F;
    FractArr[1197] 	=	 0x4A9A25D7;
    FractArr[1198] 	=	 0x2E4A78E6;
    FractArr[1199] 	=	 0x5D6BB5A7;
    FractArr[1200] 	=	 0xD7BE76F7;
    FractArr[1201] 	=	 0x15F8EDB5;
    FractArr[1202] 	=	 0x6A61EDCC;
    FractArr[1203] 	=	 0xBD653B7B;
    FractArr[1204] 	=	 0x3A735BB9;
    FractArr[1205] 	=	 0xAC1D5840;
    FractArr[1206] 	=	 0x7BFD0CFC;
    FractArr[1207] 	=	 0x04087A55;
    FractArr[1208] 	=	 0x23D5D660;
    FractArr[1209] 	=	 0xBBA84529;
    FractArr[1210] 	=	 0x9A84FD3E;
    FractArr[1211] 	=	 0x8DB4534F;
    FractArr[1212] 	=	 0x6DCBD2FE;
    FractArr[1213] 	=	 0x1C451A61;
    FractArr[1214] 	=	 0x6096B3B9;
    FractArr[1215] 	=	 0xE97E72AD;
    FractArr[1216] 	=	 0x1907D525;
    FractArr[1217] 	=	 0x757A5E18;
    FractArr[1218] 	=	 0x9BBBE21C;
    FractArr[1219] 	=	 0x597CFD4B;
    FractArr[1220] 	=	 0xF78B3CC0;
    FractArr[1221] 	=	 0x45F36BC4;
    FractArr[1222] 	=	 0x1D37AA1A;
    FractArr[1223] 	=	 0xE8816754;
    FractArr[1224] 	=	 0x5AD2852B;
    FractArr[1225] 	=	 0xADD3A558;
    FractArr[1226] 	=	 0xB86399AD;
    FractArr[1227] 	=	 0x56E4814D;
    FractArr[1228] 	=	 0x18DC948C;
    FractArr[1229] 	=	 0x86F15861;
    FractArr[1230] 	=	 0xE3FB2CFE;
    FractArr[1231] 	=	 0xEDD7671C;
    FractArr[1232] 	=	 0xE0AC9122;
    FractArr[1233] 	=	 0x4C4A3463;
    FractArr[1234] 	=	 0xB005486A;
    FractArr[1235] 	=	 0xE2D7FA38;
    FractArr[1236] 	=	 0x4647CDF9;
    FractArr[1237] 	=	 0x9A5E84A2;
    FractArr[1238] 	=	 0x7FDFF5EE;
    FractArr[1239] 	=	 0xA8F7E89E;
    FractArr[1240] 	=	 0x9F5D4945;
    FractArr[1241] 	=	 0xADA7EF32;
    FractArr[1242] 	=	 0x5AA6A642;
    FractArr[1243] 	=	 0x011EA1FD;
    FractArr[1244] 	=	 0x8BE9D332;
    FractArr[1245] 	=	 0x1440AB4E;
    FractArr[1246] 	=	 0x604A8A66;
    FractArr[1247] 	=	 0x5094142D;
    FractArr[1248] 	=	 0x52144501;
    FractArr[1249] 	=	 0x298AA200;
    FractArr[1250] 	=	 0x288A6680;
    FractArr[1251] 	=	 0x144501A4;
    FractArr[1252] 	=	 0x14450150;
    FractArr[1253] 	=	 0x25450150;
    FractArr[1254] 	=	 0xD0CCDF1E;
    FractArr[1255] 	=	 0xFD56D202;
    FractArr[1256] 	=	 0x9D0686D6;
    FractArr[1257] 	=	 0x3503C503;
    FractArr[1258] 	=	 0x4904A8DC;
    FractArr[1259] 	=	 0x8EDF6E03;
    FractArr[1260] 	=	 0x7EB72309;
    FractArr[1261] 	=	 0xD260D67B;
    FractArr[1262] 	=	 0x27E244AF;
    FractArr[1263] 	=	 0x59DB4488;
    FractArr[1264] 	=	 0x1BC07689;
    FractArr[1265] 	=	 0xA7DFE77A;
    FractArr[1266] 	=	 0x1F765EE7;
    FractArr[1267] 	=	 0x8B57C235;
    FractArr[1268] 	=	 0xBE569A92;
    FractArr[1269] 	=	 0xD1FC74ED;
    FractArr[1270] 	=	 0x2ECEA8AC;
    FractArr[1271] 	=	 0x9AD129D6;
    FractArr[1272] 	=	 0x47802068;
    FractArr[1273] 	=	 0x7A2DCD43;
    FractArr[1274] 	=	 0x15454126;
    FractArr[1275] 	=	 0x9269A2A9;
    FractArr[1276] 	=	 0xAB4C776A;
    FractArr[1277] 	=	 0x1D509510;
    FractArr[1278] 	=	 0xE404BBC9;
    FractArr[1279] 	=	 0x8D1FF47A;
    FractArr[1280] 	=	 0xD3C48A61;
    FractArr[1281] 	=	 0x6A9551C3;
    FractArr[1282] 	=	 0x2054448F;
    FractArr[1283] 	=	 0x651425E7;
    FractArr[1284] 	=	 0xEDAF56D1;
    FractArr[1285] 	=	 0x30BDB7E0;
    FractArr[1286] 	=	 0x4065C8DA;
    FractArr[1287] 	=	 0xBC2C673A;
    FractArr[1288] 	=	 0xF43B0E9E;
    FractArr[1289] 	=	 0x6BC2CEA2;
    FractArr[1290] 	=	 0x8F6BE1BB;
    FractArr[1291] 	=	 0xE52C802D;
    FractArr[1292] 	=	 0x4D1DC0F8;
    FractArr[1293] 	=	 0x74D2C54A;
    FractArr[1294] 	=	 0x8D76BB7D;
    FractArr[1295] 	=	 0x406FAFAF;
    FractArr[1296] 	=	 0x2F379770;
    FractArr[1297] 	=	 0x0415AD52;
    FractArr[1298] 	=	 0xEF0573A9;
    FractArr[1299] 	=	 0x3B5284D9;
    FractArr[1300] 	=	 0x84109B3C;
    FractArr[1301] 	=	 0x313D3920;
    FractArr[1302] 	=	 0x91E2759C;
    FractArr[1303] 	=	 0x3C530CEE;
    FractArr[1304] 	=	 0x42248032;
    FractArr[1305] 	=	 0x083D9755;
    FractArr[1306] 	=	 0x46154FA7;
    FractArr[1307] 	=	 0x8164B4A3;
    FractArr[1308] 	=	 0x6E744BC6;
    FractArr[1309] 	=	 0xA22AD768;
    FractArr[1310] 	=	 0x6E90134B;
    FractArr[1311] 	=	 0x0E0454E4;
    FractArr[1312] 	=	 0xB1E75C07;
    FractArr[1313] 	=	 0x1F0403CF;
    FractArr[1314] 	=	 0xFC7F5DE7;
    FractArr[1315] 	=	 0x913FE624;
    FractArr[1316] 	=	 0x5597AB74;
    FractArr[1317] 	=	 0x30DC3AE0;
    FractArr[1318] 	=	 0x5A00EE0D;
    FractArr[1319] 	=	 0x08BA74F3;
    FractArr[1320] 	=	 0xB08A91C1;
    FractArr[1321] 	=	 0x310E0C39;
    FractArr[1322] 	=	 0x995A1357;
    FractArr[1323] 	=	 0x7D135423;
    FractArr[1324] 	=	 0x7F2507D0;
    FractArr[1325] 	=	 0x377BBEC2;
    FractArr[1326] 	=	 0xEBE3F8E1;
    FractArr[1327] 	=	 0x5DDB587B;
    FractArr[1328] 	=	 0x6ED7DAEE;
    FractArr[1329] 	=	 0x541D499E;
    FractArr[1330] 	=	 0x6C944E31;
    FractArr[1331] 	=	 0xB54C2D66;
    FractArr[1332] 	=	 0x5FCBD40A;
    FractArr[1333] 	=	 0x75328E4C;
    FractArr[1334] 	=	 0x3AADA6A5;
    FractArr[1335] 	=	 0x455100AD;
    FractArr[1336] 	=	 0xA228C014;
    FractArr[1337] 	=	 0x5114408A;
    FractArr[1338] 	=	 0x51140045;
    FractArr[1339] 	=	 0x51140045;
    FractArr[1340] 	=	 0x490B4049;
    FractArr[1341] 	=	 0x02A06846;
    FractArr[1342] 	=	 0x4051D28C;
    FractArr[1343] 	=	 0x4D144B0B;
    FractArr[1344] 	=	 0x6312AB34;
    FractArr[1345] 	=	 0x9C0C1C73;
    FractArr[1346] 	=	 0xE86D6D0A;
    FractArr[1347] 	=	 0x34455157;
    FractArr[1348] 	=	 0xFEB1052B;
    FractArr[1349] 	=	 0x392656D0;
    FractArr[1350] 	=	 0xDC810A86;
    FractArr[1351] 	=	 0x7BD073B6;
    FractArr[1352] 	=	 0xB9D6717B;
    FractArr[1353] 	=	 0xE189D831;
    FractArr[1354] 	=	 0x23A44AE8;
    FractArr[1355] 	=	 0x77F5D7CC;
    FractArr[1356] 	=	 0x2EF2DFE5;
    FractArr[1357] 	=	 0x93A4549C;
    FractArr[1358] 	=	 0xDA5D222E;
    FractArr[1359] 	=	 0x7757AC58;
    FractArr[1360] 	=	 0xC691ACB6;
    FractArr[1361] 	=	 0xDC860319;
    FractArr[1362] 	=	 0xDEEDEC46;
    FractArr[1363] 	=	 0x4FE0C733;
    FractArr[1364] 	=	 0x734680CD;
    FractArr[1365] 	=	 0xD77CCF5A;
    FractArr[1366] 	=	 0x5C5F3157;
    FractArr[1367] 	=	 0x024D7034;
    FractArr[1368] 	=	 0x8CA77C08;
    FractArr[1369] 	=	 0x7040A279;
    FractArr[1370] 	=	 0xC7F358C6;
    FractArr[1371] 	=	 0xC9EB771F;
    FractArr[1372] 	=	 0xA1AB013C;
    FractArr[1373] 	=	 0x696B4F5B;
    FractArr[1374] 	=	 0x095EAA71;
    FractArr[1375] 	=	 0x2589B527;
    FractArr[1376] 	=	 0x40248256;
    FractArr[1377] 	=	 0x42803BC4;
    FractArr[1378] 	=	 0x98C1D0AE;
    FractArr[1379] 	=	 0x183DCF86;
    FractArr[1380] 	=	 0x3FBD4E56;
    FractArr[1381] 	=	 0xC3DA6FDA;
    FractArr[1382] 	=	 0x6DA0D347;
    FractArr[1383] 	=	 0x9A6E4379;
    FractArr[1384] 	=	 0xB1564432;
    FractArr[1385] 	=	 0x49F30836;
    FractArr[1386] 	=	 0x530FF2FB;
    FractArr[1387] 	=	 0xCBAFF0D3;
    FractArr[1388] 	=	 0x716221DE;
    FractArr[1389] 	=	 0xF78AE612;
    FractArr[1390] 	=	 0x49A2776F;
    FractArr[1391] 	=	 0x975FF6BD;
    FractArr[1392] 	=	 0x95BD9EF9;
    FractArr[1393] 	=	 0xAED3EB35;
    FractArr[1394] 	=	 0xC3AC2587;
    FractArr[1395] 	=	 0xD1A6BE0D;
    FractArr[1396] 	=	 0x2B028043;
    FractArr[1397] 	=	 0xEB891D30;
    FractArr[1398] 	=	 0x05BDFCE9;
    FractArr[1399] 	=	 0x75655853;
    FractArr[1400] 	=	 0x5448B10C;
    FractArr[1401] 	=	 0x2754E6E7;
    FractArr[1402] 	=	 0x74EA5AA7;
    FractArr[1403] 	=	 0xB55B08BF;
    FractArr[1404] 	=	 0x5ECD4BC1;
    FractArr[1405] 	=	 0xDBB2AB1B;
    FractArr[1406] 	=	 0xCC9B58A4;
    FractArr[1407] 	=	 0x71F2C462;
    FractArr[1408] 	=	 0x5DF1FCDF;
    FractArr[1409] 	=	 0xBBE19D1E;
    FractArr[1410] 	=	 0xB4A0EE79;
    FractArr[1411] 	=	 0xCBC2D4B1;
    FractArr[1412] 	=	 0x684B3220;
    FractArr[1413] 	=	 0xDEBE3123;
    FractArr[1414] 	=	 0xF930829B;
    FractArr[1415] 	=	 0xFAC18358;
    FractArr[1416] 	=	 0xF58173F0;
    FractArr[1417] 	=	 0x869E3815;
    FractArr[1418] 	=	 0xEB29840A;
    FractArr[1419] 	=	 0xDAAE2429;
    FractArr[1420] 	=	 0xB59D7C69;
    FractArr[1421] 	=	 0x8E137FFD;
    FractArr[1422] 	=	 0x4D565738;
    FractArr[1423] 	=	 0x1CD5CBDE;
    FractArr[1424] 	=	 0x9EEFF060;
    FractArr[1425] 	=	 0xA45BDB7C;
    FractArr[1426] 	=	 0x62315F7B;
    FractArr[1427] 	=	 0x37B7D5DD;
    FractArr[1428] 	=	 0x0475B443;
    FractArr[1429] 	=	 0x8AD367FA;
    FractArr[1430] 	=	 0xF46EC592;
    FractArr[1431] 	=	 0x68822769;
    FractArr[1432] 	=	 0xBBB4B926;
    FractArr[1433] 	=	 0xD1DE2D5F;
    FractArr[1434] 	=	 0xEC118F0C;
    FractArr[1435] 	=	 0x8EFAF54A;
    FractArr[1436] 	=	 0x9CAEB9FE;
    FractArr[1437] 	=	 0xFA765768;
    FractArr[1438] 	=	 0x69A208E2;
    FractArr[1439] 	=	 0xAE78A7D1;
    FractArr[1440] 	=	 0x0C81EA45;
    FractArr[1441] 	=	 0x7CA30C60;
    FractArr[1442] 	=	 0x1CB83380;
    FractArr[1443] 	=	 0xB3D2D767;
    FractArr[1444] 	=	 0x3CEE28B5;
    FractArr[1445] 	=	 0x9F067F3B;
    FractArr[1446] 	=	 0x2DCBA3AA;
    FractArr[1447] 	=	 0x2CE848F2;
    FractArr[1448] 	=	 0x076CAB98;
    FractArr[1449] 	=	 0xB8DE28E5;
    FractArr[1450] 	=	 0xD53EF5FA;
    FractArr[1451] 	=	 0xA93B47E5;
    FractArr[1452] 	=	 0x9CB18F8B;
    FractArr[1453] 	=	 0x742BA3F9;
    FractArr[1454] 	=	 0x7A534DD7;
    FractArr[1455] 	=	 0xED57577C;
    FractArr[1456] 	=	 0x0FEFE6B6;
    FractArr[1457] 	=	 0xAC242718;
    FractArr[1458] 	=	 0x17F7F5D7;
    FractArr[1459] 	=	 0xC5CD48F4;
    FractArr[1460] 	=	 0x33BB4BC1;
    FractArr[1461] 	=	 0xC7F28415;
    FractArr[1462] 	=	 0x5BDD036F;
    FractArr[1463] 	=	 0xC1487798;
    FractArr[1464] 	=	 0xDC1894B7;
    FractArr[1465] 	=	 0xE37AF900;
    FractArr[1466] 	=	 0x3F2BAEBF;
    FractArr[1467] 	=	 0xA4EB56C4;
    FractArr[1468] 	=	 0x3A0FB346;
    FractArr[1469] 	=	 0x6219485B;
    FractArr[1470] 	=	 0x58B9F04B;
    FractArr[1471] 	=	 0x7805C9E5;
    FractArr[1472] 	=	 0x1C23B07C;
    FractArr[1473] 	=	 0x1C1C39F2;
    FractArr[1474] 	=	 0xA9853656;
    FractArr[1475] 	=	 0x8D58C3E8;
    FractArr[1476] 	=	 0x55B5ED2C;
    FractArr[1477] 	=	 0xC38B91BD;
    FractArr[1478] 	=	 0x49C3C53D;
    FractArr[1479] 	=	 0x36E86520;
    FractArr[1480] 	=	 0x1DC1B98E;
    FractArr[1481] 	=	 0xDBEE4EEB;
    FractArr[1482] 	=	 0x4FBCD459;
    FractArr[1483] 	=	 0x7291E715;
    FractArr[1484] 	=	 0x1652E6D6;
    FractArr[1485] 	=	 0xCC13C2C0;
    FractArr[1486] 	=	 0x1736F30B;
    FractArr[1487] 	=	 0x2DC74880;
    FractArr[1488] 	=	 0xAABCE6F4;
    FractArr[1489] 	=	 0x4DF1A851;
    FractArr[1490] 	=	 0x5B1B35C1;
    FractArr[1491] 	=	 0xF66BAFA6;
    FractArr[1492] 	=	 0xB98100FF;
    FractArr[1493] 	=	 0x242D8FBB;
    FractArr[1494] 	=	 0xE7B74D9B;
    FractArr[1495] 	=	 0x68EE76B1;
    FractArr[1496] 	=	 0xF4D307F5;
    FractArr[1497] 	=	 0x3B1F3A5D;
    FractArr[1498] 	=	 0xC65D4552;
    FractArr[1499] 	=	 0x8642C53D;
    FractArr[1500] 	=	 0x70727E62;
    FractArr[1501] 	=	 0xDF110C0A;
    FractArr[1502] 	=	 0x4F5071AE;
    FractArr[1503] 	=	 0x4FA96961;
    FractArr[1504] 	=	 0xCCA3C9E7;
    FractArr[1505] 	=	 0x82FEAD96;
    FractArr[1506] 	=	 0x34031ACF;
    FractArr[1507] 	=	 0x00749CF2;
    FractArr[1508] 	=	 0x9DF28393;
    FractArr[1509] 	=	 0xAD77E4DE;
    FractArr[1510] 	=	 0xB6584F7D;
    FractArr[1511] 	=	 0xF5D35297;
    FractArr[1512] 	=	 0xADED85A4;
    FractArr[1513] 	=	 0x4B2A48EE;
    FractArr[1514] 	=	 0x293F9005;
    FractArr[1515] 	=	 0x2CD373B4;
    FractArr[1516] 	=	 0x6ED70337;
    FractArr[1517] 	=	 0x2464C579;
    FractArr[1518] 	=	 0x512C11B7;
    FractArr[1519] 	=	 0x5B24D9B9;
    FractArr[1520] 	=	 0x3B836DE3;
    FractArr[1521] 	=	 0x1324775F;
    FractArr[1522] 	=	 0x031470CE;
    FractArr[1523] 	=	 0xA22B3DEE;
    FractArr[1524] 	=	 0x92042314;
    FractArr[1525] 	=	 0xABAFEB93;
    FractArr[1526] 	=	 0x7EBAFEBB;
    FractArr[1527] 	=	 0xBF294A25;
    FractArr[1528] 	=	 0x3772E4EB;
    FractArr[1529] 	=	 0x619913FE;
    FractArr[1530] 	=	 0xF327D357;
    FractArr[1531] 	=	 0x7B078AA4;
    FractArr[1532] 	=	 0xC6F33E98;
    FractArr[1533] 	=	 0x380EDDCA;
    FractArr[1534] 	=	 0xF7713BCE;
    FractArr[1535] 	=	 0xF639D77B;
    FractArr[1536] 	=	 0xA5B55B6D;
    FractArr[1537] 	=	 0x35FBFDC7;
    FractArr[1538] 	=	 0xDEFE01EB;
    FractArr[1539] 	=	 0xC52B6FAB;
    FractArr[1540] 	=	 0x6D59811C;
    FractArr[1541] 	=	 0x231D59DA;
    FractArr[1542] 	=	 0x7E02738F;
    FractArr[1543] 	=	 0x67BBE554;
    FractArr[1544] 	=	 0x0E7A3D71;
    FractArr[1545] 	=	 0x271E77D9;
    FractArr[1546] 	=	 0x8F65AE48;
    FractArr[1547] 	=	 0x39368676;
    FractArr[1548] 	=	 0x8FFD1D5C;
    FractArr[1549] 	=	 0xB1BCB5FB;
    FractArr[1550] 	=	 0xE24B8A98;
    FractArr[1551] 	=	 0x0F4B186D;
    FractArr[1552] 	=	 0xA538F416;
    FractArr[1553] 	=	 0x5AA192A9;
    FractArr[1554] 	=	 0xA8F42B95;
    FractArr[1555] 	=	 0x533A419E;
    FractArr[1556] 	=	 0x2BA934EA;
    FractArr[1557] 	=	 0x14450144;
    FractArr[1558] 	=	 0x14450150;
    FractArr[1559] 	=	 0x52B40094;
    FractArr[1560] 	=	 0x01D08C66;
    FractArr[1561] 	=	 0xA0286946;
    FractArr[1562] 	=	 0x1425CD05;
    FractArr[1563] 	=	 0x14450150;
    FractArr[1564] 	=	 0x26450150;
    FractArr[1565] 	=	 0xAB05A068;
    FractArr[1566] 	=	 0x638B1756;
    FractArr[1567] 	=	 0x6295D370;
    FractArr[1568] 	=	 0x002618E1;
    FractArr[1569] 	=	 0x07772423;
    FractArr[1570] 	=	 0x2B4A553C;
    FractArr[1571] 	=	 0x5AA1D42A;
    FractArr[1572] 	=	 0x9E3DA79B;
    FractArr[1573] 	=	 0xBBB8698C;
    FractArr[1574] 	=	 0x4374D3A3;
    FractArr[1575] 	=	 0x36576903;
    FractArr[1576] 	=	 0x685EC6D1;
    FractArr[1577] 	=	 0xC8F08E25;
    FractArr[1578] 	=	 0x150E15D3;
    FractArr[1579] 	=	 0xE46D9CB7;
    FractArr[1580] 	=	 0x27F509B6;
    FractArr[1581] 	=	 0x6305B01D;
    FractArr[1582] 	=	 0x5FD1C45B;
    FractArr[1583] 	=	 0xB063BA6B;
    FractArr[1584] 	=	 0xA2373737;
    FractArr[1585] 	=	 0xC67657ED;
    FractArr[1586] 	=	 0x9F63B6BB;
    FractArr[1587] 	=	 0x8E679C94;
    FractArr[1588] 	=	 0x58717ADC;
    FractArr[1589] 	=	 0x73BB661A;
    FractArr[1590] 	=	 0xBB1CCFA7;
    FractArr[1591] 	=	 0x6448B2D8;
    FractArr[1592] 	=	 0x21CB0959;
    FractArr[1593] 	=	 0x00FF8EDB;
    FractArr[1594] 	=	 0xF8727485;
    FractArr[1595] 	=	 0xACBAC383;
    FractArr[1596] 	=	 0xDE4E3D02;
    FractArr[1597] 	=	 0x6077AA1D;
    FractArr[1598] 	=	 0x0BCEC028;
    FractArr[1599] 	=	 0x9F0A0E10;
    FractArr[1600] 	=	 0xC9D34EBC;
    FractArr[1601] 	=	 0x7931CEAF;
    FractArr[1602] 	=	 0x39032B46;
    FractArr[1603] 	=	 0x492847AE;
    FractArr[1604] 	=	 0xF62A5EA9;
    FractArr[1605] 	=	 0x4FEBCA6B;
    FractArr[1606] 	=	 0xA687AEB9;
    FractArr[1607] 	=	 0x9AADC2AA;
    FractArr[1608] 	=	 0x314B6B76;
    FractArr[1609] 	=	 0xD4B44CA7;
    FractArr[1610] 	=	 0x759DE6C5;
    FractArr[1611] 	=	 0x786B006D;
    FractArr[1612] 	=	 0x682CAB04;
    FractArr[1613] 	=	 0xE5B32589;
    FractArr[1614] 	=	 0x009485B0;
    FractArr[1615] 	=	 0x751C1432;
    FractArr[1616] 	=	 0xF04FEF19;
    FractArr[1617] 	=	 0xBFCEED9D;
    FractArr[1618] 	=	 0x5FA17F67;
    FractArr[1619] 	=	 0xB4BE155D;
    FractArr[1620] 	=	 0xCB7A436C;
    FractArr[1621] 	=	 0xE8297689;
    FractArr[1622] 	=	 0xB6FE5F4F;
    FractArr[1623] 	=	 0xF1BE5245;
    FractArr[1624] 	=	 0x69A58F66;
    FractArr[1625] 	=	 0xC6A363AD;
    FractArr[1626] 	=	 0xDB149C81;
    FractArr[1627] 	=	 0x46920408;
    FractArr[1628] 	=	 0x0418297A;
    FractArr[1629] 	=	 0x12CB9681;
    FractArr[1630] 	=	 0xDF553B30;
    FractArr[1631] 	=	 0xD8431A87;
    FractArr[1632] 	=	 0x6AAC7943;
    FractArr[1633] 	=	 0x6E582B46;
    FractArr[1634] 	=	 0xDBEE4041;
    FractArr[1635] 	=	 0xAFD72D57;
    FractArr[1636] 	=	 0x74302A60;
    FractArr[1637] 	=	 0x5451459D;
    FractArr[1638] 	=	 0x7DB68556;
    FractArr[1639] 	=	 0x3F5BDBFB;
    FractArr[1640] 	=	 0x4CBDF92B;
    FractArr[1641] 	=	 0x9597CBAB;
    FractArr[1642] 	=	 0xCF1DEFA7;
    FractArr[1643] 	=	 0x6E2C6F86;
    FractArr[1644] 	=	 0x2FD365EC;
    FractArr[1645] 	=	 0xAB7B5E6E;
    FractArr[1646] 	=	 0xCDDC6456;
    FractArr[1647] 	=	 0x8B3598C4;
    FractArr[1648] 	=	 0x5E9CD50D;
    FractArr[1649] 	=	 0x5D320924;
    FractArr[1650] 	=	 0x5490225F;
    FractArr[1651] 	=	 0xD98641CC;
    FractArr[1652] 	=	 0x098E2407;
    FractArr[1653] 	=	 0x2B82EDC8;
    FractArr[1654] 	=	 0xB4D5B693;
    FractArr[1655] 	=	 0x3B4B3CF8;
    FractArr[1656] 	=	 0x7FC990E8;
    FractArr[1657] 	=	 0x9D322486;
    FractArr[1658] 	=	 0x7AE87188;
    FractArr[1659] 	=	 0x07BBCA9F;
    FractArr[1660] 	=	 0x74D6E7D7;
    FractArr[1661] 	=	 0xC1AB85C9;
    FractArr[1662] 	=	 0xAC507061;
    FractArr[1663] 	=	 0x678188B1;
    FractArr[1664] 	=	 0xC9B305F6;
    FractArr[1665] 	=	 0x198D93AF;
    FractArr[1666] 	=	 0xB4CCADD1;
    FractArr[1667] 	=	 0x52579E3D;
    FractArr[1668] 	=	 0x7872E799;
    FractArr[1669] 	=	 0xF1BE478B;
    FractArr[1670] 	=	 0xB3985A44;
    FractArr[1671] 	=	 0x9B24DC30;
    FractArr[1672] 	=	 0xC1716B5B;
    FractArr[1673] 	=	 0xBAE935C0;
    FractArr[1674] 	=	 0xAB8DEEAD;
    FractArr[1675] 	=	 0xCCBA5BDB;
    FractArr[1676] 	=	 0xB7685113;
    FractArr[1677] 	=	 0x4A7C23C0;
    FractArr[1678] 	=	 0xF5383809;
    FractArr[1679] 	=	 0xAFF1D307;
    FractArr[1680] 	=	 0x71C3832F;
    FractArr[1681] 	=	 0xA525AD47;
    FractArr[1682] 	=	 0x252E48E8;
    FractArr[1683] 	=	 0xB2920E97;
    FractArr[1684] 	=	 0x32F71D87;
    FractArr[1685] 	=	 0x1E397082;
    FractArr[1686] 	=	 0xB6A06B9D;
    FractArr[1687] 	=	 0x4997D0B0;
    FractArr[1688] 	=	 0x494D5DBD;
    FractArr[1689] 	=	 0xCB9761D5;
    FractArr[1690] 	=	 0x059C4146;
    FractArr[1691] 	=	 0xAB1DB7E9;
    FractArr[1692] 	=	 0xEA7921EC;
    FractArr[1693] 	=	 0xA5E1D8DA;
    FractArr[1694] 	=	 0x6ECB0527;
    FractArr[1695] 	=	 0xB5AD89A5;
    FractArr[1696] 	=	 0xECED355D;
    FractArr[1697] 	=	 0xB8B8B1E5;
    FractArr[1698] 	=	 0x924059B2;
    FractArr[1699] 	=	 0x1BCB2337;
    FractArr[1700] 	=	 0x1F866094;
    FractArr[1701] 	=	 0x7AFCF036;
    FractArr[1702] 	=	 0x23AA3A7F;
    FractArr[1703] 	=	 0xDD8A3696;
    FractArr[1704] 	=	 0x881992A2;
    FractArr[1705] 	=	 0xCA122411;
    FractArr[1706] 	=	 0x777F23AD;
    FractArr[1707] 	=	 0x7ADB800B;
    FractArr[1708] 	=	 0xC5747A72;
    FractArr[1709] 	=	 0x1B356E3D;
    FractArr[1710] 	=	 0xB96118DB;
    FractArr[1711] 	=	 0x6D4363FB;
    FractArr[1712] 	=	 0x52B65DBA;
    FractArr[1713] 	=	 0xC0183911;
    FractArr[1714] 	=	 0x8283EFC0;
    FractArr[1715] 	=	 0x17619A71;
    FractArr[1716] 	=	 0x62D21E5E;
    FractArr[1717] 	=	 0xFB8475D4;
    FractArr[1718] 	=	 0xC5E0DC6D;
    FractArr[1719] 	=	 0xB88D1C6D;
    FractArr[1720] 	=	 0x0330ACAC;
    FractArr[1721] 	=	 0xC39FB2B7;
    FractArr[1722] 	=	 0x952DD3A7;
    FractArr[1723] 	=	 0xD6AF9DCD;
    FractArr[1724] 	=	 0x5495E0C3;
    FractArr[1725] 	=	 0xE473AD24;
    FractArr[1726] 	=	 0xC5F82643;
    FractArr[1727] 	=	 0x6411B6C2;
    FractArr[1728] 	=	 0xFC8F5019;
    FractArr[1729] 	=	 0x6F5F0F2C;
    FractArr[1730] 	=	 0x5F22ED7A;
    FractArr[1731] 	=	 0xA5901812;
    FractArr[1732] 	=	 0xB4AB96C6;
    FractArr[1733] 	=	 0xC0217761;
    FractArr[1734] 	=	 0x068FC8F4;
    FractArr[1735] 	=	 0xD612BDB8;
    FractArr[1736] 	=	 0x6FD351E7;
    FractArr[1737] 	=	 0x8267AEAC;
    FractArr[1738] 	=	 0xBA4CE7DE;
    FractArr[1739] 	=	 0x89245A59;
    FractArr[1740] 	=	 0xBCC85D64;
    FractArr[1741] 	=	 0xBC823C75;
    FractArr[1742] 	=	 0xDA5CE977;
    FractArr[1743] 	=	 0x755BAEF8;
    FractArr[1744] 	=	 0x11538810;
    FractArr[1745] 	=	 0x1DE5838D;
    FractArr[1746] 	=	 0xA38CABB8;
    FractArr[1747] 	=	 0x39A4AE1B;
    FractArr[1748] 	=	 0xCCE893B4;
    FractArr[1749] 	=	 0xA196A905;
    FractArr[1750] 	=	 0xF46B955A;
    FractArr[1751] 	=	 0x3A119E98;
    FractArr[1752] 	=	 0xD4129554;
    FractArr[1753] 	=	 0x1540AB99;
    FractArr[1754] 	=	 0xB206FAA2;
    FractArr[1755] 	=	 0x7D6D5F96;
    FractArr[1756] 	=	 0x3C6DFD26;
    FractArr[1757] 	=	 0x6D9E27B1;
    FractArr[1758] 	=	 0xD1C3269C;
    FractArr[1759] 	=	 0x3E1863B7;
    FractArr[1760] 	=	 0xAF5E9BB5;
    FractArr[1761] 	=	 0x46BBF8AA;
    FractArr[1762] 	=	 0x382BF4D3;
    FractArr[1763] 	=	 0xEEE6E612;
    FractArr[1764] 	=	 0xD1C2E3F2;
    FractArr[1765] 	=	 0x1D18E6E9;
    FractArr[1766] 	=	 0x6126DE1E;
    FractArr[1767] 	=	 0xCAE65F82;
    FractArr[1768] 	=	 0x63E4B8C8;
    FractArr[1769] 	=	 0x8ACB15A1;
    FractArr[1770] 	=	 0x8A9B52AF;
    FractArr[1771] 	=	 0xA8DC1BA7;
    FractArr[1772] 	=	 0xDD3CF7A4;
    FractArr[1773] 	=	 0x21594DB4;
    FractArr[1774] 	=	 0xBBB499B6;
    FractArr[1775] 	=	 0x95E98AD5;
    FractArr[1776] 	=	 0xB8DBDC2D;
    FractArr[1777] 	=	 0xE8BE9959;
    FractArr[1778] 	=	 0x9E988F43;
    FractArr[1779] 	=	 0xBA4AEBD8;
    FractArr[1780] 	=	 0x30E6B016;
    FractArr[1781] 	=	 0x19F3559A;
    FractArr[1782] 	=	 0x0B506824;
    FractArr[1783] 	=	 0x739D3B77;
    FractArr[1784] 	=	 0xB01C47B9;
    FractArr[1785] 	=	 0x30EAC8C1;
    FractArr[1786] 	=	 0xFED5BD6B;
    FractArr[1787] 	=	 0xDB5C5826;
    FractArr[1788] 	=	 0x6F674F5E;
    FractArr[1789] 	=	 0xE9DB1BA8;
    FractArr[1790] 	=	 0x2778E5AC;
    FractArr[1791] 	=	 0x4C2DF275;
    FractArr[1792] 	=	 0x88EE3704;
    FractArr[1793] 	=	 0x46C5FC24;
    FractArr[1794] 	=	 0xFB5C0772;
    FractArr[1795] 	=	 0x17DFC456;
    FractArr[1796] 	=	 0xC83511B4;
    FractArr[1797] 	=	 0x042ED2B7;
    FractArr[1798] 	=	 0x644D1B09;
    FractArr[1799] 	=	 0xB7AC680C;
    FractArr[1800] 	=	 0x666FE64F;
    FractArr[1801] 	=	 0xF3018E3B;
    FractArr[1802] 	=	 0x5B9E8C3A;
    FractArr[1803] 	=	 0x2F96E48A;
    FractArr[1804] 	=	 0xA39AA21A;
    FractArr[1805] 	=	 0xEAA7AFAF;
    FractArr[1806] 	=	 0xCA66E7ED;
    FractArr[1807] 	=	 0xC9738FE5;
    FractArr[1808] 	=	 0x2F75B2BF;
    FractArr[1809] 	=	 0x2FEDDFEC;
    FractArr[1810] 	=	 0xB0AFFBEC;
    FractArr[1811] 	=	 0xF26A1F67;
    FractArr[1812] 	=	 0x71CECA5B;
    FractArr[1813] 	=	 0xBC8EF1F7;
    FractArr[1814] 	=	 0x4B53EB75;
    FractArr[1815] 	=	 0x0B6EEDE1;
    FractArr[1816] 	=	 0xD1662DA8;
    FractArr[1817] 	=	 0x27EE08F5;
    FractArr[1818] 	=	 0xB62786C8;
    FractArr[1819] 	=	 0x6460F270;
    FractArr[1820] 	=	 0xE06404ED;
    FractArr[1821] 	=	 0x7165577A;
    FractArr[1822] 	=	 0x6FD20AF1;
    FractArr[1823] 	=	 0x47A7C507;
    FractArr[1824] 	=	 0xB47BFBF6;
    FractArr[1825] 	=	 0xD09C06D3;
    FractArr[1826] 	=	 0x8AB90DC5;
    FractArr[1827] 	=	 0xC65D3E40;
    FractArr[1828] 	=	 0x04205346;
    FractArr[1829] 	=	 0x9E635472;
    FractArr[1830] 	=	 0x9FA77584;
    FractArr[1831] 	=	 0xF01F9688;
    FractArr[1832] 	=	 0x7B5DB5B1;
    FractArr[1833] 	=	 0x1736BCC8;
    FractArr[1834] 	=	 0x4D647FD6;
    FractArr[1835] 	=	 0x002C8DCA;
    FractArr[1836] 	=	 0x18C10DA2;
    FractArr[1837] 	=	 0x9E323F94;
    FractArr[1838] 	=	 0x73DCC109;
    FractArr[1839] 	=	 0x637D5AE9;
    FractArr[1840] 	=	 0xF6EEAF17;
    FractArr[1841] 	=	 0x7E6BE1BF;
    FractArr[1842] 	=	 0x6885FC6D;
    FractArr[1843] 	=	 0xD08438F7;
    FractArr[1844] 	=	 0xE62E8975;
    FractArr[1845] 	=	 0xBE498FB4;
    FractArr[1846] 	=	 0x3300987B;
    FractArr[1847] 	=	 0x5EEEB642;
    FractArr[1848] 	=	 0x97057930;
    FractArr[1849] 	=	 0x6B8D1F19;
    FractArr[1850] 	=	 0xA90DC27F;
    FractArr[1851] 	=	 0x73C200FF;
    FractArr[1852] 	=	 0x970800FF;
    FractArr[1853] 	=	 0xFFF6699F;
    FractArr[1854] 	=	 0xBDE9F900;
    FractArr[1855] 	=	 0x5EF5AFBC;
    FractArr[1856] 	=	 0xE9B95D67;
    FractArr[1857] 	=	 0xAEF3D7ED;
    FractArr[1858] 	=	 0x6920FE8D;
    FractArr[1859] 	=	 0xF6DA3F97;
    FractArr[1860] 	=	 0x26EB73B7;
    FractArr[1861] 	=	 0xDAD6EEDE;
    FractArr[1862] 	=	 0x8EA36E08;
    FractArr[1863] 	=	 0x294FE608;
    FractArr[1864] 	=	 0x3615F0D9;
    FractArr[1865] 	=	 0x462CA9AA;
    FractArr[1866] 	=	 0xFB1DC732;
    FractArr[1867] 	=	 0x9CF0BF0A;
    FractArr[1868] 	=	 0xB7F09FE9;
    FractArr[1869] 	=	 0x2EE100FF;
    FractArr[1870] 	=	 0x00FF2EF2;
    FractArr[1871] 	=	 0xE700FFB3;
    FractArr[1872] 	=	 0x7FF3C59E;
    FractArr[1873] 	=	 0xE9CBDFE3;
    FractArr[1874] 	=	 0xFA7D1DBB;
    FractArr[1875] 	=	 0x581F557E;
    FractArr[1876] 	=	 0x726F3EC6;
    FractArr[1877] 	=	 0x9BB78BD6;
    FractArr[1878] 	=	 0x7DE3B7B2;
    FractArr[1879] 	=	 0xF768873C;
    FractArr[1880] 	=	 0x3DFC2939;
    FractArr[1881] 	=	 0x2445DBAD;
    FractArr[1882] 	=	 0x84FAE8B3;
    FractArr[1883] 	=	 0xDD81C651;
    FractArr[1884] 	=	 0x0A75B6E4;
    FractArr[1885] 	=	 0x8E24A1A7;
    FractArr[1886] 	=	 0x7747BD07;
    FractArr[1887] 	=	 0x16B6EAA2;
    FractArr[1888] 	=	 0xA67975A9;
    FractArr[1889] 	=	 0xB95B5B5E;
    FractArr[1890] 	=	 0x03CD920A;
    FractArr[1891] 	=	 0x20233122;
    FractArr[1892] 	=	 0x400E4602;
    FractArr[1893] 	=	 0x4247BD26;
    FractArr[1894] 	=	 0xA8935EF1;
    FractArr[1895] 	=	 0xED230D1B;
    FractArr[1896] 	=	 0x1B120D33;
    FractArr[1897] 	=	 0x426D9B4B;
    FractArr[1898] 	=	 0x28EE567D;
    FractArr[1899] 	=	 0xDC3171A4;
    FractArr[1900] 	=	 0x72F98D0A;
    FractArr[1901] 	=	 0x068276A5;
    FractArr[1902] 	=	 0xAED9EE46;
    FractArr[1903] 	=	 0x293EC63F;
    FractArr[1904] 	=	 0xBE11F1B5;
    FractArr[1905] 	=	 0x2D752E91;
    FractArr[1906] 	=	 0xB76EA8ED;
    FractArr[1907] 	=	 0xA27F32B6;
    FractArr[1908] 	=	 0xB1C7BA2D;
    FractArr[1909] 	=	 0x48E42158;
    FractArr[1910] 	=	 0x138CE971;
    FractArr[1911] 	=	 0x112B8E4E;
    FractArr[1912] 	=	 0x8592BC3A;
    FractArr[1913] 	=	 0xB2F35797;
    FractArr[1914] 	=	 0x95FCBDF9;
    FractArr[1915] 	=	 0xAF14E384;
    FractArr[1916] 	=	 0xA5A28F73;
    FractArr[1917] 	=	 0x1268DA96;
    FractArr[1918] 	=	 0x44269637;
    FractArr[1919] 	=	 0x1C31EE93;
    FractArr[1920] 	=	 0x20A3D237;
    FractArr[1921] 	=	 0x1B81BB8D;
    FractArr[1922] 	=	 0x329EE186;
    FractArr[1923] 	=	 0xE38CD12B;
    FractArr[1924] 	=	 0x93BB7825;
    FractArr[1925] 	=	 0x9C12B766;
    FractArr[1926] 	=	 0xD436F288;
    FractArr[1927] 	=	 0xE08CCD42;
    FractArr[1928] 	=	 0x2440D30C;
    FractArr[1929] 	=	 0xE3490680;
    FractArr[1930] 	=	 0xD25EBD03;
    FractArr[1931] 	=	 0x9776C3F7;
    FractArr[1932] 	=	 0xC02B6D2D;
    FractArr[1933] 	=	 0x32D21932;
    FractArr[1934] 	=	 0x00FF3DDB;
    FractArr[1935] 	=	 0x5913CB0A;
    FractArr[1936] 	=	 0xF629A551;
    FractArr[1937] 	=	 0xECCC631C;
    FractArr[1938] 	=	 0x60A16C51;
    FractArr[1939] 	=	 0x823A82C0;
    FractArr[1940] 	=	 0xD0D7553A;
    FractArr[1941] 	=	 0x20E920B5;
    FractArr[1942] 	=	 0x46886C0A;
    FractArr[1943] 	=	 0x2108E192;
    FractArr[1944] 	=	 0xEC3A7F37;
    FractArr[1945] 	=	 0x65D24574;
    FractArr[1946] 	=	 0x2938DBBC;
    FractArr[1947] 	=	 0x2E0FC325;
    FractArr[1948] 	=	 0x49936267;
    FractArr[1949] 	=	 0xC1474F90;
    FractArr[1950] 	=	 0xE08CC2D9;
    FractArr[1951] 	=	 0xB31D9B0F;
    FractArr[1952] 	=	 0x86A2AC55;
    FractArr[1953] 	=	 0x4A6B7DF2;
    FractArr[1954] 	=	 0x208E5587;
    FractArr[1955] 	=	 0x883158BE;
    FractArr[1956] 	=	 0x67DC19B6;
    FractArr[1957] 	=	 0x2A966B7C;
    FractArr[1958] 	=	 0x5E0F7F7A;
    FractArr[1959] 	=	 0xF235677A;
    FractArr[1960] 	=	 0xAD249E58;
    FractArr[1961] 	=	 0x84CA7E52;
    FractArr[1962] 	=	 0x7BAD6479;
    FractArr[1963] 	=	 0x76D576F6;
    FractArr[1964] 	=	 0xD9EFBBB6;
    FractArr[1965] 	=	 0x89F0B41D;
    FractArr[1966] 	=	 0xFE7BB7AB;
    FractArr[1967] 	=	 0x17FE3487;
    FractArr[1968] 	=	 0x523403D4;
    FractArr[1969] 	=	 0xAA245ADF;
    FractArr[1970] 	=	 0x46E24670;
    FractArr[1971] 	=	 0x3E07860A;
    FractArr[1972] 	=	 0xC7E9C5DF;
    FractArr[1973] 	=	 0xC293CB5A;
    FractArr[1974] 	=	 0x8CAFDC3A;
    FractArr[1975] 	=	 0x3EC0EB46;
    FractArr[1976] 	=	 0x4682E45B;
    FractArr[1977] 	=	 0x8C514770;
    FractArr[1978] 	=	 0xBF2BA61F;
    FractArr[1979] 	=	 0x92BCF49A;
    FractArr[1980] 	=	 0x1B413618;
    FractArr[1981] 	=	 0x88E35948;
    FractArr[1982] 	=	 0x0B21D96D;
    FractArr[1983] 	=	 0xC6396DBC;
    FractArr[1984] 	=	 0xEFA88F0A;
    FractArr[1985] 	=	 0xECD629C6;
    FractArr[1986] 	=	 0x2BA94CD3;
    FractArr[1987] 	=	 0x1C337031;
    FractArr[1988] 	=	 0x0F92EAAA;
    FractArr[1989] 	=	 0x099ECD60;
    FractArr[1990] 	=	 0xF5A99F61;
    FractArr[1991] 	=	 0x31F579CD;
    FractArr[1992] 	=	 0x54BABAD8;
    FractArr[1993] 	=	 0x5FB6FC9D;
    FractArr[1994] 	=	 0xD547A786;
    FractArr[1995] 	=	 0x94EEA569;
    FractArr[1996] 	=	 0x69035FF4;
    FractArr[1997] 	=	 0xFEB61DDE;
    FractArr[1998] 	=	 0x42E5D7D1;
    FractArr[1999] 	=	 0xEA166E54;
    FractArr[2000] 	=	 0xFAFA66D9;
    FractArr[2001] 	=	 0x1FE32957;
    FractArr[2002] 	=	 0x96F8DE14;
    FractArr[2003] 	=	 0xD76D2DF1;
    FractArr[2004] 	=	 0xED88B3CB;
    FractArr[2005] 	=	 0x8531DE8A;
    FractArr[2006] 	=	 0x7777851F;
    FractArr[2007] 	=	 0xF5E5053A;
    FractArr[2008] 	=	 0x652DBCAD;
    FractArr[2009] 	=	 0x08E47830;
    FractArr[2010] 	=	 0x6F78DFE1;
    FractArr[2011] 	=	 0x9091E394;
    FractArr[2012] 	=	 0x63FEEEC3;
    FractArr[2013] 	=	 0xBCD1558F;
    FractArr[2014] 	=	 0xB269A32D;
    FractArr[2015] 	=	 0xE87677CD;
    FractArr[2016] 	=	 0x46E0C84A;
    FractArr[2017] 	=	 0xF254720E;
    FractArr[2018] 	=	 0x758ABA3D;
    FractArr[2019] 	=	 0xB45239EB;
    FractArr[2020] 	=	 0xEB2F6F63;
    FractArr[2021] 	=	 0x61E939F3;
    FractArr[2022] 	=	 0x99F7B49F;
    FractArr[2023] 	=	 0x747AC253;
    FractArr[2024] 	=	 0xDBD01A3E;
    FractArr[2025] 	=	 0xB72ABC52;
    FractArr[2026] 	=	 0xA7C4840E;
    FractArr[2027] 	=	 0x85BF3EA9;
    FractArr[2028] 	=	 0x0900FF45;
    FractArr[2029] 	=	 0x0C7D976E;
    FractArr[2030] 	=	 0x970773AD;
    FractArr[2031] 	=	 0xBC42AA78;
    FractArr[2032] 	=	 0xFE312B3F;
    FractArr[2033] 	=	 0xC57FAE59;
    FractArr[2034] 	=	 0xF3922BFE;
    FractArr[2035] 	=	 0xC0E2F250;
    FractArr[2036] 	=	 0x54953F82;
    FractArr[2037] 	=	 0x4EDF1E74;
    FractArr[2038] 	=	 0xC5DDC9B5;
    FractArr[2039] 	=	 0x30C35234;
    FractArr[2040] 	=	 0x821C12C8;
    FractArr[2041] 	=	 0xC7CF573A;
    FractArr[2042] 	=	 0x9A4BEA04;
    FractArr[2043] 	=	 0x00FFC4AF;
    FractArr[2044] 	=	 0x4CBD1EAB;
    FractArr[2045] 	=	 0x431B6154;
    FractArr[2046] 	=	 0xF06C8F54;
    FractArr[2047] 	=	 0x9DC787A7;
    FractArr[2048] 	=	 0x78CFAB6B;
    FractArr[2049] 	=	 0x5B416A93;
    FractArr[2050] 	=	 0x91BB3C31;
    FractArr[2051] 	=	 0x93DDB819;
    FractArr[2052] 	=	 0x1F85B6D5;
    FractArr[2053] 	=	 0xE1E1BAD2;
    FractArr[2054] 	=	 0xB73461FB;
    FractArr[2055] 	=	 0xC9DBA56F;
    FractArr[2056] 	=	 0xEF2C2971;
    FractArr[2057] 	=	 0x5E9313E6;
    FractArr[2058] 	=	 0x5E3DE131;
    FractArr[2059] 	=	 0xDD0117EE;
    FractArr[2060] 	=	 0x7A32EA80;
    FractArr[2061] 	=	 0x7C19BBE2;
    FractArr[2062] 	=	 0x89765172;
    FractArr[2063] 	=	 0x9A030338;
    FractArr[2064] 	=	 0xD1D2E0F5;
    FractArr[2065] 	=	 0x2E1AB3BB;
    FractArr[2066] 	=	 0x888E372E;
    FractArr[2067] 	=	 0x632DE2B5;
    FractArr[2068] 	=	 0x5835D04A;
    FractArr[2069] 	=	 0x37221DAE;
    FractArr[2070] 	=	 0xB6010931;
    FractArr[2071] 	=	 0x2CED3284;
    FractArr[2072] 	=	 0xFEEFC931;
    FractArr[2073] 	=	 0x3CCB2B7D;
    FractArr[2074] 	=	 0x4F6DE347;
    FractArr[2075] 	=	 0x3389985E;
    FractArr[2076] 	=	 0xA29D8215;
    FractArr[2077] 	=	 0x5D015C15;
    FractArr[2078] 	=	 0xFC2FA556;
    FractArr[2079] 	=	 0xA236F324;
    FractArr[2080] 	=	 0xE3594B8A;
    FractArr[2081] 	=	 0x76676CCC;
    FractArr[2082] 	=	 0x9AC30F7A;
    FractArr[2083] 	=	 0x657F174F;
    FractArr[2084] 	=	 0x562C1AEA;
    FractArr[2085] 	=	 0x27E1D656;
    FractArr[2086] 	=	 0x34BA0F8B;
    FractArr[2087] 	=	 0x6A8F93C2;
    FractArr[2088] 	=	 0x1C52ADE5;
    FractArr[2089] 	=	 0x2A636FD6;
    FractArr[2090] 	=	 0x2C9F26D5;
    FractArr[2091] 	=	 0x64F02676;
    FractArr[2092] 	=	 0xAF62CFB7;
    FractArr[2093] 	=	 0xD9F1F66A;
    FractArr[2094] 	=	 0x130D2CB3;
    FractArr[2095] 	=	 0xC09D02C2;
    FractArr[2096] 	=	 0x3987FB8D;
    FractArr[2097] 	=	 0xAEF3E71E;
    FractArr[2098] 	=	 0xBB42F34A;
    FractArr[2099] 	=	 0x3CE1FA8E;
    FractArr[2100] 	=	 0x18D90E99;
    FractArr[2101] 	=	 0x5AC72964;
    FractArr[2102] 	=	 0x65273CEC;
    FractArr[2103] 	=	 0x31E16D73;
    FractArr[2104] 	=	 0xAE21D61E;
    FractArr[2105] 	=	 0xC9F619D7;
    FractArr[2106] 	=	 0xC88D47AF;
    FractArr[2107] 	=	 0xCE56418D;
    FractArr[2108] 	=	 0x9EC46B06;
    FractArr[2109] 	=	 0x92D4E868;
    FractArr[2110] 	=	 0x117AEA71;
    FractArr[2111] 	=	 0x13A4C6C3;
    FractArr[2112] 	=	 0xF45A783E;
    FractArr[2113] 	=	 0xFA85BF3B;
    FractArr[2114] 	=	 0x6DEFD680;
    FractArr[2115] 	=	 0xB2B1B861;
    FractArr[2116] 	=	 0x2BE686B7;
    FractArr[2117] 	=	 0xF0EE9A38;
    FractArr[2118] 	=	 0xC63B9BBF;
    FractArr[2119] 	=	 0x115623AE;
    FractArr[2120] 	=	 0x0786E3AE;
    FractArr[2121] 	=	 0xE7B85E95;
    FractArr[2122] 	=	 0x803EAF19;
    FractArr[2123] 	=	 0x9920D344;
    FractArr[2124] 	=	 0x032C22DD;
    FractArr[2125] 	=	 0x0B6628B2;
    FractArr[2126] 	=	 0x8C04489E;
    FractArr[2127] 	=	 0xA88F6C9F;
    FractArr[2128] 	=	 0x7ED546AF;
    FractArr[2129] 	=	 0xE24B5C2A;
    FractArr[2130] 	=	 0x4FF3464D;
    FractArr[2131] 	=	 0xC29AB6B0;
    FractArr[2132] 	=	 0x2D6FA87B;
    FractArr[2133] 	=	 0xDC61D4E1;
    FractArr[2134] 	=	 0x4B1C4DD0;
    FractArr[2135] 	=	 0x616D9018;
    FractArr[2136] 	=	 0x487DF986;
    FractArr[2137] 	=	 0xB05FEFE9;
    FractArr[2138] 	=	 0x99231E62;
    FractArr[2139] 	=	 0xE69F1D2A;
    FractArr[2140] 	=	 0x2E9FFAAD;
    FractArr[2141] 	=	 0xB6997AB9;
    FractArr[2142] 	=	 0x750F00FF;
    FractArr[2143] 	=	 0xEB9B7499;
    FractArr[2144] 	=	 0xFDD1F6F2;
    FractArr[2145] 	=	 0xE0792789;
    FractArr[2146] 	=	 0x21EA7266;
    FractArr[2147] 	=	 0x0A09AE62;
    FractArr[2148] 	=	 0xC2413650;
    FractArr[2149] 	=	 0x408E0496;
    FractArr[2150] 	=	 0xE2BB35C7;
    FractArr[2151] 	=	 0xA32F862F;
    FractArr[2152] 	=	 0xE12886F8;
    FractArr[2153] 	=	 0xE2320D98;
    FractArr[2154] 	=	 0x513B0BFA;
    FractArr[2155] 	=	 0x9C263671;
    FractArr[2156] 	=	 0x65D85DB8;
    FractArr[2157] 	=	 0x4B80AA8C;
    FractArr[2158] 	=	 0x3AC6910C;
    FractArr[2159] 	=	 0xFCF0F81E;
    FractArr[2160] 	=	 0x4317D74C;
    FractArr[2161] 	=	 0x8EA5D2B8;
    FractArr[2162] 	=	 0x8EB874CE;
    FractArr[2163] 	=	 0x922639E6;
    FractArr[2164] 	=	 0x9D08F336;
    FractArr[2165] 	=	 0x6078488B;
    FractArr[2166] 	=	 0x7124C9B9;
    FractArr[2167] 	=	 0xF8BC58C5;
    FractArr[2168] 	=	 0x13EAAD9F;
    FractArr[2169] 	=	 0xEC6D6E19;
    FractArr[2170] 	=	 0x1BF62264;
    FractArr[2171] 	=	 0xC89195E8;
    FractArr[2172] 	=	 0x80824886;
    FractArr[2173] 	=	 0x3BC02F13;
    FractArr[2174] 	=	 0x37ED1F79;
    FractArr[2175] 	=	 0x662DD64C;
    FractArr[2176] 	=	 0xDAD2772E;
    FractArr[2177] 	=	 0xBDFE96FE;
    FractArr[2178] 	=	 0x9F58EE47;
    FractArr[2179] 	=	 0xA57615FE;
    FractArr[2180] 	=	 0xC1917671;
    FractArr[2181] 	=	 0xD742A67F;
    FractArr[2182] 	=	 0xDA301737;
    FractArr[2183] 	=	 0xED70CDDB;
    FractArr[2184] 	=	 0xCA0EF923;
    FractArr[2185] 	=	 0x0E3022E3;
    FractArr[2186] 	=	 0x1938E736;
    FractArr[2187] 	=	 0x579C81E3;
    FractArr[2188] 	=	 0x62F8AE3D;
    FractArr[2189] 	=	 0xD3B342E7;
    FractArr[2190] 	=	 0xCAF21EEF;
    FractArr[2191] 	=	 0x49FCD6EE;
    FractArr[2192] 	=	 0xEC684BE4;
    FractArr[2193] 	=	 0xAC1076CA;
    FractArr[2194] 	=	 0x9AD05139;
    FractArr[2195] 	=	 0x35121FD4;
    FractArr[2196] 	=	 0xDD34D585;
    FractArr[2197] 	=	 0xF9585B40;
    FractArr[2198] 	=	 0x73D77CDA;
    FractArr[2199] 	=	 0xD63E36C4;
    FractArr[2200] 	=	 0xFB2C866B;
    FractArr[2201] 	=	 0xB1806EBE;
    FractArr[2202] 	=	 0x9AEF31C6;
    FractArr[2203] 	=	 0x5D174FCF;
    FractArr[2204] 	=	 0x48D70E0D;
    FractArr[2205] 	=	 0xAED34F97;
    FractArr[2206] 	=	 0xAE6DED2D;
    FractArr[2207] 	=	 0xE169E12D;
    FractArr[2208] 	=	 0x30E7C92E;
    FractArr[2209] 	=	 0x60777066;
    FractArr[2210] 	=	 0x20E52338;
    FractArr[2211] 	=	 0xB6D69371;
    FractArr[2212] 	=	 0xC9D8F5A5;
    FractArr[2213] 	=	 0xDFAED939;
    FractArr[2214] 	=	 0x00FF17F8;
    FractArr[2215] 	=	 0x3EB1F76B;
    FractArr[2216] 	=	 0xA84D875E;
    FractArr[2217] 	=	 0x97B92BFC;
    FractArr[2218] 	=	 0x9B8E3AC5;
    FractArr[2219] 	=	 0x60465DA2;
    FractArr[2220] 	=	 0x8C6391B6;
    FractArr[2221] 	=	 0x1819B35D;
    FractArr[2222] 	=	 0x92435698;
    FractArr[2223] 	=	 0xCD47B589;
    FractArr[2224] 	=	 0x3D685B80;
    FractArr[2225] 	=	 0x47634D06;
    FractArr[2226] 	=	 0x5100FFF0;
    FractArr[2227] 	=	 0xB345EF96;
    FractArr[2228] 	=	 0xDD38FD4B;
    FractArr[2229] 	=	 0xC52D10EB;
    FractArr[2230] 	=	 0xEE631EBC;
    FractArr[2231] 	=	 0xDC17CA58;
    FractArr[2232] 	=	 0x072600FF;
    FractArr[2233] 	=	 0xCF53470A;
    FractArr[2234] 	=	 0x505AF34C;
    FractArr[2235] 	=	 0xE0D75D7C;
    FractArr[2236] 	=	 0x16BBB9BE;
    FractArr[2237] 	=	 0xC53D637A;
    FractArr[2238] 	=	 0xD12AB9C0;
    FractArr[2239] 	=	 0x10718549;
    FractArr[2240] 	=	 0x713E8C8B;
    FractArr[2241] 	=	 0xF35C47B5;
    FractArr[2242] 	=	 0x2E9EFC5C;
    FractArr[2243] 	=	 0xD1549BBE;
    FractArr[2244] 	=	 0xB2B5E4EF;
    FractArr[2245] 	=	 0x48AB3479;
    FractArr[2246] 	=	 0xE28D20ED;
    FractArr[2247] 	=	 0x9D898E2C;
    FractArr[2248] 	=	 0x2D27C1A5;
    FractArr[2249] 	=	 0xE41875F3;
    FractArr[2250] 	=	 0x51593102;
    FractArr[2251] 	=	 0xA695608E;
    FractArr[2252] 	=	 0x5BD78BD6;
    FractArr[2253] 	=	 0xF9869D7F;
    FractArr[2254] 	=	 0xE1E31A3A;
    FractArr[2255] 	=	 0xC2DAB28E;
    FractArr[2256] 	=	 0xA7778969;
    FractArr[2257] 	=	 0xF76CEDBC;
    FractArr[2258] 	=	 0x28930744;
    FractArr[2259] 	=	 0xE44EC18A;
    FractArr[2260] 	=	 0x4F12BC29;
    FractArr[2261] 	=	 0x38386F03;
    FractArr[2262] 	=	 0xE1DB55E9;
    FractArr[2263] 	=	 0xFD1DA5E6;
    FractArr[2264] 	=	 0x8EBABCED;
    FractArr[2265] 	=	 0x50361497;
    FractArr[2266] 	=	 0xDCC52DC7;
    FractArr[2267] 	=	 0x2F8AB093;
    FractArr[2268] 	=	 0xBE15E130;
    FractArr[2269] 	=	 0xA18FC15D;
    FractArr[2270] 	=	 0x54D4EF51;
    FractArr[2271] 	=	 0xD5497C93;
    FractArr[2272] 	=	 0xBAB2BFA7;
    FractArr[2273] 	=	 0x7F4FCF92;
    FractArr[2274] 	=	 0x432AC7B1;
    FractArr[2275] 	=	 0x8124891C;
    FractArr[2276] 	=	 0x1D0BC27C;
    FractArr[2277] 	=	 0xE0C1E5CC;
    FractArr[2278] 	=	 0x0E0C4300;
    FractArr[2279] 	=	 0xBF240506;
    FractArr[2280] 	=	 0x8B7B3512;
    FractArr[2281] 	=	 0xB4BAA4A9;
    FractArr[2282] 	=	 0xE66DEED3;
    FractArr[2283] 	=	 0x4BD68AB6;
    FractArr[2284] 	=	 0x48DE884B;
    FractArr[2285] 	=	 0xC86263D9;
    FractArr[2286] 	=	 0xB0BC2D49;
    FractArr[2287] 	=	 0x39969C24;
    FractArr[2288] 	=	 0xBFA639CF;
    FractArr[2289] 	=	 0x6FDB3AB4;
    FractArr[2290] 	=	 0xCAB7F3D7;
    FractArr[2291] 	=	 0x4712EEE1;
    FractArr[2292] 	=	 0xF359C7F0;
    FractArr[2293] 	=	 0xF7E68E25;
    FractArr[2294] 	=	 0xEF78B34C;
    FractArr[2295] 	=	 0xE2169EC6;
    FractArr[2296] 	=	 0xAC641E66;
    FractArr[2297] 	=	 0x6AB7EB8A;
    FractArr[2298] 	=	 0x310C869C;
    FractArr[2299] 	=	 0x9E0AD4DF;
    FractArr[2300] 	=	 0x7696C0C7;
    FractArr[2301] 	=	 0x1A5757B6;
    FractArr[2302] 	=	 0xE28DE0F5;
    FractArr[2303] 	=	 0x97DAA295;
    FractArr[2304] 	=	 0xB24D2402;
    FractArr[2305] 	=	 0xB1EFB298;
    FractArr[2306] 	=	 0x7D75E6DB;
    FractArr[2307] 	=	 0x1B3142B8;
    FractArr[2308] 	=	 0xDC674512;
    FractArr[2309] 	=	 0xAED6437C;
    FractArr[2310] 	=	 0x122E59E4;
    FractArr[2311] 	=	 0x555D47D6;
    FractArr[2312] 	=	 0x32C66035;
    FractArr[2313] 	=	 0x118A7A3F;
    FractArr[2314] 	=	 0x02F78353;
    FractArr[2315] 	=	 0xC7EB18A8;
    FractArr[2316] 	=	 0xF8B0D55A;
    FractArr[2317] 	=	 0x69716383;
    FractArr[2318] 	=	 0x747A6B3A;
    FractArr[2319] 	=	 0xFDDDE466;
    FractArr[2320] 	=	 0xB7A219AA;
    FractArr[2321] 	=	 0x9A3750F3;
    FractArr[2322] 	=	 0xC85080F2;
    FractArr[2323] 	=	 0x2469E587;
    FractArr[2324] 	=	 0x398CCFC7;
    FractArr[2325] 	=	 0x53F31E04;
    FractArr[2326] 	=	 0xF73EEAEB;
    FractArr[2327] 	=	 0xDBF8ED95;
    FractArr[2328] 	=	 0xDA412EA8;
    FractArr[2329] 	=	 0x1A35C3B7;
    FractArr[2330] 	=	 0x4EDAD6AC;
    FractArr[2331] 	=	 0x6DC70AA3;
    FractArr[2332] 	=	 0x1B5DD204;
    FractArr[2333] 	=	 0x31210ED9;
    FractArr[2334] 	=	 0xF5C69908;
    FractArr[2335] 	=	 0x90899B4F;
    FractArr[2336] 	=	 0xA5BF03E7;
    FractArr[2337] 	=	 0x9DC33F66;
    FractArr[2338] 	=	 0xF3A4394E;
    FractArr[2339] 	=	 0x3B2E0D75;
    FractArr[2340] 	=	 0xF18AB424;
    FractArr[2341] 	=	 0x1076DEAF;
    FractArr[2342] 	=	 0x58B17279;
    FractArr[2343] 	=	 0x24BBDBF9;
    FractArr[2344] 	=	 0x5DB5DB83;
    FractArr[2345] 	=	 0x3523BED7;
    FractArr[2346] 	=	 0x69D2DCD5;
    FractArr[2347] 	=	 0x36B712B1;
    FractArr[2348] 	=	 0x7DDCD933;
    FractArr[2349] 	=	 0xFB8B3DA2;
    FractArr[2350] 	=	 0x20B20BE2;
    FractArr[2351] 	=	 0x517E2A66;
    FractArr[2352] 	=	 0x830E3BCB;
    FractArr[2353] 	=	 0x079F1503;
    FractArr[2354] 	=	 0x16452DC4;
    FractArr[2355] 	=	 0x746C2E28;
    FractArr[2356] 	=	 0xB115BBFB;
    FractArr[2357] 	=	 0x494BC282;
    FractArr[2358] 	=	 0x47936262;
    FractArr[2359] 	=	 0xBE192509;
    FractArr[2360] 	=	 0x3E09BE6F;
    FractArr[2361] 	=	 0x4BA5F4DC;
    FractArr[2362] 	=	 0x4F5341FB;
    FractArr[2363] 	=	 0xFFAF9DAF;
    FractArr[2364] 	=	 0xAF6D0F00;
    FractArr[2365] 	=	 0x73C907F3;
    FractArr[2366] 	=	 0x117CD3B4;
    FractArr[2367] 	=	 0x10991675;
    FractArr[2368] 	=	 0x1DCE12D4;
    FractArr[2369] 	=	 0x1652D35A;
    FractArr[2370] 	=	 0x613EB30A;
    FractArr[2371] 	=	 0x8181329A;
    FractArr[2372] 	=	 0x6648AA5D;
    FractArr[2373] 	=	 0x1D383DDC;
    FractArr[2374] 	=	 0xF0572FAB;
    FractArr[2375] 	=	 0x1FBEEC1D;
    FractArr[2376] 	=	 0xEC136ED4;
    FractArr[2377] 	=	 0x663D4DE6;
    FractArr[2378] 	=	 0x56D7AE94;
    FractArr[2379] 	=	 0x814B8E50;
    FractArr[2380] 	=	 0x38A0E3F2;
    FractArr[2381] 	=	 0x2387E504;
    FractArr[2382] 	=	 0xD0D49A83;
    FractArr[2383] 	=	 0xEAA86BBC;
    FractArr[2384] 	=	 0x8DA719F3;
    FractArr[2385] 	=	 0x9326691E;
    FractArr[2386] 	=	 0x4C638152;
    FractArr[2387] 	=	 0x1A33D6E1;
    FractArr[2388] 	=	 0x57801CA3;
    FractArr[2389] 	=	 0xF7750C00;
    FractArr[2390] 	=	 0xCEF141E9;
    FractArr[2391] 	=	 0xADF637A9;
    FractArr[2392] 	=	 0x196AC19A;
    FractArr[2393] 	=	 0x96D17824;
    FractArr[2394] 	=	 0x67586637;
    FractArr[2395] 	=	 0x6EE0EC50;
    FractArr[2396] 	=	 0xEFFEE520;
    FractArr[2397] 	=	 0x1539BD50;
    FractArr[2398] 	=	 0x5211EBF0;
    FractArr[2399] 	=	 0xB5533296;
    FractArr[2400] 	=	 0x93AE06A5;
    FractArr[2401] 	=	 0xD7E9DD7B;
    FractArr[2402] 	=	 0xD033DD4B;
    FractArr[2403] 	=	 0x7B4BDC6B;
    FractArr[2404] 	=	 0x30C000FF;
    FractArr[2405] 	=	 0xAB5DF820;
    FractArr[2406] 	=	 0x9BDF5C59;
    FractArr[2407] 	=	 0xB4696289;
    FractArr[2408] 	=	 0x6F0384F7;
    FractArr[2409] 	=	 0x984C0023;
    FractArr[2410] 	=	 0x1BE42EC3;
    FractArr[2411] 	=	 0x747A9EB1;
    FractArr[2412] 	=	 0x053DC0EF;
    FractArr[2413] 	=	 0xB93580A7;
    FractArr[2414] 	=	 0x37D744ED;
    FractArr[2415] 	=	 0x29B13696;
    FractArr[2416] 	=	 0x863769E2;
    FractArr[2417] 	=	 0x2A888D44;
    FractArr[2418] 	=	 0x9C5E68C3;
    FractArr[2419] 	=	 0x637A2486;
    FractArr[2420] 	=	 0x735DD58C;
    FractArr[2421] 	=	 0x6FA126E2;
    FractArr[2422] 	=	 0xD3263568;
    FractArr[2423] 	=	 0x199BDB34;
    FractArr[2424] 	=	 0x508B23AE;
    FractArr[2425] 	=	 0x306F6149;
    FractArr[2426] 	=	 0x73304EB4;
    FractArr[2427] 	=	 0x1CC80DBB;
    FractArr[2428] 	=	 0x662BFC6D;
    FractArr[2429] 	=	 0x685C1C2F;
    FractArr[2430] 	=	 0x0DC1B137;
    FractArr[2431] 	=	 0xDEA749A2;
    FractArr[2432] 	=	 0x865BB96E;
    FractArr[2433] 	=	 0x90599E5D;
    FractArr[2434] 	=	 0xE39C2B96;
    FractArr[2435] 	=	 0xE819CF68;
    FractArr[2436] 	=	 0x78563B30;
    FractArr[2437] 	=	 0xBF2A6EFC;
    FractArr[2438] 	=	 0x9A564DFB;
    FractArr[2439] 	=	 0x4FB5685A;
    FractArr[2440] 	=	 0x65EF1BFE;
    FractArr[2441] 	=	 0xF68B5152;
    FractArr[2442] 	=	 0xEAEFD171;
    FractArr[2443] 	=	 0x0B7E9772;
    FractArr[2444] 	=	 0x12688AF1;
    FractArr[2445] 	=	 0xB4D19278;
    FractArr[2446] 	=	 0x99E571BB;
    FractArr[2447] 	=	 0xF3C048E6;
    FractArr[2448] 	=	 0xF753212E;
    FractArr[2449] 	=	 0x8254F0D9;
    FractArr[2450] 	=	 0x8EDC7683;
    FractArr[2451] 	=	 0x968A673B;
    FractArr[2452] 	=	 0xF0C6E173;
    FractArr[2453] 	=	 0x6A9077F4;
    FractArr[2454] 	=	 0xC72C6D8B;
    FractArr[2455] 	=	 0xDB60076B;
    FractArr[2456] 	=	 0xDE7727E4;
    FractArr[2457] 	=	 0xD4912323;
    FractArr[2458] 	=	 0x5AE91863;
    FractArr[2459] 	=	 0x5FBE265E;
    FractArr[2460] 	=	 0xE9487810;
    FractArr[2461] 	=	 0xEAD86A9A;
    FractArr[2462] 	=	 0x2D119572;
    FractArr[2463] 	=	 0x629EC4DB;
    FractArr[2464] 	=	 0xC66A2720;
    FractArr[2465] 	=	 0x92E1C6AD;
    FractArr[2466] 	=	 0x09106003;
    FractArr[2467] 	=	 0xCA35CFC1;
    FractArr[2468] 	=	 0x964A47F8;
    FractArr[2469] 	=	 0x742E39DE;
    FractArr[2470] 	=	 0x26EF48DB;
    FractArr[2471] 	=	 0x96C997B4;
    FractArr[2472] 	=	 0x772A5719;
    FractArr[2473] 	=	 0xE39E51AF;
    FractArr[2474] 	=	 0x4ECD18F1;
    FractArr[2475] 	=	 0x276BAC0A;
    FractArr[2476] 	=	 0xDCEBA335;
    FractArr[2477] 	=	 0x7CD3A9DD;
    FractArr[2478] 	=	 0x4FEC57B2;
    FractArr[2479] 	=	 0x3BB28FA9;
    FractArr[2480] 	=	 0x3E5EDCDD;
    FractArr[2481] 	=	 0xA83418A0;
    FractArr[2482] 	=	 0xC056A36D;
    FractArr[2483] 	=	 0x9C3B1FC8;
    FractArr[2484] 	=	 0x7090A41D;
    FractArr[2485] 	=	 0xAD8A833E;
    FractArr[2486] 	=	 0xB49AC22B;
    FractArr[2487] 	=	 0x72DC5D3A;
    FractArr[2488] 	=	 0x29144B3A;
    FractArr[2489] 	=	 0x7840E174;
    FractArr[2490] 	=	 0x36256665;
    FractArr[2491] 	=	 0x7400FF21;
    FractArr[2492] 	=	 0xC7F1E064;
    FractArr[2493] 	=	 0x0F7FDF4A;
    FractArr[2494] 	=	 0xFF50685B;
    FractArr[2495] 	=	 0x4F6A6900;
    FractArr[2496] 	=	 0x9EAC2A6D;
    FractArr[2497] 	=	 0x0161C46D;
    FractArr[2498] 	=	 0x9F10E809;
    FractArr[2499] 	=	 0xC71807E2;
    FractArr[2500] 	=	 0x88B779A5;
    FractArr[2501] 	=	 0x596F3DB5;
    FractArr[2502] 	=	 0x02C49FBF;
    FractArr[2503] 	=	 0xD1D344CA;
    FractArr[2504] 	=	 0xCCCE98BC;
    FractArr[2505] 	=	 0xFC984E28;
    FractArr[2506] 	=	 0x596BFD7B;
    FractArr[2507] 	=	 0xA88A1DB7;
    FractArr[2508] 	=	 0x00FF77D4;
    FractArr[2509] 	=	 0xB9A3533F;
    FractArr[2510] 	=	 0xD5439BBB;
    FractArr[2511] 	=	 0xBF246C6F;
    FractArr[2512] 	=	 0x4902DCBF;
    FractArr[2513] 	=	 0x3E598BA6;
    FractArr[2514] 	=	 0x5D7E0E49;
    FractArr[2515] 	=	 0xABE275C0;
    FractArr[2516] 	=	 0x4F98F8DF;
    FractArr[2517] 	=	 0x2A91AC23;
    FractArr[2518] 	=	 0xA258CC48;
    FractArr[2519] 	=	 0xF77E0C74;
    FractArr[2520] 	=	 0x4BD75C3F;
    FractArr[2521] 	=	 0x50877DA0;
    FractArr[2522] 	=	 0x3BDED3B1;
    FractArr[2523] 	=	 0x1B482234;
    FractArr[2524] 	=	 0x188062CB;
    FractArr[2525] 	=	 0xD30D03A5;
    FractArr[2526] 	=	 0x0FB862D4;
    FractArr[2527] 	=	 0x82DAE812;
    FractArr[2528] 	=	 0xB6218C6A;
    FractArr[2529] 	=	 0x817A8290;
    FractArr[2530] 	=	 0xE628B9D2;
    FractArr[2531] 	=	 0x652FA9B0;
    FractArr[2532] 	=	 0x56885FB3;
    FractArr[2533] 	=	 0xCDC1B8C2;
    FractArr[2534] 	=	 0x84AFED32;
    FractArr[2535] 	=	 0xE385B40E;
    FractArr[2536] 	=	 0x81916E90;
    FractArr[2537] 	=	 0xE8BAE2C1;
    FractArr[2538] 	=	 0xF61C0DBC;
    FractArr[2539] 	=	 0xEC13B676;
    FractArr[2540] 	=	 0x6281884B;
    FractArr[2541] 	=	 0xF584E0A4;
    FractArr[2542] 	=	 0xE8032FAC;
    FractArr[2543] 	=	 0xC9CDD53A;
    FractArr[2544] 	=	 0x16BEBCB8;
    FractArr[2545] 	=	 0xCFF72936;
    FractArr[2546] 	=	 0x2BEC3856;
    FractArr[2547] 	=	 0x4CE7D2D6;
    FractArr[2548] 	=	 0x0BDF131A;
    FractArr[2549] 	=	 0x25DD9645;
    FractArr[2550] 	=	 0x97936F11;
    FractArr[2551] 	=	 0x6E0BC674;
    FractArr[2552] 	=	 0x96EF9AEB;
    FractArr[2553] 	=	 0x6C940A3B;
    FractArr[2554] 	=	 0x65EF3CB6;
    FractArr[2555] 	=	 0xA72EED52;
    FractArr[2556] 	=	 0x16766B97;
    FractArr[2557] 	=	 0x054B54BA;
    FractArr[2558] 	=	 0xC1AB80B2;
    FractArr[2559] 	=	 0xCF6BC663;
    FractArr[2560] 	=	 0x21F3EE6E;
    FractArr[2561] 	=	 0x634FFD0A;
    FractArr[2562] 	=	 0xF151AFC0;
    FractArr[2563] 	=	 0xD485829F;
    FractArr[2564] 	=	 0x6AA57F6D;
    FractArr[2565] 	=	 0x02B33D12;
    FractArr[2566] 	=	 0x57B219DB;
    FractArr[2567] 	=	 0xBF95D7D8;
    FractArr[2568] 	=	 0xBBC46F87;
    FractArr[2569] 	=	 0xDB49CC07;
    FractArr[2570] 	=	 0x63EBACC8;
    FractArr[2571] 	=	 0xB2775569;
    FractArr[2572] 	=	 0x3473D54C;
    FractArr[2573] 	=	 0x0F5FED91;
    FractArr[2574] 	=	 0xBC686BF4;
    FractArr[2575] 	=	 0xE6666A21;
    FractArr[2576] 	=	 0x246EC06B;
    FractArr[2577] 	=	 0x27670B2C;
    FractArr[2578] 	=	 0x00FF18E5;
    FractArr[2579] 	=	 0xDEBF0E74;
    FractArr[2580] 	=	 0x6958BCAE;
    FractArr[2581] 	=	 0x154B2472;
    FractArr[2582] 	=	 0x958528AC;
    FractArr[2583] 	=	 0x29B96089;
    FractArr[2584] 	=	 0xC793EDF4;
    FractArr[2585] 	=	 0x4E7BAE38;
    FractArr[2586] 	=	 0x82622D9D;
    FractArr[2587] 	=	 0x1C4758DD;
    FractArr[2588] 	=	 0xE9468451;
    FractArr[2589] 	=	 0xFE7A55B5;
    FractArr[2590] 	=	 0x5F40AD18;
    FractArr[2591] 	=	 0xB8B1B453;
    FractArr[2592] 	=	 0xA4DC7B3A;
    FractArr[2593] 	=	 0xA0B3C833;
    FractArr[2594] 	=	 0x3A865D01;
    FractArr[2595] 	=	 0x4ACF7190;
    FractArr[2596] 	=	 0x9A57F7F9;
    FractArr[2597] 	=	 0x3AAB4F4E;
    FractArr[2598] 	=	 0xDED85A54;
    FractArr[2599] 	=	 0x05B0C397;
    FractArr[2600] 	=	 0x8DA16D8D;
    FractArr[2601] 	=	 0x08BCDF21;
    FractArr[2602] 	=	 0x7FAE66FA;
    FractArr[2603] 	=	 0xCFC61164;
    FractArr[2604] 	=	 0x239DB9D2;
    FractArr[2605] 	=	 0x9AD2B45B;
    FractArr[2606] 	=	 0x92B82749;
    FractArr[2607] 	=	 0x61626728;
    FractArr[2608] 	=	 0x1273C9B6;
    FractArr[2609] 	=	 0xC9C9518F;
    FractArr[2610] 	=	 0x27D135C1;
    FractArr[2611] 	=	 0x45666D88;
    FractArr[2612] 	=	 0x51803B96;
    FractArr[2613] 	=	 0x4F3BE5C6;
    FractArr[2614] 	=	 0x83FDD020;
    FractArr[2615] 	=	 0x65AC91F8;
    FractArr[2616] 	=	 0xA67C6C51;
    FractArr[2617] 	=	 0x855AA5A6;
    FractArr[2618] 	=	 0xFBB54A4D;
    FractArr[2619] 	=	 0x649F0F5C;
    FractArr[2620] 	=	 0xD2416ACA;
    FractArr[2621] 	=	 0x8E5253A2;
    FractArr[2622] 	=	 0xB410B295;
    FractArr[2623] 	=	 0x14004551;
    FractArr[2624] 	=	 0x80966652;
    FractArr[2625] 	=	 0x45D14C0A;
    FractArr[2626] 	=	 0x14252D00;
    FractArr[2627] 	=	 0x14450150;
    FractArr[2628] 	=	 0xF28C1C99;
    FractArr[2629] 	=	 0xD1008A3A;
    FractArr[2630] 	=	 0x8D7BB5D3;
    FractArr[2631] 	=	 0xEC60643E;
    FractArr[2632] 	=	 0x72A5ACEA;
    FractArr[2633] 	=	 0x9ED3E67E;
    FractArr[2634] 	=	 0x67E85A3F;
    FractArr[2635] 	=	 0x7A813EF1;
    FractArr[2636] 	=	 0xB652E752;
    FractArr[2637] 	=	 0xC045B974;
    FractArr[2638] 	=	 0x3C642692;
    FractArr[2639] 	=	 0xE3BCFBF5;
    FractArr[2640] 	=	 0xE738E339;
    FractArr[2641] 	=	 0xDB76AEA0;
    FractArr[2642] 	=	 0x55BCBF42;
    FractArr[2643] 	=	 0x68474A99;
    FractArr[2644] 	=	 0x07B801DB;
    FractArr[2645] 	=	 0x01DDD27E;
    FractArr[2646] 	=	 0xB65ACF51;
    FractArr[2647] 	=	 0x91B40EDE;
    FractArr[2648] 	=	 0x2E6FAE11;
    FractArr[2649] 	=	 0x8218CA32;
    FractArr[2650] 	=	 0xA10F7DC1;
    FractArr[2651] 	=	 0xE8E31CF7;
    FractArr[2652] 	=	 0x9B1D5F31;
    FractArr[2653] 	=	 0xCE6500FF;
    FractArr[2654] 	=	 0xA8BB92A3;
    FractArr[2655] 	=	 0xDFAE7CB4;
    FractArr[2656] 	=	 0x7EF9F626;
    FractArr[2657] 	=	 0x5A1575A7;
    FractArr[2658] 	=	 0x3CE6E9D6;
    FractArr[2659] 	=	 0xEF8DBFDE;
    FractArr[2660] 	=	 0x1E1DB4AC;
    FractArr[2661] 	=	 0xD069DBD4;
    FractArr[2662] 	=	 0x001EB39C;
    FractArr[2663] 	=	 0xE94F8251;
    FractArr[2664] 	=	 0x9A54D0CF;
    FractArr[2665] 	=	 0xBD26BDF6;
    FractArr[2666] 	=	 0x0D3F6BE2;
    FractArr[2667] 	=	 0x934ECC69;
    FractArr[2668] 	=	 0x80C62269;
    FractArr[2669] 	=	 0x312A650E;
    FractArr[2670] 	=	 0xA0A78FB9;
    FractArr[2671] 	=	 0x63689BAD;
    FractArr[2672] 	=	 0x8D1B34D2;
    FractArr[2673] 	=	 0x20894933;
    FractArr[2674] 	=	 0xE63396B9;
    FractArr[2675] 	=	 0x92745B48;
    FractArr[2676] 	=	 0xED38FBAF;
    FractArr[2677] 	=	 0x51FAF4C1;
    FractArr[2678] 	=	 0x0F00FFE1;
    FractArr[2679] 	=	 0xED67F8D9;
    FractArr[2680] 	=	 0x13D38D7A;
    FractArr[2681] 	=	 0x94FBAB78;
    FractArr[2682] 	=	 0x6464587E;
    FractArr[2683] 	=	 0x68355E13;
    FractArr[2684] 	=	 0x28B96141;
    FractArr[2685] 	=	 0xF3E7D7EB;
    FractArr[2686] 	=	 0x4D7DB7FE;
    FractArr[2687] 	=	 0x757BA968;
    FractArr[2688] 	=	 0x41249A2A;
    FractArr[2689] 	=	 0x69113FE2;
    FractArr[2690] 	=	 0x3DA13E7A;
    FractArr[2691] 	=	 0x9561D3A0;
    FractArr[2692] 	=	 0x90094255;
    FractArr[2693] 	=	 0x07E32495;
    FractArr[2694] 	=	 0xF05FBDA7;
    FractArr[2695] 	=	 0x88748C87;
    FractArr[2696] 	=	 0x8EB5606D;
    FractArr[2697] 	=	 0x01501AD1;
    FractArr[2698] 	=	 0xC81C4792;
    FractArr[2699] 	=	 0x7BA6A7DE;
    FractArr[2700] 	=	 0x9B57C00F;
    FractArr[2701] 	=	 0xD7573F9F;
    FractArr[2702] 	=	 0x31865C4A;
    FractArr[2703] 	=	 0x800521CD;
    FractArr[2704] 	=	 0xE57F5FC9;
    FractArr[2705] 	=	 0xE0976E5D;
    FractArr[2706] 	=	 0x0B2E9D7B;
    FractArr[2707] 	=	 0x7192AB8B;
    FractArr[2708] 	=	 0xE919DB19;
    FractArr[2709] 	=	 0x2CB85C93;
    FractArr[2710] 	=	 0x1DE9A914;
    FractArr[2711] 	=	 0xB8AA4EFD;
    FractArr[2712] 	=	 0xADA629B5;
    FractArr[2713] 	=	 0x6FF1A163;
    FractArr[2714] 	=	 0x6C6E2D8E;
    FractArr[2715] 	=	 0x1892B4E6;
    FractArr[2716] 	=	 0xB88F80E4;
    FractArr[2717] 	=	 0xBCD15945;
    FractArr[2718] 	=	 0xA47B792F;
    FractArr[2719] 	=	 0xDF976243;
    FractArr[2720] 	=	 0x44E4D267;
    FractArr[2721] 	=	 0x80E49698;
    FractArr[2722] 	=	 0xAFF471C0;
    FractArr[2723] 	=	 0x636ED71F;
    FractArr[2724] 	=	 0xE140D75B;
    FractArr[2725] 	=	 0xF000798E;
    FractArr[2726] 	=	 0x313CDC2B;
    FractArr[2727] 	=	 0x29FC99AC;
    FractArr[2728] 	=	 0x2D7539A5;
    FractArr[2729] 	=	 0xFBFE58F6;
    FractArr[2730] 	=	 0xC755E663;
    FractArr[2731] 	=	 0xE71A7A6C;
    FractArr[2732] 	=	 0x164A2ACC;
    FractArr[2733] 	=	 0x39C4D6D7;
    FractArr[2734] 	=	 0xC7B657FB;
    FractArr[2735] 	=	 0x57F8A043;
    FractArr[2736] 	=	 0xE8F4D24D;
    FractArr[2737] 	=	 0x8C608220;
    FractArr[2738] 	=	 0x2B296FB2;
    FractArr[2739] 	=	 0xC0E986E0;
    FractArr[2740] 	=	 0x4C671DAD;
    FractArr[2741] 	=	 0x9D46A0D3;
    FractArr[2742] 	=	 0x67E40EA2;
    FractArr[2743] 	=	 0x0B568C27;
    FractArr[2744] 	=	 0x9B2D2EC2;
    FractArr[2745] 	=	 0xD882B750;
    FractArr[2746] 	=	 0x2E8BBCDB;
    FractArr[2747] 	=	 0x199CCFC7;
    FractArr[2748] 	=	 0x410FF27E;
    FractArr[2749] 	=	 0xE39651C5;
    FractArr[2750] 	=	 0x9561D457;
    FractArr[2751] 	=	 0x33E4DCBE;
    FractArr[2752] 	=	 0x31D9F205;
    FractArr[2753] 	=	 0xE3780EB0;
    FractArr[2754] 	=	 0x5EE1DF1E;
    FractArr[2755] 	=	 0x8AF04A2D;
    FractArr[2756] 	=	 0x66FEB577;
    FractArr[2757] 	=	 0xA3E584F1;
    FractArr[2758] 	=	 0xEDF5E3D8;
    FractArr[2759] 	=	 0x0CD45746;
    FractArr[2760] 	=	 0xD8D83E10;
    FractArr[2761] 	=	 0x628CAAE0;
    FractArr[2762] 	=	 0x8EA609BB;
    FractArr[2763] 	=	 0x01A9C0DB;
    FractArr[2764] 	=	 0xD3CE2F38;
    FractArr[2765] 	=	 0xF468ACF4;
    FractArr[2766] 	=	 0x31B31227;
    FractArr[2767] 	=	 0x2F394626;
    FractArr[2768] 	=	 0xA77FB78A;
    FractArr[2769] 	=	 0x17966C4C;
    FractArr[2770] 	=	 0x85C67691;
    FractArr[2771] 	=	 0xFF8C2D80;
    FractArr[2772] 	=	 0x95EBF500;
    FractArr[2773] 	=	 0x4E94BDD5;
    FractArr[2774] 	=	 0x3649D2A8;
    FractArr[2775] 	=	 0x6A0E0FCC;
    FractArr[2776] 	=	 0x41D34CB7;
    FractArr[2777] 	=	 0x2E1A43BA;
    FractArr[2778] 	=	 0x64E041C2;
    FractArr[2779] 	=	 0xBBDABBF1;
    FractArr[2780] 	=	 0x49CBE141;
    FractArr[2781] 	=	 0x226FD9F4;
    FractArr[2782] 	=	 0xCB381A40;
    FractArr[2783] 	=	 0xD70CFA95;
    FractArr[2784] 	=	 0x20666511;
    FractArr[2785] 	=	 0x1B5195D4;
    FractArr[2786] 	=	 0x39C7963C;
    FractArr[2787] 	=	 0x2B0E00FF;
    FractArr[2788] 	=	 0xD99834D4;
    FractArr[2789] 	=	 0xE48864EC;
    FractArr[2790] 	=	 0x1F978D07;
    FractArr[2791] 	=	 0x5EB95B51;
    FractArr[2792] 	=	 0x94E6CC4A;
    FractArr[2793] 	=	 0x34E7D81A;
    FractArr[2794] 	=	 0x308A99E8;
    FractArr[2795] 	=	 0x278691E0;
    FractArr[2796] 	=	 0x67A435B8;
    FractArr[2797] 	=	 0x6DC5127D;
    FractArr[2798] 	=	 0x892156D6;
    FractArr[2799] 	=	 0x3F17D974;
    FractArr[2800] 	=	 0xFFDC8143;
    FractArr[2801] 	=	 0xABF8FA00;
    FractArr[2802] 	=	 0x6B0996B6;
    FractArr[2803] 	=	 0x811C6608;
    FractArr[2804] 	=	 0xE5DDAAD2;
    FractArr[2805] 	=	 0x724E9EDB;
    FractArr[2806] 	=	 0xF5F6C82B;
    FractArr[2807] 	=	 0xCE8EFC24;
    FractArr[2808] 	=	 0x5AF09958;
    FractArr[2809] 	=	 0x84DC891B;
    FractArr[2810] 	=	 0x981C6169;
    FractArr[2811] 	=	 0xF6C91537;
    FractArr[2812] 	=	 0xD3E7743C;
    FractArr[2813] 	=	 0xDB749AD2;
    FractArr[2814] 	=	 0xF9563B95;
    FractArr[2815] 	=	 0xC7008EC2;
    FractArr[2816] 	=	 0x97165D41;
    FractArr[2817] 	=	 0xCCC2E567;
    FractArr[2818] 	=	 0x8D532447;
    FractArr[2819] 	=	 0xBD2A3DB7;
    FractArr[2820] 	=	 0x97D457A5;
    FractArr[2821] 	=	 0x321FBAC8;
    FractArr[2822] 	=	 0x53A4E2A9;
    FractArr[2823] 	=	 0x45A9A150;
    FractArr[2824] 	=	 0x7A52437F;
    FractArr[2825] 	=	 0x94C9281F;
    FractArr[2826] 	=	 0x50F1A0D4;
    FractArr[2827] 	=	 0xD654A9A9;
    FractArr[2828] 	=	 0x28E943E8;
    FractArr[2829] 	=	 0x140598A2;
    FractArr[2830] 	=	 0x28009A51;
    FractArr[2831] 	=	 0x0014CDA4;
    FractArr[2832] 	=	 0x5CCDA4B9;
    FractArr[2833] 	=	 0x6FCC3ED3;
    FractArr[2834] 	=	 0xD2445D42;
    FractArr[2835] 	=	 0x1150A923;
    FractArr[2836] 	=	 0xB8DD1037;
    FractArr[2837] 	=	 0xDDF4B9AD;
    FractArr[2838] 	=	 0x31093B2A;
    FractArr[2839] 	=	 0x26AAF10C;
    FractArr[2840] 	=	 0xC532495E;
    FractArr[2841] 	=	 0xB873025C;
    FractArr[2842] 	=	 0x8E638C71;
    FractArr[2843] 	=	 0x1B2FAF79;
    FractArr[2844] 	=	 0xD4C1529B;
    FractArr[2845] 	=	 0xEB45EA54;
    FractArr[2846] 	=	 0xF9AD556D;
    FractArr[2847] 	=	 0x42A736F4;
    FractArr[2848] 	=	 0x39D31555;
    FractArr[2849] 	=	 0x49404F8E;
    FractArr[2850] 	=	 0xD33503F4;
    FractArr[2851] 	=	 0x3B4B0458;
    FractArr[2852] 	=	 0xCEA57731;
    FractArr[2853] 	=	 0xB364BB4D;
    FractArr[2854] 	=	 0x2D5F59C6;
    FractArr[2855] 	=	 0x76E7D081;
    FractArr[2856] 	=	 0xE7C18533;
    FractArr[2857] 	=	 0x63F813F3;
    FractArr[2858] 	=	 0x2C1337C3;
    FractArr[2859] 	=	 0xD64CD757;
    FractArr[2860] 	=	 0xDD08E4AA;
    FractArr[2861] 	=	 0xF714028B;
    FractArr[2862] 	=	 0xBD3D39DA;
    FractArr[2863] 	=	 0xE1ED7405;
    FractArr[2864] 	=	 0xF1F0E7F8;
    FractArr[2865] 	=	 0x6EB95BBF;
    FractArr[2866] 	=	 0xE601CE93;
    FractArr[2867] 	=	 0x31699BB4;
    FractArr[2868] 	=	 0xD0AE7702;
    FractArr[2869] 	=	 0x796E31BF;
    FractArr[2870] 	=	 0xAFF4A8E3;
    FractArr[2871] 	=	 0xFCF3CF9A;
    FractArr[2872] 	=	 0xE2D53F37;
    FractArr[2873] 	=	 0xBEBA17FE;
    FractArr[2874] 	=	 0x742BABFA;
    FractArr[2875] 	=	 0xD300FFDE;
    FractArr[2876] 	=	 0x59D8B03A;
    FractArr[2877] 	=	 0xCCDCCDB5;
    FractArr[2878] 	=	 0xA826C530;
    FractArr[2879] 	=	 0xD2E9DAD3;
    FractArr[2880] 	=	 0x709BA749;
    FractArr[2881] 	=	 0xF97C62D2;
    FractArr[2882] 	=	 0xE8958333;
    FractArr[2883] 	=	 0x7B70590E;
    FractArr[2884] 	=	 0x14C1EDF7;
    FractArr[2885] 	=	 0x924191E7;
    FractArr[2886] 	=	 0x1CD87766;
    FractArr[2887] 	=	 0xE58B2491;
    FractArr[2888] 	=	 0x19A02787;
    FractArr[2889] 	=	 0x839E7B3F;
    FractArr[2890] 	=	 0x8AA2E2DC;
    FractArr[2891] 	=	 0xF05300FF;
    FractArr[2892] 	=	 0xF4EF88DF;
    FractArr[2893] 	=	 0xD305166D;
    FractArr[2894] 	=	 0x09C167AC;
    FractArr[2895] 	=	 0x2CC8BB24;
    FractArr[2896] 	=	 0x159E1CA3;
    FractArr[2897] 	=	 0xD7FA4086;
    FractArr[2898] 	=	 0xF7449A42;
    FractArr[2899] 	=	 0xDEA9F6CD;
    FractArr[2900] 	=	 0xD33A4DEA;
    FractArr[2901] 	=	 0xEDF81201;
    FractArr[2902] 	=	 0x7CC1ED94;
    FractArr[2903] 	=	 0xA30D1C63;
    FractArr[2904] 	=	 0xF478C5F2;
    FractArr[2905] 	=	 0xD9289560;
    FractArr[2906] 	=	 0xF5BFCFDF;
    FractArr[2907] 	=	 0x4B2DBA63;
    FractArr[2908] 	=	 0xDE5F1C63;
    FractArr[2909] 	=	 0xD3626F88;
    FractArr[2910] 	=	 0xEA225F75;
    FractArr[2911] 	=	 0x6222B0D1;
    FractArr[2912] 	=	 0x21414E36;
    FractArr[2913] 	=	 0x917B6E8C;
    FractArr[2914] 	=	 0x567A0EC6;
    FractArr[2915] 	=	 0xEAACB346;
    FractArr[2916] 	=	 0x83F66589;
    FractArr[2917] 	=	 0x3350CC15;
    FractArr[2918] 	=	 0x8E6E8D14;
    FractArr[2919] 	=	 0x3B380A65;
    FractArr[2920] 	=	 0x8C737ECF;
    FractArr[2921] 	=	 0xC0A52AFD;
    FractArr[2922] 	=	 0x2B352FD6;
    FractArr[2923] 	=	 0x7D5817EF;
    FractArr[2924] 	=	 0xE6852D95;
    FractArr[2925] 	=	 0x0588DD16;
    FractArr[2926] 	=	 0x7EF97494;
    FractArr[2927] 	=	 0xF55A8F9F;
    FractArr[2928] 	=	 0xB70C2F0B;
    FractArr[2929] 	=	 0xE3A8748A;
    FractArr[2930] 	=	 0x49548ABD;
    FractArr[2931] 	=	 0x6489CC57;
    FractArr[2932] 	=	 0x54CF6418;
    FractArr[2933] 	=	 0x2BB9D23F;
    FractArr[2934] 	=	 0x92D1AAE3;
    FractArr[2935] 	=	 0xFCD38D82;
    FractArr[2936] 	=	 0xAB521807;
    FractArr[2937] 	=	 0xE7B1E576;
    FractArr[2938] 	=	 0x1ED70B7E;
    FractArr[2939] 	=	 0xD3B755EF;
    FractArr[2940] 	=	 0xB8D4F4AC;
    FractArr[2941] 	=	 0x2393439E;
    FractArr[2942] 	=	 0xAB07B581;
    FractArr[2943] 	=	 0x4D4DF51A;
    FractArr[2944] 	=	 0x9AD04634;
    FractArr[2945] 	=	 0xE296A159;
    FractArr[2946] 	=	 0xE668CFCF;
    FractArr[2947] 	=	 0x5560E578;
    FractArr[2948] 	=	 0x4C07BCBF;
    FractArr[2949] 	=	 0xCDB9E275;
    FractArr[2950] 	=	 0x9E96C35F;
    FractArr[2951] 	=	 0xEC7FB00C;
    FractArr[2952] 	=	 0x82E0B2E1;
    FractArr[2953] 	=	 0xA5A7C719;
    FractArr[2954] 	=	 0x07BF4137;
    FractArr[2955] 	=	 0x73B637C2;
    FractArr[2956] 	=	 0xF08E5982;
    FractArr[2957] 	=	 0xC4E67A0F;
    FractArr[2958] 	=	 0x795CD5E6;
    FractArr[2959] 	=	 0x65688956;
    FractArr[2960] 	=	 0xEEA4064B;
    FractArr[2961] 	=	 0x0F78DC62;
    FractArr[2962] 	=	 0x47B8BE40;
    FractArr[2963] 	=	 0x85DC65D2;
    FractArr[2964] 	=	 0x7D9D44B0;
    FractArr[2965] 	=	 0x6B49D77A;
    FractArr[2966] 	=	 0xF5E14BA2;
    FractArr[2967] 	=	 0x2165B4B4;
    FractArr[2968] 	=	 0x80899636;
    FractArr[2969] 	=	 0xF3CF73E0;
    FractArr[2970] 	=	 0xD1976BFD;
    FractArr[2971] 	=	 0xF675DA84;
    FractArr[2972] 	=	 0xF21DE118;
    FractArr[2973] 	=	 0xEA157932;
    FractArr[2974] 	=	 0x5B0DF196;
    FractArr[2975] 	=	 0x66588D4F;
    FractArr[2976] 	=	 0x27748848;
    FractArr[2977] 	=	 0x2FAFA8B7;
    FractArr[2978] 	=	 0xE2A45611;
    FractArr[2979] 	=	 0x7468FAEC;
    FractArr[2980] 	=	 0x25C9A753;
    FractArr[2981] 	=	 0x67496774;
    FractArr[2982] 	=	 0x0BDB4EB7;
    FractArr[2983] 	=	 0xB3D845BC;
    FractArr[2984] 	=	 0xAE0F7200;
    FractArr[2985] 	=	 0xBD55E001;
    FractArr[2986] 	=	 0x2AEB021A;
    FractArr[2987] 	=	 0x5A6F4836;
    FractArr[2988] 	=	 0x5000E99E;
    FractArr[2989] 	=	 0x0A0CD08B;
    FractArr[2990] 	=	 0x8643A3B3;
    FractArr[2991] 	=	 0xF29A3ACE;
    FractArr[2992] 	=	 0xEF371723;
    FractArr[2993] 	=	 0x49CA756A;
    FractArr[2994] 	=	 0x4CD7082D;
    FractArr[2995] 	=	 0x471B9356;
    FractArr[2996] 	=	 0x4A353907;
    FractArr[2997] 	=	 0xFEDDCDFA;
    FractArr[2998] 	=	 0x8C11CA54;
    FractArr[2999] 	=	 0xD3BAD213;
    FractArr[3000] 	=	 0x9024A002;
    FractArr[3001] 	=	 0xA7CBAC09;
    FractArr[3002] 	=	 0xC9933C82;
    FractArr[3003] 	=	 0x431EA5AD;
    FractArr[3004] 	=	 0x8CB3CD38;
    FractArr[3005] 	=	 0xDBBCD3B4;
    FractArr[3006] 	=	 0x021E2BA7;
    FractArr[3007] 	=	 0x04577290;
    FractArr[3008] 	=	 0x645BD7FA;
    FractArr[3009] 	=	 0x04205AD1;
    FractArr[3010] 	=	 0x8795DE82;
    FractArr[3011] 	=	 0x84AA6AA8;
    FractArr[3012] 	=	 0x71ECC085;
    FractArr[3013] 	=	 0xAA47A9DE;
    FractArr[3014] 	=	 0x2A351FE7;
    FractArr[3015] 	=	 0xCB9D9312;
    FractArr[3016] 	=	 0x824EF79A;
    FractArr[3017] 	=	 0xCF482C77;
    FractArr[3018] 	=	 0x6D08AA1D;
    FractArr[3019] 	=	 0x380ED3CC;
    FractArr[3020] 	=	 0x28639B06;
    FractArr[3021] 	=	 0xE42087BC;
    FractArr[3022] 	=	 0xDDD660AB;
    FractArr[3023] 	=	 0x821209A4;
    FractArr[3024] 	=	 0x564A7168;
    FractArr[3025] 	=	 0x964B9B23;
    FractArr[3026] 	=	 0x6FD9B621;
    FractArr[3027] 	=	 0x9F343980;
    FractArr[3028] 	=	 0xED9F6A64;
    FractArr[3029] 	=	 0x14038368;
    FractArr[3030] 	=	 0x6DA5B6CF;
    FractArr[3031] 	=	 0xC82B13CB;
    FractArr[3032] 	=	 0x4A0D15F9;
    FractArr[3033] 	=	 0x3035042A;
    FractArr[3034] 	=	 0x17CDFB35;
    FractArr[3035] 	=	 0x99ACF3A1;
    FractArr[3036] 	=	 0xA1A64A4D;
    FractArr[3037] 	=	 0xAEA95253;
    FractArr[3038] 	=	 0x262549A4;
    FractArr[3039] 	=	 0x02A8A268;
    FractArr[3040] 	=	 0x02A0288A;
    FractArr[3041] 	=	 0x0228298A;
    FractArr[3042] 	=	 0x967295F6;
    FractArr[3043] 	=	 0x3DE6CAB7;
    FractArr[3044] 	=	 0x3122DFCE;
    FractArr[3045] 	=	 0x277826E4;
    FractArr[3046] 	=	 0xFAAEF81D;
    FractArr[3047] 	=	 0xF0BCCDE7;
    FractArr[3048] 	=	 0xE64592F5;
    FractArr[3049] 	=	 0xC85BDBDC;
    FractArr[3050] 	=	 0xDB02EA86;
    FractArr[3051] 	=	 0x580E44DA;
    FractArr[3052] 	=	 0x7672C6F1;
    FractArr[3053] 	=	 0xB4F304B0;
    FractArr[3054] 	=	 0x75E6638F;
    FractArr[3055] 	=	 0xD455D36E;
    FractArr[3056] 	=	 0xCA129D2C;
    FractArr[3057] 	=	 0x30C0BC69;
    FractArr[3058] 	=	 0x7DDC1889;
    FractArr[3059] 	=	 0xD7F83170;
    FractArr[3060] 	=	 0x337967CE;
    FractArr[3061] 	=	 0x8A38CAC5;
    FractArr[3062] 	=	 0xEF514A72;
    FractArr[3063] 	=	 0x6FCD5A7B;
    FractArr[3064] 	=	 0x376D77D3;
    FractArr[3065] 	=	 0x85AAD7D7;
    FractArr[3066] 	=	 0xD6E0447E;
    FractArr[3067] 	=	 0x55B5E98C;
    FractArr[3068] 	=	 0xEADEA2DB;
    FractArr[3069] 	=	 0x6E2DAEFB;
    FractArr[3070] 	=	 0xB5B5C26D;
    FractArr[3071] 	=	 0x1E295EDE;
    FractArr[3072] 	=	 0x6CB8F24C;
    FractArr[3073] 	=	 0x51C71816;
    FractArr[3074] 	=	 0xEF5641C7;
    FractArr[3075] 	=	 0xBF20B9F5;
    FractArr[3076] 	=	 0x5C90FCF0;
    FractArr[3077] 	=	 0x6AAD6F4B;
    FractArr[3078] 	=	 0x889E2770;
    FractArr[3079] 	=	 0xE0A1B130;
    FractArr[3080] 	=	 0x3C6E3A3E;
    FractArr[3081] 	=	 0xAEF4F3E3;
    FractArr[3082] 	=	 0xD64FCF7A;
    FractArr[3083] 	=	 0x096AFD35;
    FractArr[3084] 	=	 0x3473497C;
    FractArr[3085] 	=	 0xBD6D745A;
    FractArr[3086] 	=	 0xF812DC91;
    FractArr[3087] 	=	 0x3D7714EC;
    FractArr[3088] 	=	 0x4BDB72AB;
    FractArr[3089] 	=	 0xFCF15397;
    FractArr[3090] 	=	 0x05A2EEBA;
    FractArr[3091] 	=	 0x22A9ADBD;
    FractArr[3092] 	=	 0x086D3C34;
    FractArr[3093] 	=	 0xABFC553E;
    FractArr[3094] 	=	 0xD4E1EAE4;
    FractArr[3095] 	=	 0x6A83D2B0;
    FractArr[3096] 	=	 0x6D6DE95A;
    FractArr[3097] 	=	 0xD1D7D365;
    FractArr[3098] 	=	 0xA5541D7A;
    FractArr[3099] 	=	 0xB276F16A;
    FractArr[3100] 	=	 0xAFBD26EA;
    FractArr[3101] 	=	 0x8C7A5669;
    FractArr[3102] 	=	 0xD4855A76;
    FractArr[3103] 	=	 0xD0B5BDF3;
    FractArr[3104] 	=	 0x5D087369;
    FractArr[3105] 	=	 0x783F49FB;
    FractArr[3106] 	=	 0xEFB6D673;
    FractArr[3107] 	=	 0x2EC1237E;
    FractArr[3108] 	=	 0x47D9F28A;
    FractArr[3109] 	=	 0x62C171B3;
    FractArr[3110] 	=	 0x6BEAC772;
    FractArr[3111] 	=	 0xBA4B97CE;
    FractArr[3112] 	=	 0xA78A07F1;
    FractArr[3113] 	=	 0x24796B86;
    FractArr[3114] 	=	 0x77595982;
    FractArr[3115] 	=	 0x3C1FF322;
    FractArr[3116] 	=	 0x3FF5EB1A;
    FractArr[3117] 	=	 0x78D85C06;
    FractArr[3118] 	=	 0xDB743866;
    FractArr[3119] 	=	 0x89D98B2B;
    FractArr[3120] 	=	 0x641CCD60;
    FractArr[3121] 	=	 0x9FE4B80E;
    FractArr[3122] 	=	 0x19268C4A;
    FractArr[3123] 	=	 0x3B69A3D1;
    FractArr[3124] 	=	 0xDED03F2F;
    FractArr[3125] 	=	 0xAD3A2DA6;
    FractArr[3126] 	=	 0xEEF0BBC4;
    FractArr[3127] 	=	 0xD2D54DE3;
    FractArr[3128] 	=	 0x28E6F6E6;
    FractArr[3129] 	=	 0xC1ADDD8D;
    FractArr[3130] 	=	 0x025C593E;
    FractArr[3131] 	=	 0x8E586518;
    FractArr[3132] 	=	 0x557E779D;
    FractArr[3133] 	=	 0xD45407E8;
    FractArr[3134] 	=	 0x76B50650;
    FractArr[3135] 	=	 0x6288787B;
    FractArr[3136] 	=	 0x2F2020B1;
    FractArr[3137] 	=	 0x83932BF1;
    FractArr[3138] 	=	 0xBC02989E;
    FractArr[3139] 	=	 0x163AC163;
    FractArr[3140] 	=	 0x936BA4BD;
    FractArr[3141] 	=	 0x030E2CC9;
    FractArr[3142] 	=	 0xC05852C6;
    FractArr[3143] 	=	 0x3FFC41FC;
    FractArr[3144] 	=	 0xD251AFA9;
    FractArr[3145] 	=	 0xC5144F90;
    FractArr[3146] 	=	 0x914C9125;
    FractArr[3147] 	=	 0xDE4FC580;
    FractArr[3148] 	=	 0xEE4142C7;
    FractArr[3149] 	=	 0xD8B1E72B;
    FractArr[3150] 	=	 0x92724A95;
    FractArr[3151] 	=	 0xA6519A7B;
    FractArr[3152] 	=	 0xB7CA6AEA;
    FractArr[3153] 	=	 0x85785FB0;
    FractArr[3154] 	=	 0x81A9DB7C;
    FractArr[3155] 	=	 0x24B22455;
    FractArr[3156] 	=	 0x8EE7405B;
    FractArr[3157] 	=	 0x8886C56A;
    FractArr[3158] 	=	 0x020AACA8;
    FractArr[3159] 	=	 0x7ACF914A;
    FractArr[3160] 	=	 0x30A2D1D0;
    FractArr[3161] 	=	 0xC52E345C;
    FractArr[3162] 	=	 0x3D098695;
    FractArr[3163] 	=	 0xC48EA5EB;
    FractArr[3164] 	=	 0x20B77105;
    FractArr[3165] 	=	 0xA790578C;
    FractArr[3166] 	=	 0x6767973A;
    FractArr[3167] 	=	 0x3CA2A732;
    FractArr[3168] 	=	 0x4F4BEFF6;
    FractArr[3169] 	=	 0xE4D14C38;
    FractArr[3170] 	=	 0x710103EE;
    FractArr[3171] 	=	 0xBADAED83;
    FractArr[3172] 	=	 0x4E97397B;
    FractArr[3173] 	=	 0x79580AB7;
    FractArr[3174] 	=	 0xE5AAFC98;
    FractArr[3175] 	=	 0x8C6845EB;
    FractArr[3176] 	=	 0xF2630B46;
    FractArr[3177] 	=	 0xD5541BAE;
    FractArr[3178] 	=	 0x7FC74E8A;
    FractArr[3179] 	=	 0x6E9A4B53;
    FractArr[3180] 	=	 0x9CD5B0A2;
    FractArr[3181] 	=	 0x974EA84E;
    FractArr[3182] 	=	 0xBB91422E;
    FractArr[3183] 	=	 0x626B3DAE;
    FractArr[3184] 	=	 0x21BC6DCB;
    FractArr[3185] 	=	 0x5C5E839B;
    FractArr[3186] 	=	 0x07A3D39A;
    FractArr[3187] 	=	 0x72CA2A47;
    FractArr[3188] 	=	 0x715BED08;
    FractArr[3189] 	=	 0x425DB8F8;
    FractArr[3190] 	=	 0x01927C03;
    FractArr[3191] 	=	 0xFDD05186;
    FractArr[3192] 	=	 0xE5B44A2B;
    FractArr[3193] 	=	 0x4414CD17;
    FractArr[3194] 	=	 0xCEBA3757;
    FractArr[3195] 	=	 0x0053F7BA;
    FractArr[3196] 	=	 0x8DB99A1F;
    FractArr[3197] 	=	 0x37005647;
    FractArr[3198] 	=	 0x9AD758CD;
    FractArr[3199] 	=	 0x8DE16CE9;
    FractArr[3200] 	=	 0xABBAB773;
    FractArr[3201] 	=	 0x14A2793E;
    FractArr[3202] 	=	 0xE5CB3DE5;
    FractArr[3203] 	=	 0x3A6A2E51;
    FractArr[3204] 	=	 0xD5702CA9;
    FractArr[3205] 	=	 0xE0440D5E;
    FractArr[3206] 	=	 0xB89CAB65;
    FractArr[3207] 	=	 0xDE6377BA;
    FractArr[3208] 	=	 0xB632B6A5;
    FractArr[3209] 	=	 0x42770D30;
    FractArr[3210] 	=	 0x9D328A82;
    FractArr[3211] 	=	 0xDA77E84B;
    FractArr[3212] 	=	 0x2F2DBC16;
    FractArr[3213] 	=	 0x25F96680;
    FractArr[3214] 	=	 0x1E7B4E1B;
    FractArr[3215] 	=	 0x6D7FD5D5;
    FractArr[3216] 	=	 0xBC5A0EAB;
    FractArr[3217] 	=	 0x47D6D2C6;
    FractArr[3218] 	=	 0x4A1F49C6;
    FractArr[3219] 	=	 0x606E79D1;
    FractArr[3220] 	=	 0xDC274950;
    FractArr[3221] 	=	 0xF86AAE77;
    FractArr[3222] 	=	 0x5564BE7B;
    FractArr[3223] 	=	 0xEB7E912A;
    FractArr[3224] 	=	 0xC1D5873B;
    FractArr[3225] 	=	 0xFD413D1C;
    FractArr[3226] 	=	 0xABF733AC;
    FractArr[3227] 	=	 0xC7556D86;
    FractArr[3228] 	=	 0xDA5FD404;
    FractArr[3229] 	=	 0x9AD5FBE7;
    FractArr[3230] 	=	 0x29DF22A3;
    FractArr[3231] 	=	 0x656A68E6;
    FractArr[3232] 	=	 0x4E0D5D35;
    FractArr[3233] 	=	 0xC375BFA6;
    FractArr[3234] 	=	 0xA4E543CB;
    FractArr[3235] 	=	 0x53A4A64C;
    FractArr[3236] 	=	 0xA9528350;
    FractArr[3237] 	=	 0x2841E4AE;
    FractArr[3238] 	=	 0x0F484BE9;
    FractArr[3239] 	=	 0x51805514;
    FractArr[3240] 	=	 0x51001445;
    FractArr[3241] 	=	 0xEBB46D56;
    FractArr[3242] 	=	 0xB556D4DB;
    FractArr[3243] 	=	 0x398379B6;
    FractArr[3244] 	=	 0x719E1340;
    FractArr[3245] 	=	 0x7349E59F;
    FractArr[3246] 	=	 0x706B7161;
    FractArr[3247] 	=	 0xD2023AB0;
    FractArr[3248] 	=	 0x722A6300;
    FractArr[3249] 	=	 0x1FD67A1C;
    FractArr[3250] 	=	 0xECCFA359;
    FractArr[3251] 	=	 0x6B37D7F9;
    FractArr[3252] 	=	 0x2457E8AB;
    FractArr[3253] 	=	 0x59137BAD;
    FractArr[3254] 	=	 0xC2DF1369;
    FractArr[3255] 	=	 0x91488A26;
    FractArr[3256] 	=	 0x975B9E04;
    FractArr[3257] 	=	 0x35F76C6C;
    FractArr[3258] 	=	 0xF069DAD5;
    FractArr[3259] 	=	 0x597397E9;
    FractArr[3260] 	=	 0x3A47DB2A;
    FractArr[3261] 	=	 0x5BDECAA2;
    FractArr[3262] 	=	 0x3C95F693;
    FractArr[3263] 	=	 0x0CA3EE29;
    FractArr[3264] 	=	 0x7780E409;
    FractArr[3265] 	=	 0x771ABD63;
    FractArr[3266] 	=	 0x33FB2784;
    FractArr[3267] 	=	 0x878B1549;
    FractArr[3268] 	=	 0x59561911;
    FractArr[3269] 	=	 0x0ECEE1CB;
    FractArr[3270] 	=	 0x73CED900;
    FractArr[3271] 	=	 0xE4C48ED3;
    FractArr[3272] 	=	 0x687F5171;
    FractArr[3273] 	=	 0x365DA3CF;
    FractArr[3274] 	=	 0x53832192;
    FractArr[3275] 	=	 0xA015C85B;
    FractArr[3276] 	=	 0x89009ABC;
    FractArr[3277] 	=	 0xBCDA380E;
    FractArr[3278] 	=	 0x8E6C85E0;
    FractArr[3279] 	=	 0xE06BFD71;
    FractArr[3280] 	=	 0x63D50CF3;
    FractArr[3281] 	=	 0x15CE1EB1;
    FractArr[3282] 	=	 0x4D6F8213;
    FractArr[3283] 	=	 0xDFCD969F;
    FractArr[3284] 	=	 0x7500FFAE;
    FractArr[3285] 	=	 0x15854ECF;
    FractArr[3286] 	=	 0x6A497349;
    FractArr[3287] 	=	 0x1A6DED3F;
    FractArr[3288] 	=	 0x74933949;
    FractArr[3289] 	=	 0x3E765B61;
    FractArr[3290] 	=	 0x78808161;
    FractArr[3291] 	=	 0xE0A80707;
    FractArr[3292] 	=	 0x499A6A1F;
    FractArr[3293] 	=	 0x6DD09B75;
    FractArr[3294] 	=	 0xE28AA5EC;
    FractArr[3295] 	=	 0x9298AFDC;
    FractArr[3296] 	=	 0x46B1805B;
    FractArr[3297] 	=	 0x50DA09BB;
    FractArr[3298] 	=	 0xEBF9C18D;
    FractArr[3299] 	=	 0x95569C9E;
    FractArr[3300] 	=	 0x167797C6;
    FractArr[3301] 	=	 0xC26AAA95;
    FractArr[3302] 	=	 0x6FCBCED4;
    FractArr[3303] 	=	 0xBD11BC25;
    FractArr[3304] 	=	 0x46DAC7BE;
    FractArr[3305] 	=	 0x39705032;
    FractArr[3306] 	=	 0xACD91ECF;
    FractArr[3307] 	=	 0x727803BF;
    FractArr[3308] 	=	 0xA3AD2FF9;
    FractArr[3309] 	=	 0xF4795183;
    FractArr[3310] 	=	 0xBB648DF6;
    FractArr[3311] 	=	 0x91AF50F3;
    FractArr[3312] 	=	 0x3D6DE021;
    FractArr[3313] 	=	 0xD73AFD73;
    FractArr[3314] 	=	 0x56AF899B;
    FractArr[3315] 	=	 0x5F0B2F8C;
    FractArr[3316] 	=	 0x39B4EACC;
    FractArr[3317] 	=	 0x37C79E92;
    FractArr[3318] 	=	 0xCBAFC9A0;
    FractArr[3319] 	=	 0x3696FDAD;
    FractArr[3320] 	=	 0xD03C0896;
    FractArr[3321] 	=	 0xE855676E;
    FractArr[3322] 	=	 0xA45EF19D;
    FractArr[3323] 	=	 0xFDEDBF75;
    FractArr[3324] 	=	 0x57365023;
    FractArr[3325] 	=	 0x63716F2F;
    FractArr[3326] 	=	 0x8DEC3629;
    FractArr[3327] 	=	 0xD3A68FC9;
    FractArr[3328] 	=	 0x8BDA5BEB;
    FractArr[3329] 	=	 0x44BAB6E8;
    FractArr[3330] 	=	 0x3D910CAB;
    FractArr[3331] 	=	 0x8C2D1FCB;
    FractArr[3332] 	=	 0x19DBF74B;
    FractArr[3333] 	=	 0x0FADB8E2;
    FractArr[3334] 	=	 0xA4299645;
    FractArr[3335] 	=	 0x2B418790;
    FractArr[3336] 	=	 0x1FD45712;
    FractArr[3337] 	=	 0x9831F15A;
    FractArr[3338] 	=	 0x324AB2EA;
    FractArr[3339] 	=	 0x8E2A767B;
    FractArr[3340] 	=	 0xAD4D0E1E;
    FractArr[3341] 	=	 0xC46B7886;
    FractArr[3342] 	=	 0x2F5DD1B7;
    FractArr[3343] 	=	 0xC2FC19DB;
    FractArr[3344] 	=	 0x39236FC4;
    FractArr[3345] 	=	 0x360C7AC5;
    FractArr[3346] 	=	 0x79CBB2B0;
    FractArr[3347] 	=	 0x8C32850A;
    FractArr[3348] 	=	 0xE3068EBA;
    FractArr[3349] 	=	 0x956B7DDC;
    FractArr[3350] 	=	 0x1CEBC39F;
    FractArr[3351] 	=	 0x25B7E2C1;
    FractArr[3352] 	=	 0x75A0E8E3;
    FractArr[3353] 	=	 0xE43F00FF;
    FractArr[3354] 	=	 0xE2D7DC56;
    FractArr[3355] 	=	 0x2720D9C2;
    FractArr[3356] 	=	 0x6BB60C04;
    FractArr[3357] 	=	 0x1356ABCA;
    FractArr[3358] 	=	 0xF96FAD7C;
    FractArr[3359] 	=	 0xCBA7119D;
    FractArr[3360] 	=	 0xE9364BAC;
    FractArr[3361] 	=	 0xD623B58B;
    FractArr[3362] 	=	 0xC46E6EB7;
    FractArr[3363] 	=	 0x0767E036;
    FractArr[3364] 	=	 0xE241C515;
    FractArr[3365] 	=	 0x06E66D48;
    FractArr[3366] 	=	 0xFD802C75;
    FractArr[3367] 	=	 0x3529EAE5;
    FractArr[3368] 	=	 0x32C3122F;
    FractArr[3369] 	=	 0xEA408616;
    FractArr[3370] 	=	 0x9BD58E7B;
    FractArr[3371] 	=	 0x0D898653;
    FractArr[3372] 	=	 0xAD8B9D47;
    FractArr[3373] 	=	 0x370C63EA;
    FractArr[3374] 	=	 0xF7EA9C57;
    FractArr[3375] 	=	 0x7527A4E5;
    FractArr[3376] 	=	 0x843A6A69;
    FractArr[3377] 	=	 0x83420CB7;
    FractArr[3378] 	=	 0xE9B9DE83;
    FractArr[3379] 	=	 0xCB469EED;
    FractArr[3380] 	=	 0x42D5557E;
    FractArr[3381] 	=	 0x3961B595;
    FractArr[3382] 	=	 0x050B5959;
    FractArr[3383] 	=	 0x0EC0CCCB;
    FractArr[3384] 	=	 0x6CD3A26B;
    FractArr[3385] 	=	 0x0C37D0BC;
    FractArr[3386] 	=	 0x74B2DE93;
    FractArr[3387] 	=	 0x0D5822CB;
    FractArr[3388] 	=	 0xD3B5EBBD;
    FractArr[3389] 	=	 0xC50BC82C;
    FractArr[3390] 	=	 0xBD294677;
    FractArr[3391] 	=	 0xC5CA390E;
    FractArr[3392] 	=	 0xD33C3C59;
    FractArr[3393] 	=	 0xFFC662A6;
    FractArr[3394] 	=	 0xF59EEE00;
    FractArr[3395] 	=	 0x92E9DFCF;
    FractArr[3396] 	=	 0x7852C8DA;
    FractArr[3397] 	=	 0x65F4365A;
    FractArr[3398] 	=	 0x60D46BC6;
    FractArr[3399] 	=	 0x7C8B54B7;
    FractArr[3400] 	=	 0x67A0A88C;
    FractArr[3401] 	=	 0x6D551C73;
    FractArr[3402] 	=	 0xF2B55643;
    FractArr[3403] 	=	 0x342BDE1A;
    FractArr[3404] 	=	 0xBE1FCFBD;
    FractArr[3405] 	=	 0xEA8FDD5F;
    FractArr[3406] 	=	 0x75AD4F7F;
    FractArr[3407] 	=	 0x57A7CB52;
    FractArr[3408] 	=	 0x0CF07FF8;
    FractArr[3409] 	=	 0x49C72D5E;
    FractArr[3410] 	=	 0x896C461E;
    FractArr[3411] 	=	 0x6BEA8571;
    FractArr[3412] 	=	 0x3C4FCB5E;
    FractArr[3413] 	=	 0x34B0B57C;
    FractArr[3414] 	=	 0x98B332D6;
    FractArr[3415] 	=	 0xB5B365D0;
    FractArr[3416] 	=	 0x56FA0046;
    FractArr[3417] 	=	 0x1E29B485;
    FractArr[3418] 	=	 0xDCE34403;
    FractArr[3419] 	=	 0xB1CAD256;
    FractArr[3420] 	=	 0xBC26DD70;
    FractArr[3421] 	=	 0xBE30FC9F;
    FractArr[3422] 	=	 0x59914DB7;
    FractArr[3423] 	=	 0x2D1F6358;
    FractArr[3424] 	=	 0x9792BE69;
    FractArr[3425] 	=	 0x8C611411;
    FractArr[3426] 	=	 0x2A72A08E;
    FractArr[3427] 	=	 0x2EDAB3CD;
    FractArr[3428] 	=	 0x54BFCC32;
    FractArr[3429] 	=	 0xBBF6A135;
    FractArr[3430] 	=	 0x7C5A5004;
    FractArr[3431] 	=	 0xFF18F70F;
    FractArr[3432] 	=	 0x02164A00;
    FractArr[3433] 	=	 0x3F28ADAD;
    FractArr[3434] 	=	 0x57D7CDB8;
    FractArr[3435] 	=	 0x6A9A67BA;
    FractArr[3436] 	=	 0x97B674BA;
    FractArr[3437] 	=	 0x87C90C0D;
    FractArr[3438] 	=	 0x7AD4E71C;
    FractArr[3439] 	=	 0x7330C68A;
    FractArr[3440] 	=	 0xEBA8D7D4;
    FractArr[3441] 	=	 0xA30E3A57;
    FractArr[3442] 	=	 0x599E6166;
    FractArr[3443] 	=	 0x52CC8B84;
    FractArr[3444] 	=	 0x0F2501C7;
    FractArr[3445] 	=	 0x832B8EE3;
    FractArr[3446] 	=	 0x9F274336;
    FractArr[3447] 	=	 0xEAB976FC;
    FractArr[3448] 	=	 0x3D98D865;
    FractArr[3449] 	=	 0x3AF4DA20;
    FractArr[3450] 	=	 0x96848A61;
    FractArr[3451] 	=	 0x50CC53AF;
    FractArr[3452] 	=	 0xBA6AC8D4;
    FractArr[3453] 	=	 0x7E0D9D1A;
    FractArr[3454] 	=	 0xE39186A3;
    FractArr[3455] 	=	 0x7A1A9CC8;
    FractArr[3456] 	=	 0x951A889A;
    FractArr[3457] 	=	 0x64717A4D;
    FractArr[3458] 	=	 0xA5786032;
    FractArr[3459] 	=	 0xD6A729A6;
    FractArr[3460] 	=	 0xA2280AA2;
    FractArr[3461] 	=	 0x35BC3780;
    FractArr[3462] 	=	 0x39E6481A;
    FractArr[3463] 	=	 0x922C586E;
    FractArr[3464] 	=	 0x5D96F2A8;
    FractArr[3465] 	=	 0x21C51FB8;
    FractArr[3466] 	=	 0x35C7C0F4;
    FractArr[3467] 	=	 0x56A45FD0;
    FractArr[3468] 	=	 0xD59D201E;
    FractArr[3469] 	=	 0x02DD1ED9;
    FractArr[3470] 	=	 0xE56026C2;
    FractArr[3471] 	=	 0xA8B3E454;
    FractArr[3472] 	=	 0x171E0B6E;
    FractArr[3473] 	=	 0xE4F436BE;
    FractArr[3474] 	=	 0xD20C0957;
    FractArr[3475] 	=	 0x8925CA5B;
    FractArr[3476] 	=	 0xCE18B8B6;
    FractArr[3477] 	=	 0x17B7D633;
    FractArr[3478] 	=	 0xA8DCFAC5;
    FractArr[3479] 	=	 0xC55B5BB0;
    FractArr[3480] 	=	 0xDD8F3A71;
    FractArr[3481] 	=	 0x27732B9F;
    FractArr[3482] 	=	 0xC64F4FBE;
    FractArr[3483] 	=	 0xCA393FBE;
    FractArr[3484] 	=	 0x4BC412B1;
    FractArr[3485] 	=	 0x462E0915;
    FractArr[3486] 	=	 0xB6B5BBB5;
    FractArr[3487] 	=	 0xD3BAB55D;
    FractArr[3488] 	=	 0x7DF4BBBB;
    FractArr[3489] 	=	 0x91476BB4;
    FractArr[3490] 	=	 0x9174EE41;
    FractArr[3491] 	=	 0x5396F76A;
    FractArr[3492] 	=	 0x5FDD71EB;
    FractArr[3493] 	=	 0x7D3A17B4;
    FractArr[3494] 	=	 0xF4ADDCBA;
    FractArr[3495] 	=	 0x11CB544A;
    FractArr[3496] 	=	 0x994EF727;
    FractArr[3497] 	=	 0x3A92182A;
    FractArr[3498] 	=	 0xAFB97E56;
    FractArr[3499] 	=	 0xB21DFEC6;
    FractArr[3500] 	=	 0xCF94D4BD;
    FractArr[3501] 	=	 0xD121EC2D;
    FractArr[3502] 	=	 0x63558122;
    FractArr[3503] 	=	 0x3D73DF50;
    FractArr[3504] 	=	 0x54B5C771;
    FractArr[3505] 	=	 0x9FBE4683;
    FractArr[3506] 	=	 0xB3B617C3;
    FractArr[3507] 	=	 0x3A6BD2CA;
    FractArr[3508] 	=	 0x04D7E8B4;
    FractArr[3509] 	=	 0xE43552F2;
    FractArr[3510] 	=	 0xE938BF2F;
    FractArr[3511] 	=	 0xE29902D3;
    FractArr[3512] 	=	 0xE81127CD;
    FractArr[3513] 	=	 0xC2AC659A;
    FractArr[3514] 	=	 0xCA8BB416;
    FractArr[3515] 	=	 0x9B8F9776;
    FractArr[3516] 	=	 0xBEC29F24;
    FractArr[3517] 	=	 0x2E02AE66;
    FractArr[3518] 	=	 0xD74EE774;
    FractArr[3519] 	=	 0xAF69EDD2;
    FractArr[3520] 	=	 0xA3ECCCCB;
    FractArr[3521] 	=	 0x5333965F;
    FractArr[3522] 	=	 0x1BCDA5DB;
    FractArr[3523] 	=	 0x178800FF;
    FractArr[3524] 	=	 0x3883EE9A;
    FractArr[3525] 	=	 0x12C180B4;
    FractArr[3526] 	=	 0x00FF009E;
    FractArr[3527] 	=	 0x7CADE61E;
    FractArr[3528] 	=	 0xCF9AF13B;
    FractArr[3529] 	=	 0x4EA4CBA4;
    FractArr[3530] 	=	 0x25B6BA41;
    FractArr[3531] 	=	 0xB5FC8F95;
    FractArr[3532] 	=	 0x3D00FF8C;
    FractArr[3533] 	=	 0x1ED3C7B9;
    FractArr[3534] 	=	 0x78A5B5F5;
    FractArr[3535] 	=	 0xF0BD4973;
    FractArr[3536] 	=	 0x30C1F0AC;
    FractArr[3537] 	=	 0xAD34DFDA;
    FractArr[3538] 	=	 0x6577B964;
    FractArr[3539] 	=	 0x78C03E1F;
    FractArr[3540] 	=	 0x7BA623E3;
    FractArr[3541] 	=	 0x748657D6;
    FractArr[3542] 	=	 0x6AD909BF;
    FractArr[3543] 	=	 0xE0A2968C;
    FractArr[3544] 	=	 0x994FF9A8;
    FractArr[3545] 	=	 0x9963B5F2;
    FractArr[3546] 	=	 0xFB18A754;
    FractArr[3547] 	=	 0xF04BDE3B;
    FractArr[3548] 	=	 0xA3CE3915;
    FractArr[3549] 	=	 0xAF75EC4D;
    FractArr[3550] 	=	 0x8AB3B911;
    FractArr[3551] 	=	 0x2CDBB655;
    FractArr[3552] 	=	 0x9030436E;
    FractArr[3553] 	=	 0x70F77190;
    FractArr[3554] 	=	 0x310ED033;
    FractArr[3555] 	=	 0x0C9D065D;
    FractArr[3556] 	=	 0x8AAC7057;
    FractArr[3557] 	=	 0x0C004880;
    FractArr[3558] 	=	 0xD3B45505;
    FractArr[3559] 	=	 0xBBACBD85;
    FractArr[3560] 	=	 0x70403324;
    FractArr[3561] 	=	 0xF5518F51;
    FractArr[3562] 	=	 0x30515AAB;
    FractArr[3563] 	=	 0x0738105C;
    FractArr[3564] 	=	 0x7309F99A;
    FractArr[3565] 	=	 0x3DD48D3A;
    FractArr[3566] 	=	 0xD23D142D;
    FractArr[3567] 	=	 0x6C49DCFD;
    FractArr[3568] 	=	 0x7603218D;
    FractArr[3569] 	=	 0x6FF38A33;
    FractArr[3570] 	=	 0xCC90DE12;
    FractArr[3571] 	=	 0xB65D7343;
    FractArr[3572] 	=	 0x1B157EBF;
    FractArr[3573] 	=	 0xD729AFE6;
    FractArr[3574] 	=	 0x65D77CAF;
    FractArr[3575] 	=	 0x2B7D9253;
    FractArr[3576] 	=	 0x9E334248;
    FractArr[3577] 	=	 0x37C60B81;
    FractArr[3578] 	=	 0x6CEA7366;
    FractArr[3579] 	=	 0x3D77B7ED;
    FractArr[3580] 	=	 0x8049672B;
    FractArr[3581] 	=	 0x9271414C;
    FractArr[3582] 	=	 0x81B5986B;
    FractArr[3583] 	=	 0x3A2CC08D;
    FractArr[3584] 	=	 0x4BB4EE9A;
    FractArr[3585] 	=	 0xBDE24062;
    FractArr[3586] 	=	 0xC75EC148;
    FractArr[3587] 	=	 0xD01A492D;
    FractArr[3588] 	=	 0xE5D27E78;
    FractArr[3589] 	=	 0x4F607440;
    FractArr[3590] 	=	 0x1A546C74;
    FractArr[3591] 	=	 0xABF485AF;
    FractArr[3592] 	=	 0xDC305F0B;
    FractArr[3593] 	=	 0x0FEEB4DC;
    FractArr[3594] 	=	 0x77DA0893;
    FractArr[3595] 	=	 0x5BED531F;
    FractArr[3596] 	=	 0x482C5DB0;
    FractArr[3597] 	=	 0xC9121512;
    FractArr[3598] 	=	 0x3FC23FD3;
    FractArr[3599] 	=	 0xC3EC8FC6;
    FractArr[3600] 	=	 0x877BA62C;
    FractArr[3601] 	=	 0x67C9EDDC;
    FractArr[3602] 	=	 0x7261EE35;
    FractArr[3603] 	=	 0xADD24AC8;
    FractArr[3604] 	=	 0xCE333F1D;
    FractArr[3605] 	=	 0xD1928AA9;
    FractArr[3606] 	=	 0xD3B29333;
    FractArr[3607] 	=	 0x1A113CDE;
    FractArr[3608] 	=	 0xADB7DEE7;
    FractArr[3609] 	=	 0x0AC0BAA0;
    FractArr[3610] 	=	 0x2BFB07C1;
    FractArr[3611] 	=	 0xAD2C615A;
    FractArr[3612] 	=	 0x23C15986;
    FractArr[3613] 	=	 0xD666A8B0;
    FractArr[3614] 	=	 0x4604E0AD;
    FractArr[3615] 	=	 0xF3DC31AA;
    FractArr[3616] 	=	 0x7430B45E;
    FractArr[3617] 	=	 0x7700FF22;
    FractArr[3618] 	=	 0xA75E7F4D;
    FractArr[3619] 	=	 0xEECDAB24;
    FractArr[3620] 	=	 0x89D322C9;
    FractArr[3621] 	=	 0x4F62E63B;
    FractArr[3622] 	=	 0xB6799AAB;
    FractArr[3623] 	=	 0x621EC9B4;
    FractArr[3624] 	=	 0x793D57E4;
    FractArr[3625] 	=	 0xE7E73CE2;
    FractArr[3626] 	=	 0x7F0E6BFD;
    FractArr[3627] 	=	 0x877C9511;
    FractArr[3628] 	=	 0x18B66BFD;
    FractArr[3629] 	=	 0xCCC5C869;
    FractArr[3630] 	=	 0x6CC77EEF;
    FractArr[3631] 	=	 0xE1B4487A;
    FractArr[3632] 	=	 0xE53F6E67;
    FractArr[3633] 	=	 0x9E02D7A2;
    FractArr[3634] 	=	 0xFEF1CF25;
    FractArr[3635] 	=	 0x26F128B5;
    FractArr[3636] 	=	 0xAAF58F7F;
    FractArr[3637] 	=	 0x7341ADFA;
    FractArr[3638] 	=	 0xB6B6D09D;
    FractArr[3639] 	=	 0x35B4C61F;
    FractArr[3640] 	=	 0x6A83BCAD;
    FractArr[3641] 	=	 0x1ED893BA;
    FractArr[3642] 	=	 0x221E882B;
    FractArr[3643] 	=	 0xB4FEF1CF;
    FractArr[3644] 	=	 0x8943FCE8;
    FractArr[3645] 	=	 0xD2FADF01;
    FractArr[3646] 	=	 0x98C3B4FA;
    FractArr[3647] 	=	 0x9E746EE9;
    FractArr[3648] 	=	 0x3A506DA7;
    FractArr[3649] 	=	 0x1FDDCF48;
    FractArr[3650] 	=	 0xF1B25995;
    FractArr[3651] 	=	 0x98016500;
    FractArr[3652] 	=	 0xD11A7B10;
    FractArr[3653] 	=	 0x3FB6D3FE;
    FractArr[3654] 	=	 0x679DBFC0;
    FractArr[3655] 	=	 0x62E9C8FB;
    FractArr[3656] 	=	 0xD0988F94;
    FractArr[3657] 	=	 0x046AC8D4;
    FractArr[3658] 	=	 0x7CBD92A9;
    FractArr[3659] 	=	 0x89BC9934;
    FractArr[3660] 	=	 0xD480D4D4;
    FractArr[3661] 	=	 0xF60A524B;
    FractArr[3662] 	=	 0x990CBD69;
    FractArr[3663] 	=	 0xA2929A32;
    FractArr[3664] 	=	 0x5BE9785A;
    FractArr[3665] 	=	 0x339A0B21;
    FractArr[3666] 	=	 0xA50F3A48;
    FractArr[3667] 	=	 0xE2FBAA07;
    FractArr[3668] 	=	 0x763A1A98;
    FractArr[3669] 	=	 0x6DA97191;
    FractArr[3670] 	=	 0x3E2C8A78;
    FractArr[3671] 	=	 0x9BDD8D68;
    FractArr[3672] 	=	 0xE99AE7EE;
    FractArr[3673] 	=	 0xD361ADED;
    FractArr[3674] 	=	 0xA3487B12;
    FractArr[3675] 	=	 0x6521398E;
    FractArr[3676] 	=	 0xF7B90956;
    FractArr[3677] 	=	 0xB9E19DE7;
    FractArr[3678] 	=	 0x00032307;
    FractArr[3679] 	=	 0xBD0F00FF;
    FractArr[3680] 	=	 0x0630FA72;
    FractArr[3681] 	=	 0x414640E2;
    FractArr[3682] 	=	 0x21FAC846;
    FractArr[3683] 	=	 0xBBE6F523;
    FractArr[3684] 	=	 0x4769127D;
    FractArr[3685] 	=	 0xCE124FC3;
    FractArr[3686] 	=	 0x5A347125;
    FractArr[3687] 	=	 0x0CE44891;
    FractArr[3688] 	=	 0x40862277;
    FractArr[3689] 	=	 0x7C853D00;
    FractArr[3690] 	=	 0xB45A7B56;
    FractArr[3691] 	=	 0x9F8C6AB1;
    FractArr[3692] 	=	 0xFBD2AEBA;
    FractArr[3693] 	=	 0xF4DCF5BC;
    FractArr[3694] 	=	 0x8B8B30F0;
    FractArr[3695] 	=	 0x1FFEDF92;
    FractArr[3696] 	=	 0xABEB8CFC;
    FractArr[3697] 	=	 0xEF6663E1;
    FractArr[3698] 	=	 0x56DC2D73;
    FractArr[3699] 	=	 0xA4A9BCC1;
    FractArr[3700] 	=	 0xB1F0B081;
    FractArr[3701] 	=	 0x3B72805C;
    FractArr[3702] 	=	 0xDDC7F19C;
    FractArr[3703] 	=	 0xE0C33439;
    FractArr[3704] 	=	 0x69513CA9;
    FractArr[3705] 	=	 0x92DAE903;
    FractArr[3706] 	=	 0x58EC724F;
    FractArr[3707] 	=	 0x36B56BBC;
    FractArr[3708] 	=	 0x2301CA9E;
    FractArr[3709] 	=	 0x29FD988E;
    FractArr[3710] 	=	 0x78DC4497;
    FractArr[3711] 	=	 0xF998056A;
    FractArr[3712] 	=	 0xDB564D82;
    FractArr[3713] 	=	 0x65777E78;
    FractArr[3714] 	=	 0x5ACF399B;
    FractArr[3715] 	=	 0xB6851FF4;
    FractArr[3716] 	=	 0xF85EF0B6;
    FractArr[3717] 	=	 0x8A603882;
    FractArr[3718] 	=	 0x6CF48E34;
    FractArr[3719] 	=	 0xF0024054;
    FractArr[3720] 	=	 0xBED2017A;
    FractArr[3721] 	=	 0xA93A1767;
    FractArr[3722] 	=	 0x545B0925;
    FractArr[3723] 	=	 0x4D2ADE75;
    FractArr[3724] 	=	 0xE7D0374A;
    FractArr[3725] 	=	 0x0FF5D3A7;
    FractArr[3726] 	=	 0xD6E8DC05;
    FractArr[3727] 	=	 0x3B5A8E36;
    FractArr[3728] 	=	 0xEA18C34D;
    FractArr[3729] 	=	 0xAEA9965B;
    FractArr[3730] 	=	 0x74791134;
    FractArr[3731] 	=	 0x21688CFA;
    FractArr[3732] 	=	 0x00DC6692;
    FractArr[3733] 	=	 0xB6D6771F;
    FractArr[3734] 	=	 0x27CD487C;
    FractArr[3735] 	=	 0x2CB94A8C;
    FractArr[3736] 	=	 0x8E01A7AA;
    FractArr[3737] 	=	 0x722BE140;
    FractArr[3738] 	=	 0x83EC57CD;
    FractArr[3739] 	=	 0x2B7D1DE5;
    FractArr[3740] 	=	 0x641571E6;
    FractArr[3741] 	=	 0xA2541DAA;
    FractArr[3742] 	=	 0xA59D3295;
    FractArr[3743] 	=	 0x0369D9EB;
    FractArr[3744] 	=	 0x2B496079;
    FractArr[3745] 	=	 0x5259ED93;
    FractArr[3746] 	=	 0xE4ECA978;
    FractArr[3747] 	=	 0x0E1CA12C;
    FractArr[3748] 	=	 0xA935FECD;
    FractArr[3749] 	=	 0xD72C6EE2;
    FractArr[3750] 	=	 0x76655EE9;
    FractArr[3751] 	=	 0xF5E4E64E;
    FractArr[3752] 	=	 0xDAEB70AC;
    FractArr[3753] 	=	 0x5C544B5D;
    FractArr[3754] 	=	 0x3A3297D5;
    FractArr[3755] 	=	 0x7D66AD1B;
    FractArr[3756] 	=	 0x14A2CA65;
    FractArr[3757] 	=	 0x349B5FCF;
    FractArr[3758] 	=	 0x73293CD8;
    FractArr[3759] 	=	 0x88074376;
    FractArr[3760] 	=	 0xFFD8DD13;
    FractArr[3761] 	=	 0xE8678500;
    FractArr[3762] 	=	 0x6B7D783F;
    FractArr[3763] 	=	 0xF7D134D1;
    FractArr[3764] 	=	 0x28F50A7F;
    FractArr[3765] 	=	 0x38B2A161;
    FractArr[3766] 	=	 0x712662EB;
    FractArr[3767] 	=	 0x5D17BED7;
    FractArr[3768] 	=	 0xAE11482A;
    FractArr[3769] 	=	 0x32897B52;
    FractArr[3770] 	=	 0x229C88CC;
    FractArr[3771] 	=	 0x62EB7DFD;
    FractArr[3772] 	=	 0xD4954DC3;
    FractArr[3773] 	=	 0xFBB7132B;
    FractArr[3774] 	=	 0xF34FADA2;
    FractArr[3775] 	=	 0xB945976B;
    FractArr[3776] 	=	 0xD2CFF1F9;
    FractArr[3777] 	=	 0x6D01A0B6;
    FractArr[3778] 	=	 0x1D07A372;
    FractArr[3779] 	=	 0xB870EBAB;
    FractArr[3780] 	=	 0x1921344A;
    FractArr[3781] 	=	 0x7FF73E46;
    FractArr[3782] 	=	 0xADC863EB;
    FractArr[3783] 	=	 0x366D725E;
    FractArr[3784] 	=	 0xA76F0B57;
    FractArr[3785] 	=	 0x60C19745;
    FractArr[3786] 	=	 0xE74A5F38;
    FractArr[3787] 	=	 0x047F6D75;
    FractArr[3788] 	=	 0x988EF1E1;
    FractArr[3789] 	=	 0xD9EE6AA7;
    FractArr[3790] 	=	 0x75FE986F;
    FractArr[3791] 	=	 0x778CEAC3;
    FractArr[3792] 	=	 0xF87A4D1E;
    FractArr[3793] 	=	 0xEC4E2A7A;
    FractArr[3794] 	=	 0xBBD472E3;
    FractArr[3795] 	=	 0x2457E273;
    FractArr[3796] 	=	 0xCC9AE48D;
    FractArr[3797] 	=	 0xF3955D9F;
    FractArr[3798] 	=	 0x166BFD86;
    FractArr[3799] 	=	 0xAEAA7962;
    FractArr[3800] 	=	 0x3577AD4F;
    FractArr[3801] 	=	 0x508A6C18;
    FractArr[3802] 	=	 0xA62EA7B9;
    FractArr[3803] 	=	 0x54C99CEF;
    FractArr[3804] 	=	 0x5B3DF3DE;
    FractArr[3805] 	=	 0xF5C94935;
    FractArr[3806] 	=	 0xAD4FECAA;
    FractArr[3807] 	=	 0x964C3C61;
    FractArr[3808] 	=	 0x4630D2C8;
    FractArr[3809] 	=	 0xA9F7BD90;
    FractArr[3810] 	=	 0xF7FEE905;
    FractArr[3811] 	=	 0x9DA058EB;
    FractArr[3812] 	=	 0x4FEA69DD;
    FractArr[3813] 	=	 0x5C1C65AD;
    FractArr[3814] 	=	 0xD046E4CA;
    FractArr[3815] 	=	 0x3FDE3FBD;
    FractArr[3816] 	=	 0x586F399D;
    FractArr[3817] 	=	 0xCE9F4310;
    FractArr[3818] 	=	 0xB43EC1B1;
    FractArr[3819] 	=	 0x6AAD4FF0;
    FractArr[3820] 	=	 0xF26432B1;
    FractArr[3821] 	=	 0xD5B4A823;
    FractArr[3822] 	=	 0xB68C8CE4;
    FractArr[3823] 	=	 0x7C8D6945;
    FractArr[3824] 	=	 0x1FD7EFE3;
    FractArr[3825] 	=	 0x56733C11;
    FractArr[3826] 	=	 0xA26B7D72;
    FractArr[3827] 	=	 0x6857332A;
    FractArr[3828] 	=	 0x1FDBC6CD;
    FractArr[3829] 	=	 0x0000D9FF;	*/
}
#endif

/*
*******************************************************************************
 * Description:	Pregenerated int values for profcode
 *
 * Created by: Joakim Larsson 
*******************************************************************************
*/

void InitArr(int arr[MAXARR_PROF])
{
  arr[0]=89982;
  arr[1]=9634;
  arr[2]=8333;
  arr[3]=14430;
  arr[4]=60881;
  arr[5]=24060;
  arr[6]=23484;
  arr[7]=33978;
  arr[8]=53347;
  arr[9]=8928;
  arr[10]=52311;
  arr[11]=89715;
  arr[12]=34767;
  arr[13]=98838;
  arr[14]=94927;
  arr[15]=98031;
  arr[16]=20746;
  arr[17]=90891;
  arr[18]=60518;
  arr[19]=91987;
  arr[20]=80588;
  arr[21]=9020;
  arr[22]=11690;
  arr[23]=96483;
  arr[24]=33334;
  arr[25]=65793;
  arr[26]=27922;
  arr[27]=3225;
  arr[28]=34159;
  arr[29]=23724;
  arr[30]=30997;
  arr[31]=86460;
  arr[32]=55141;
  arr[33]=68994;
  arr[34]=37100;
  arr[35]=94953;
  arr[36]=21948;
  arr[37]=37398;
  arr[38]=50340;
  arr[39]=69787;
  arr[40]=30544;
  arr[41]=47715;
  arr[42]=90009;
  arr[43]=47448;
  arr[44]=52829;
  arr[45]=89581;
  arr[46]=92339;
  arr[47]=5613;
  arr[48]=80157;
  arr[49]=35176;
  arr[50]=34054;
  arr[51]=54696;
  arr[52]=19910;
  arr[53]=32931;
  arr[54]=14024;
  arr[55]=60655;
  arr[56]=46520;
  arr[57]=60711;
  arr[58]=86067;
  arr[59]=27069;
  arr[60]=93119;
  arr[61]=4290;
  arr[62]=99804;
  arr[63]=18064;
  arr[64]=8874;
  arr[65]=69320;
  arr[66]=54325;
  arr[67]=91699;
  arr[68]=29252;
  arr[69]=16859;
  arr[70]=17408;
  arr[71]=96316;
  arr[72]=43109;
  arr[73]=49095;
  arr[74]=77826;
  arr[75]=10721;
  arr[76]=87692;
  arr[77]=19966;
  arr[78]=38413;
  arr[79]=51629;
  arr[80]=77600;
  arr[81]=26125;
  arr[82]=88530;
  arr[83]=80830;
  arr[84]=28666;
  arr[85]=7991;
  arr[86]=13387;
  arr[87]=73614;
  arr[88]=30915;
  arr[89]=92026;
  arr[90]=77073;
  arr[91]=25734;
  arr[92]=52914;
  arr[93]=49689;
  arr[94]=95574;
  arr[95]=76049;
  arr[96]=12610;
  arr[97]=18560;
  arr[98]=71703;
  arr[99]=41069;
}

/*
*******************************************************************************
 * Convert FractArr to Big Endian. Must be used to be able to view memory 
 * as JPEG when big endian CPU is used 
 *
 * Created by: Kristoffer Martinsson
*******************************************************************************
*/

#ifdef BIG_ENDIAN
void HexSwapFractMem(void){
	int i;
	
	for(i = 0; i < 3830; i++)
        {
                FractArr[i] =                             \
                ((FractArr[i]&0xFF000000)>>24)   |        \
                ((FractArr[i]&0x00FF0000)>>8)    |        \
                ((FractArr[i]&0x0000FF00)<<8)    |        \
                ((FractArr[i]&0x000000FF)<<24);
        }
	
}
#endif

/* EOF */

