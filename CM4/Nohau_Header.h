/*
*******************************************************************************
 * File:	Nohau_Header.h
 *-----------------------------------------------------------------------------
 * Copyright (C) 2008 Nohau Solutions
 * All Rights Reserved
 *
 * Version History
 *-----------------------------------------------------------------------------
 * 08-FEB-2010	Joakim LArsson		Re - port of code to be generic
 *
 * 10-AUG-2009	Joakim Larsson		Added profiling code for trace
 *
 * 11-JUL-2008	Joakim Larsson		Created
 *
 *
*******************************************************************************
*/

//#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

int GetPrimerBack(void);
#define BASE 33
#define nsteps0 75
#define n0 200
#define xmin -2.
#define xmax 2.
#define ymin -2
#define ymax 2
#define repC -0.75
#define impC 0.0
#define MAXARR_LEAF 5000
#define f(x) ((double)(4.0/(1.0+x*x)))
#define pi ((double)(4.0*atan(1.0)))
#define MAXARR_PRIME 15
#define MAXARR_FREC 412
void HexSwapFractMem(void);
#define IM1   2147483563
#define IM2   2147483399
#define AM    (1.0/IM1)
#define IMM1  (IM1-1)
#define IA1   40014
#define IA2   40692
#define IQ1   53668
#define IQ2   52774
#define IR1   12211
#define IR2   3791
#define NTAB  32
#define NDIV  (1+IMM1/NTAB)
#define EPS   1.2e-7
#define RNMX  (1.0 - EPS)
#define MAXARR_PROF 100
#define MaxMorphosis 10
#define TRUE 1
#define FALSE 0
#define MaxPrimer 13
#define MAX 10

#define MX 30
#define MY 36
#define Apokryf 8

#define NohauSIN	sin
#define NohauCOS	cos
#define NohauATAN	atan
#define CarlosMarciaRex 4
#define DRAC 3

void StructHandler(void);
void StructHandlerAnotherMem(void);
struct NohauLinkedList {
    int BoxOfInteger;
    char BoxOfChar;
    struct NohauLinkedList * next;
};

static int FractArr[100];
#define DIMM 18
char DimKeeper[DIMM+1];
int Checker = 0;

typedef struct NohauLinkedList container;
void IOTestConsole(int argc, char **argv,char **envp);

struct StructCore {

    char * RosenFors;
    int MosseBo;

};

static struct StructMain {

    int Venus;

    struct StructCore Identity1;
    struct StructCore Identity2;

    union {

        int Ra;
        float Osiris;
        char Hathor;

        } EOR;

    int Field_1:2;
    unsigned Field_2:3;

}Statter[2];

struct tracker{
    int x, y, fx, fy;
};


static void Trigo (int Looper);
int NumberOfStackCalls = 0;
int lecter = 0;
static int SingArr[MAXARR_PROF];
static int Primer[MaxPrimer];
int GlobalEcho = 0;
double ResPow = 0.0;
void LinkedListInitFunction(void);
void LinkedListBlancFunction(void);
static int NohauMatrix[MAX][MAX];
static int StopPrimer = MaxPrimer;
static int StartPrimer = 1;
static int fact = 1000000000;
//static int LeafArr[MAXARR_LEAF];
static float FloatArr[MAXARR_PROF];
static float FrecArr1[MAXARR_FREC];
static float FrecArr2[MAXARR_FREC];
static float FrecArr3[MAXARR_FREC];

unsigned char Sinewave[255];
void SetupSinewave(void);

void DataMotionWaveFormer(void);
//void LeafOfJulia(void);
void IntToFloat(int _int_[MAXARR_PROF], float _float_[MAXARR_PROF]);
void BigSmall(int end[2], int arr[MAXARR_PROF]);
int Partition(int *numbers, int low, int high);
void Sorter(int *numbers, int low, int high);

char *IdentifyMeNow[4];
static char IdentifyMe3[]={74,111,97,107,105,109,46,76,97,114,115,115,111,110,64,78,111,104,97,117,0};
static char IdentifyMe2[]={74,97,110,101,46,83,118,101,110,115,115,111,110,64,78,111,104,97,117,0};
static char IdentifyMe1[]={78,105,99,107,108,97,115,46,74,111,104,110,115,115,111,110,64,78,111,104,97,117,0};
static char IdentifyMe4[]={75,114,105,115,116,111,102,102,101,114,46,77,97,114,116,105,110,115,115,111,110,64,78,111,104,97,117,0};

static char IdentifyMeClean[]={78,111,104,97,117,32,111,102,32,83,119,101,100,101,110,0};
char *IdentifyMeNowClean;

float SinArr[MAXARR_PROF], CosArr[MAXARR_PROF], TanArr[MAXARR_PROF];
int NumBItter, NumbPower;

struct StructCE {

    char * Who;
    int ID;

	union {

        int N[MAX][MAX];
        char H;

    }   P;
};

static struct NohauSM {  

    struct StructCE ISCE;

    union {
        int GK[MAX][MAX];
        char H;
    } 	E;

    int F_1:2;
    unsigned F_2:3;

}NSM[CarlosMarciaRex];

void BlancStructHandler(void);
void SwapStructNameFault(void);

float WaveFormer(int x);
void HexSwap(int arr[MAXARR_PROF]);
void InitArr(int arr[MAXARR_PROF]);
void Linker(void);
double RecurcePower(double RecCount, unsigned pow);
void EchoesOfOsirian(void);
void Echoes00(void);
void Echoes01(void);
void Echoes02(void);
void Echoes03(void);
void Echoes04(void);
void Echoes05(void);
void Echoes06(void);
void Echoes07(void);
void Echoes08(void);
void Echoes09(void);
void Echoes10(void);
void Echoes11(void);
void Echoes12(void);
static void XEchoes(void);
int Virtual(int i);
void Common(void);
void BitPrint(int Number);
void EchoesOfOsirian(void);
void StructInit(void);
void LoadFractMem(void);
void BlankFractMem(void);
void WriteMemWrite(void);
void WriteMemDest(void);
void SpeedTrouble(void);
void Morgoth(void);
void InitMatrix(void);
float Wotan(int *sejd);
int Kain(int Jered);

//ITM define
int quicksort_main(void);


//ITM defines
/*#define ITM_Port8(n)    (*((volatile unsigned char *)(0xE0000000+4*n)))
#define ITM_Port16(n)   (*((volatile unsigned short*)(0xE0000000+4*n)))
#define ITM_Port32(n)   (*((volatile unsigned long *)(0xE0000000+4*n)))

;#define DEMCR           (*((volatile unsigned long *)(0xE000EDFC)))
#define TRCENA          0x01000000

	*/


/* EOF */
