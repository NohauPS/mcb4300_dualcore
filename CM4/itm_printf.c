#include <stdarg.h>

static volatile unsigned int  *g_itmBase = (volatile unsigned int *)0xE0000000;

#define ITM_ENABLE_ACCESS  { g_itmBase[0x3EC]=0xC5ACCE55; }
#define ITM_TRACE_PRIV     g_itmBase[0x390]
#define ITM_TRACE_ENABLE   g_itmBase[0x380]

static volatile unsigned int *ITM_BASE = (volatile unsigned int *)0xE0000000;
#define ITM_TRACE_D8(_channel_,_data_) { \
	volatile unsigned int *_ch_=ITM_BASE+(_channel_); \
	while ( *_ch_ == 0); \
	(*((volatile unsigned char *)(_ch_)))=(_data_); \
}

#define ITM_TRACE_D16(_channel_,_data_) { \
	volatile unsigned int *_ch_=ITM_BASE+(_channel_); \
	while ( *_ch_ == 0); \
	(*((volatile unsigned short *)(_ch_)))=(_data_); \
}

#define ITM_TRACE_D32(_channel_,_data_) { \
	volatile unsigned int *_ch_=ITM_BASE+(_channel_); \
	while ( *_ch_ == 0); \
	*_ch_ = (_data_); \
}

extern int itm_vsprintf(char *buf, const char *fmt, va_list args);

void ITM_write_channel_1(int data) {
	ITM_TRACE_D32(1,data);
}

void ITM_printf(const char *format,...)
{
	union {
		char c[100];
		unsigned int i[25];
	} line;
	unsigned int v;
	int i,j,l;
	va_list ap;

	va_start(ap, format);
	l=itm_vsprintf(&(line.c[0]),format,ap);
	l++;
	va_end(ap);
	i=0;j=0;
	while (i<l)
	{
		v=line.i[j];
		i+=4;
		j++;
		if (i>l)
			v&=(0xFFFFFFFF>>((i-l)*8));
		ITM_TRACE_D32(0,v);
	}
}

void itm_enable(void)
{
 	ITM_ENABLE_ACCESS;
	ITM_TRACE_PRIV=0;
	ITM_TRACE_ENABLE=0x7FFFFFFF;   //Disable channel 31 since RTX use this channel
}








