/*------------------------------------------------------------------------------
 * Copyright (c) 2004-2014 Arm Limited (or its affiliates). All
 * rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1.Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   2.Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   3.Neither the name of Arm nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *------------------------------------------------------------------------------
 * Name:    Blinky.c
 * Purpose: Dual Core LED Flasher for MCB4300
 *----------------------------------------------------------------------------*/

#include "LPC43xx.h"                    /* LPC43xx Definitions                */
#include "Board_LED.h"
#include "Board_ADC.h"
#include "Board_Thermometer.h"          // ::Board Support:Thermometer
#include "cmsis_os2.h" 

const                                   /* Place the CM0 image in CM4 ROM     */
#include "CM0_Image.c"                  /* Cortex M0 image reference          */

uint32_t LEDOn, LEDOff;

uint32_t M4_EventTimer, M0_EventResp;
extern void nohau_demo();
extern void ITM_printf(const char *format,...);

static osThreadId_t tid_nohau_demo; 

// Main stack size must be multiple of 8 Bytes
#define NOHAU_DEMO_STK_SZ (768U)
uint64_t nohau_demo_stk[NOHAU_DEMO_STK_SZ / 8 ];
static uint64_t nohau_demo_tcb[64];

const osThreadAttr_t nohau_demo_attr = {
  .stack_mem  = &nohau_demo_stk[0],
  .stack_size = sizeof(nohau_demo_stk),
  .priority = osPriorityBelowNormal,
  .cb_mem  = &nohau_demo_tcb,
  .cb_size = sizeof(nohau_demo_tcb),
	 .name = "Nohau Demo"
};

static osThreadId_t tid_blinky; 
// Main stack size must be multiple of 8 Bytes
#define BLINKY_STK_SZ (512U)
uint64_t blinky_stk[BLINKY_STK_SZ / 8 ];
static uint64_t blinky_tcb[64];
const osThreadAttr_t blinky_attr = {
  .stack_mem  = &blinky_stk[0],
  .stack_size = sizeof(blinky_stk),
  .cb_mem  = &blinky_tcb,
  .cb_size = sizeof(blinky_tcb),
	 .name = "Blinky"
};

static osThreadId_t tid_ADC; 
// Main stack size must be multiple of 8 Bytes
#define ADC_STK_SZ (256U)
uint64_t ADC_stk[ADC_STK_SZ / 8 ];
static uint64_t ADC_tcb[64];
const osThreadAttr_t ADC_attr = {
  .stack_mem  = &ADC_stk[0],
  .stack_size = sizeof(ADC_stk),
  .cb_mem  = &ADC_tcb,
  .cb_size = sizeof(ADC_tcb),
  .name = "ADC"
};

static osThreadId_t tid_Temp; 
// Main stack size must be multiple of 8 Bytes
#define TEMP_STK_SZ (256U)
uint64_t Temp_stk[TEMP_STK_SZ / 8 ];
static uint64_t Temp_tcb[64];
const osThreadAttr_t Temp_attr = {
  .stack_mem  = &Temp_stk[0],
  .stack_size = sizeof(Temp_stk),
  .cb_mem  = &Temp_tcb,
  .cb_size = sizeof(Temp_tcb),
  .name = "Temperature"
};

const osTimerAttr_t led_timer_attr = {
  .name = "Led timer"
};

const osTimerAttr_t event_timer_attr = {
  .name = "Event timer"
};

osTimerId_t led_timer_id;
osTimerId_t event_timer_id;
int led_status=0;

osMessageQueueId_t Temp_MsgQueue, ADC_MsgQueue;
#define MSGQUEUE_OBJECTS      10                                   // number of Message Queue Objects
 
typedef struct {                                                   // object data type
  int Buf;
} MSGQUEUE_OBJ_t;
 

/*------------------------------------------------------------------------------
  SysTick IRQ Handler @ 50ms
 *----------------------------------------------------------------------------*/
#if 0
void SysTick_Handler (void) {
  static uint32_t ticks;
  
  switch (ticks++) {
    case  0: LEDOn  = 1; break;
    case  2: LEDOff = 1; break;
    case  4: ticks  = 0; break;
    
    default:
      if (ticks > 10) {
        ticks = 0;
      }
  }
  if (M4_EventTimer) {
    M4_EventTimer -= 50;
  }
}
#endif

/*------------------------------------------------------------------------------
  M0APP Core IRQ Handler
 *----------------------------------------------------------------------------*/
void M0APP_IRQHandler (void) {
  LPC_CREG->M0APPTXEVENT = 0;
  M0_EventResp = 1;
}

/*------------------------------------------------------------------------------
  Load Cortex M0APP Application Image
 *----------------------------------------------------------------------------*/
void Load_CM0_Image (uint32_t DestAddr, const uint8_t *Image, uint32_t Sz) {
  uint32_t i;
  uint8_t *dp = (uint8_t *)DestAddr;

  /* Copy application image */
  for (i = 0; i < Sz; i++) {
    dp[i] = Image[i];
  }
  /* Set shadow pointer to beginning of the CM0 application */
  LPC_CREG->M0APPMEMMAP = DestAddr;
}

void Blinky(){
	
	uint32_t LEDState    = 0;
  uint32_t M4_EventSet = 1;
	int count = 0;
	osTimerStart(led_timer_id, 250);
	M4_EventTimer = 1;
	osTimerStart(event_timer_id, 250);
  
  char infobuf[100];
  osVersion_t osv;
  osStatus_t status;
  status = osKernelGetInfo(&osv, infobuf, sizeof(infobuf));
  if(status == osOK) {
    ITM_printf("Kernel Information: %s\r\n", infobuf);
    ITM_printf("Kernel Version    : %d\r\n", osv.kernel);
    ITM_printf("Kernel API Version: %d\r\n", osv.api);
  }
	while (1) {
  if (LEDOn) {
      LEDOn = 0;
      LED_On (0);                       /* Turn on LED 0                      */
  }

  if (LEDOff) {
      LEDOff = 0;
      LED_Off (0);                      /* Turn off LED 0                     */
  }
    
  if (M4_EventSet) {
      M4_EventSet = 0;
      __sev();                          /* Send event signal to the CM0 core  */
  }

  if (M4_EventTimer == 0) {           /* Wait for timer timeout (250ms)     */
      if (M0_EventResp) {
        M0_EventResp = 0;               /* M0 response received, toggle LED 3 */

        (LEDState ^= 1) ? LED_On (3) : LED_Off (3);

        M4_EventSet =   1;
        M4_EventTimer  = 1;
        osTimerStart(event_timer_id, 250);
        if (count < 65535) { 
          count++;
        }
        else {
          count = 0;
        }
       
      }
  }
	osDelay(1);
  }
}


void Temperature() {
  static int32_t temp = 0;
  while(1) {
    temp = Thermometer_GetTemperature ();
    if (temp != TEMPERATURE_INVALID) {
      if ( osMessageQueueGetSpace(Temp_MsgQueue) > 0 ) {
          osMessageQueuePut (Temp_MsgQueue, &temp, 0, NULL);
      }
    }
    osDelay(1000);
  }
}


void Timer_Callback (void *arg)  {                          
  if(led_status == 0 ) {
		LEDOn = 1;
		led_status = 1;
	}
	else {
		LEDOff = 1;
		led_status = 0;
	}
                                                            
}

void Event_Callback (void *arg)  {
	
	M4_EventTimer = 0;
	
}

void ADC() {
  static int adcVal;
  while(1) {
    ADC_StartConversion();
    while (ADC_ConversionDone() != 0);
    adcVal = ADC_GetValue();
    if ( osMessageQueueGetSpace(ADC_MsgQueue) > 0 ) {
      osMessageQueuePut (ADC_MsgQueue, &adcVal, 0, NULL);
    }
    osDelay(500);
  }
}


__NO_RETURN void osRtxIdleThread (void *argument) {
  for(;;) {
    __WFI();
  }
}




/*------------------------------------------------------------------------------
  Main function
 *----------------------------------------------------------------------------*/
int main (void) {


  SystemCoreClockUpdate ();             /* Update system core clock           */
  SysTick_Config(SystemCoreClock/20);   /* Generate interrupt each 50 ms      */
  LED_Initialize();                     /* LED Initialization                 */
  ADC_Initialize(); 
  Thermometer_Initialize();
  NVIC_EnableIRQ (M0APP_IRQn);          /* Enable IRQ from the M0APP Core     */

  /* Stop CM0 core */
  LPC_RGU->RESET_CTRL1 = (1 << 24);

  /* Download CM0 application */
  Load_CM0_Image (0x10080000, LR0, sizeof (LR0)); 
  
  /* Start CM0 core */
  LPC_RGU->RESET_CTRL1 = 0;

  osKernelInitialize ();
	
  led_timer_id = osTimerNew (Timer_Callback, osTimerPeriodic, NULL, &led_timer_attr);
  event_timer_id = osTimerNew (Event_Callback, osTimerOnce, NULL, &event_timer_attr);
	
  Temp_MsgQueue = osMessageQueueNew(MSGQUEUE_OBJECTS, sizeof(MSGQUEUE_OBJ_t), NULL);
  ADC_MsgQueue = osMessageQueueNew(MSGQUEUE_OBJECTS, sizeof(MSGQUEUE_OBJ_t), NULL);
  /* Create application main thread */
  tid_nohau_demo = osThreadNew(nohau_demo, NULL, &nohau_demo_attr);
  tid_blinky = osThreadNew(Blinky, NULL, &blinky_attr);
  tid_ADC = osThreadNew(ADC, NULL, &ADC_attr);
  tid_Temp = osThreadNew(Temperature, NULL, &Temp_attr);
  /* Start thread execution */
  osKernelStart();
	
  while(1);

}
