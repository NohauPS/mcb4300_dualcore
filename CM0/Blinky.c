/*------------------------------------------------------------------------------
 * Copyright (c) 2004-2014 Arm Limited (or its affiliates). All
 * rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1.Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   2.Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   3.Neither the name of Arm nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *------------------------------------------------------------------------------
 * Name:    Blinky.c
 * Purpose: Cortex M0 Core LED Flasher for MCB4300
 *----------------------------------------------------------------------------*/

#include "LPC43xx.h"                    /* LPC43xx Definitions                */
#include "Board_LED.h"

#define CLOCK_M4 180000000

uint32_t LEDOn, LEDOff, LEDToggle; 

/*------------------------------------------------------------------------------
  Repetitive Interrupt Timer IRQ Handler @ 50ms
 *----------------------------------------------------------------------------*/
void M0_RITIMER_OR_WWDT_IRQHandler (void) {
  static uint32_t ticks;

  LPC_RITIMER->CTRL |= 1;
  
  switch (ticks++) {
    case  0: LEDOn  = 1; break;
    case  5: LEDOff = 1; break;
    case  9: ticks  = 0; break;
    
    default:
      if (ticks > 10) {
        ticks = 0;
      }
  }
}

/*------------------------------------------------------------------------------
  M4 Core IRQ Handler
 *----------------------------------------------------------------------------*/
void M0_M4CORE_IRQHandler (void) {
  LPC_CREG->M4TXEVENT = 0;
  LEDToggle = 1;
}


/*------------------------------------------------------------------------------
  Repetitive Interrupt Timer Initialization
 *----------------------------------------------------------------------------*/
void RIT_Init (void) {
  LPC_RITIMER->COMPVAL = CLOCK_M4/20-1; /* Set compare value (50ms)           */
  LPC_RITIMER->COUNTER = 0;
  LPC_RITIMER->CTRL    = (1 << 3) |
                         (1 << 2) |
                         (1 << 1) ;

  NVIC_EnableIRQ (M0_RITIMER_OR_WWDT_IRQn);                           
}

/*------------------------------------------------------------------------------
  Main function
 *----------------------------------------------------------------------------*/
int main (void) {
  uint32_t LEDState = 0;
  
  RIT_Init();                           /* Repetitive Interrupt Timer Init    */
  LED_Initialize();                     /* LED Initialization                 */
  NVIC_EnableIRQ (M0_M4CORE_IRQn);      /* Enable IRQ from the M4 Core        */

  while (1) {
    if (LEDOn) {
      LEDOn = 0;
      LED_On (7);                       /* Turn on LED 7                      */
    }

    if (LEDOff) {
      LEDOff = 0;
      LED_Off (7);                      /* Turn off LED 7                     */
    }

    if (LEDToggle) {                    /* Toggle LED 4                       */
      LEDToggle = 0;
              
      (LEDState ^= 1) ? LED_On (4) : LED_Off(4);
      
      __sev();
    }
  }
}
